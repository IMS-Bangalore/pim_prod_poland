//
//  CustomLabel.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 09/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "CustomLabel.h"
#import "ComponentGroup.h"

@implementation CustomLabel

-(void)setEnabled:(BOOL)enabled {
    self.alpha = enabled ? 1 : kComponentDisabledAlpha;
}

@end
