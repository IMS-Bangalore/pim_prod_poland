//
//  MultipleChoiceViewController.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 22/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "MultipleChoiceViewController.h"

@implementation MultipleChoiceViewController
@synthesize message = _message;
@synthesize acceptButtonTitle = _acceptButtonTitle;
@synthesize cancelButtonTitle = _cancelButtonTitle;
@synthesize otherButtonTitle = _otherButtonTitle;
@synthesize delegate = _delegate;
@synthesize eventBlock = _eventBlock;
@synthesize messageLabel = _messageLabel;
@synthesize acceptButton = _acceptButton;
@synthesize cancelButton = _cancelButton;
@synthesize otherButton = _otherButton;
@synthesize messageFont = _messageFont;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc {
    [_message release];
    [_acceptButtonTitle release];
    [_cancelButtonTitle release];
    [_otherButtonTitle release];
    _delegate = nil;
    [_messageLabel release];
    [_acceptButton release];
    [_cancelButton release];
    [_otherButton release];
    [_messageFont release];
    [super dealloc];
}

#pragma mark - Private methods

- (void)resizeFontForLabel:(UILabel*)aLabel maxSize:(int)maxSize minSize:(int)minSize {
    // use font from provided label so we don't lose color, style, etc
    UIFont *font = [UIFont boldSystemFontOfSize:36];
    
    // start with maxSize and keep reducing until it doesn't clip
    for(int i = maxSize; i >= minSize; i--) {
        font = [font fontWithSize:i];
        CGSize constraintSize = CGSizeMake(aLabel.bounds.size.width, MAXFLOAT);
        
        // This step checks how tall the label would be with the desired font.
        CGSize labelSize = [self.message sizeWithFont:font constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
        if(labelSize.height <= aLabel.bounds.size.height)
            break;
    }
    // Set the UILabel's font to the newly adjusted font.
    aLabel.text = self.message;
    aLabel.font = font;
}

- (void)autoResizeFont {
    [self resizeFontForLabel:_messageLabel maxSize:_messageLabel.font.pointSize minSize:_messageLabel.minimumFontSize];
}

#pragma mark - Public methods
- (void)setMessage:(NSString *)message {
    [message retain];
    [_message release];
    _message = message;
    
    self.messageLabel.text = message;
    [self autoResizeFont];
}

- (void)setAcceptButtonTitle:(NSString *)acceptButtonTitle {
    [acceptButtonTitle retain];
    [_acceptButtonTitle release];
    _acceptButtonTitle = acceptButtonTitle;
    
    [self.acceptButton setTitle:acceptButtonTitle forState:UIControlStateNormal];
    self.acceptButton.hidden = acceptButtonTitle == nil;
}

- (void)setCancelButtonTitle:(NSString *)cancelButtonTitle {
    [cancelButtonTitle retain];
    [_cancelButtonTitle release];
    _cancelButtonTitle = cancelButtonTitle;
    
    [self.cancelButton setTitle:cancelButtonTitle forState:UIControlStateNormal];
    self.cancelButton.hidden = cancelButtonTitle == nil;
}

- (void)setOtherButtonTitle:(NSString *)otherButtonTitle {
    [otherButtonTitle retain];
    [_otherButtonTitle release];
    _otherButtonTitle = otherButtonTitle;
    
    [self.otherButton setTitle:otherButtonTitle forState:UIControlStateNormal];
    self.otherButton.hidden = otherButtonTitle == nil;
}

- (void)setMessageFont:(UIFont *)messageFont {
    [messageFont retain];
    [_messageFont release];
    _messageFont = messageFont;
    
    if (messageFont != nil) {
        self.messageLabel.font = messageFont;
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//Ravi_Bukka
    self.messageLabel.text = NSLocalizedString(@"MULTICHOICE_MSG", nil);
    
    [self.cancelButton setTitle:NSLocalizedString(@"MULTICHOICE_CANCEL", nil) forState:UIControlStateNormal];
    [self.cancelButton setTitle:NSLocalizedString(@"MULTICHOICE_CANCEL", nil) forState:UIControlStateHighlighted];
    
    [self.acceptButton setTitle:NSLocalizedString(@"MULTICHOICE_ACCEPT", nil) forState:UIControlStateNormal];
    [self.acceptButton setTitle:NSLocalizedString(@"MULTICHOICE_ACCEPT", nil) forState:UIControlStateHighlighted];
    
    [self.otherButton setTitle:NSLocalizedString(@"MULTICHOICE_OTHER", nil) forState:UIControlStateNormal];
    [self.otherButton setTitle:NSLocalizedString(@"MULTICHOICE_OTHER", nil) forState:UIControlStateHighlighted];
    
    // Refresh UI
    self.message = _message;
    self.acceptButtonTitle = _acceptButtonTitle;
    self.cancelButtonTitle = _cancelButtonTitle;
    self.otherButtonTitle = _otherButtonTitle;
    self.messageFont = _messageFont;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

#pragma mark - IBActions
- (IBAction)acceptButtonClicked:(id)sender {
    if (_delegate != nil) {
        [_delegate multipleChoiceDidClickButton:kMultipleChoiceAccept];
    }
    if (_eventBlock != nil) {
        _eventBlock(kMultipleChoiceAccept);
    }
}

- (IBAction)cancelButtonClicked:(id)sender {
    if (_delegate != nil) {
        [_delegate multipleChoiceDidClickButton:kMultipleChoiceCancel];
    }
    if (_eventBlock != nil) {
        _eventBlock(kMultipleChoiceCancel);
    }
}

- (IBAction)otherButtonClicked:(id)sender {
    if (_delegate != nil) {
        [_delegate multipleChoiceDidClickButton:kMultipleChoiceOther];
    }
    if (_eventBlock != nil) {
        _eventBlock(kMultipleChoiceOther);
    }
}

@end
