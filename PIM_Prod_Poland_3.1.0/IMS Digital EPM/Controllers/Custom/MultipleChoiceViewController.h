//
//  MultipleChoiceViewController.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 22/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kMultipleChoiceAccept,
    kMultipleChoiceCancel,
    kMultipleChoiceOther
} MultipleChoiceButton;

@class  MultipleChoiceViewController;
@protocol MultipleChoiceViewControllerDelegate
- (void)multipleChoiceDidClickButton:(MultipleChoiceButton)button;
@end

typedef void (^MultipleChoiceButtonEventBlock)(MultipleChoiceButton button);

@interface MultipleChoiceViewController : UIViewController


#pragma mark - Properties
@property (nonatomic, retain) NSString* message;
@property (nonatomic, retain) NSString* acceptButtonTitle;
@property (nonatomic, retain) NSString* cancelButtonTitle;
@property (nonatomic, retain) NSString* otherButtonTitle;
@property (nonatomic, retain) UIFont* messageFont;
@property (nonatomic, assign) id<MultipleChoiceViewControllerDelegate> delegate;
@property (nonatomic, copy) MultipleChoiceButtonEventBlock eventBlock;

#pragma mark - IBActions
- (IBAction)acceptButtonClicked:(id)sender;
- (IBAction)cancelButtonClicked:(id)sender;
- (IBAction)otherButtonClicked:(id)sender;

#pragma mark - IBOutlets
@property (retain, nonatomic) IBOutlet UILabel *messageLabel;
@property (retain, nonatomic) IBOutlet UIButton *acceptButton;
@property (retain, nonatomic) IBOutlet UIButton *cancelButton;
@property (retain, nonatomic) IBOutlet UIButton *otherButton;


@end
