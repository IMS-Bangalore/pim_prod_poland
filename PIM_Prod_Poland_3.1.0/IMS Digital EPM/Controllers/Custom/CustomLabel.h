//
//  CustomLabel.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 09/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomLabel : UILabel

@end
