//
//  PlaceOfVisitTableViewController.h
//  IMS Digital EPM
//
//  Created by Bukka, Ravi (Bangalore) on 20/12/13.
//
//

#import <UIKit/UIKit.h>
//#import "PatientViewController.h"

@class PlaceOfVisitTableViewController;

@protocol PlaceOfVisitViewControllerDelegate <NSObject>

//- (void)placeOfVisitDismissPopOverController:(NSString*)selectedCellString;

- (void)placeOfVisitDismissPopOverController:(PlaceOfVisitTableViewController*)controller didSelectTableView:(UITableView *)tableView rowAtIndexPath:(NSIndexPath *)indexPath didSelectString:(NSString*)selectedCellString;

@end


@interface PlaceOfVisitTableViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, assign) IBOutlet UITableView *tableView;
@property (nonatomic, assign) NSMutableArray *placeOfVisitArray;
//@property (nonatomic, assign) PatientViewController *patientViewController;
@property (assign, nonatomic) id<PlaceOfVisitViewControllerDelegate> delegate;




@end
