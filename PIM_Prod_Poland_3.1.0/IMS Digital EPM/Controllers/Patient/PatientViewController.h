//
//  PatientViewController.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 11/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "ComponentGroup.h"
#import <UIKit/UIKit.h>
#import "CustomTextField.h"
#import "CustomSegmentedControl.h"
#import "Patient.h"
#import "Doctor.h"
#import "DiagnosticTabbarItem.h"
#import "DiagnosticViewController.h"
#import "ChangesControlProtocol.h"
#import "ModalControllerProtocol.h"
#import "WeekdaySelectionView.h"
#import "WeekdaySelectionPopoverContentViewController.h"
#import "Smoker.h"
#import "PredictiveTextField.h"
#import "PrescriptionAndSickFundVViewController.h"
#import "PlaceOfVisitTableViewController.h"
#import "PlaceOfVisit.h"
#import "TherapyType.h"
#import "SpecialitySelectionViewController.h"
#import "RadioButton.h"
#import "RadioButtonGroup.h"
#import "SpecialitySelectionViewController.h"

//Ravi_Bukka: CR#2: Patient Difference and Local Notification
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>


@class PlaceOfVisitTableViewController;
@class PatientViewController;

#pragma mark - Delegate protocol
@protocol PatientViewControllerDelegate <NSObject>


- (void)PatientViewControllerWillPresentPopUp:(PatientViewController*)controller;
- (void)PatientViewControllerWillDismissPopUp:(PatientViewController*)controller;
@end

@interface PatientViewController : UIViewController <UITextFieldDelegate, DiagnosticTabbarItemDelegate, DiagnosticViewControllerDelegate, ChangesControlProtocol, RadioButtonGroupDelegate,SpecialitySelectionViewControllerDelegate,UIWeekdaySelectionPopoverContentViewControllerDelegate, UIPopoverControllerDelegate,PrescriptionAndSickFundVViewControllerDelegate,PlaceOfVisitViewControllerDelegate>{
  //Deepak Carpenter: Variables for Sick and Fund Controllers
    NSString *pres;
    NSString *sick;
    NSString *presSick;
}

typedef void (^PatientClosedBlock)(BOOL closed);

// Delegate
@property (nonatomic, assign) id<ModalControllerProtocol> modalController;

// Referenced entity
@property (nonatomic, retain) Patient *selectedPatient;
@property (nonatomic, retain) Doctor *doctor;

// Patient navigation
@property (retain, nonatomic) IBOutlet UIButton *prevPatientButton;
@property (retain, nonatomic) IBOutlet UILabel *patientNumberLabel;
@property (retain, nonatomic) IBOutlet UIButton *finishPatientButton;
// Age
@property (retain, nonatomic) IBOutlet ComponentGroup *ageGroup;
@property (retain, nonatomic) IBOutlet CustomTextField *ageTextField;
@property (retain, nonatomic) IBOutlet CustomSegmentedControl *ageTypeSelector;
@property (retain, nonatomic) IBOutlet UIView *agePopoverView;

//Ravi_Bukka
@property (retain, nonatomic) IBOutlet UILabel *patientLabel;
@property (retain, nonatomic) IBOutlet UILabel *ageLabel;
@property (retain, nonatomic) IBOutlet UILabel *typeOfVisitLabel;
@property (retain, nonatomic) IBOutlet UILabel *dateOfVisit;
@property (retain, nonatomic) IBOutlet UILabel *genderLabel;

//Deepak Carpenter : Entity injection
@property (nonatomic, retain) id<ChangesControlProtocol> changesControlProtocol;
@property (retain, nonatomic) IBOutlet PredictiveTextField *placeOfVisitTextField;
@property (nonatomic, retain) PlaceOfVisit* userDefinedPlaceOfVisit;
@property (retain, nonatomic) IBOutlet PredictiveTextField *presSickTextField;

//Ravi_Bukka: added for different Greece and poland xibs
@property (retain, nonatomic) IBOutlet CustomSegmentedControl *smokerTypeSelector;
@property (retain, nonatomic) IBOutlet ComponentGroup *smokerGroup;
@property (retain, nonatomic) IBOutlet UILabel *smokerLabel;
@property (nonatomic, retain) Smoker *smoker;
@property (nonatomic, retain) IBOutlet UILabel *prescriptionLabel;
@property (nonatomic, retain) IBOutlet UILabel *sickFundLabel;
@property (nonatomic, retain) IBOutlet UILabel *dividerLabel;

//@property (retain, nonatomic) IBOutlet UITextField *placeOfVisitTextField;
@property (retain, nonatomic) IBOutlet ComponentGroup *placeOfVisitGroup;
@property (retain, nonatomic) IBOutlet UILabel *placeOfVisit;
@property (assign, nonatomic) id<PatientViewControllerDelegate> delegate;
@property (nonatomic, assign) NSArray *placeOfVisitArray;
@property (nonatomic, retain) UIPopoverController *myPopoverController;
@property (nonatomic, retain) PlaceOfVisitTableViewController *placeOfVisitViewController;



// Gender
@property (retain, nonatomic) IBOutlet ComponentGroup *genderGroup;
@property (retain, nonatomic) IBOutlet CustomSegmentedControl *genderSelector;
// Consultation
@property (retain, nonatomic) IBOutlet ComponentGroup *consultationGroup;
@property (retain, nonatomic) IBOutlet CustomSegmentedControl *consultationSelector;
// Date
@property (retain, nonatomic) IBOutlet ComponentGroup *dateGroup;
@property (retain, nonatomic) IBOutlet UIButton *dateButton;
@property (retain, nonatomic) IBOutlet CustomTextField *dateTextField;

// Diagnosis
@property (retain, nonatomic) IBOutlet UIView *diagnosisHolder;

// TabBar
@property (retain, nonatomic) IBOutlet UIView *tabBarView;
@property (retain, nonatomic) IBOutlet UIButton *addDiagnoseButton;
@property (nonatomic, assign) NSInteger selectedTabbarItem;

@property (retain, nonatomic) IBOutlet ComponentGroup *patientInsuranceGroup;
@property (retain, nonatomic) IBOutlet PredictiveTextField *patientInsuranceTextField;
@property (retain, nonatomic) IBOutlet CustomLabel *patientInsuranceLabel;

@property (retain, nonatomic) IBOutlet PredictiveTextField *placeOfVisitTextField_pl;
@property (retain, nonatomic) IBOutlet ComponentGroup *placeOfVisitGroup_pl;

@property (retain, nonatomic) IBOutlet ComponentGroup *typeOfContactGroup;
@property (retain, nonatomic) IBOutlet CustomLabel *typeOfContactLabel;
@property (retain, nonatomic) IBOutlet CustomSegmentedControl *typeOfContactSelector;

@property (retain, nonatomic) IBOutlet ComponentGroup *therapyChoiceGroup;
@property (retain, nonatomic) IBOutlet CustomLabel *therapyChoiceSpecialistLabel;
@property (retain, nonatomic) IBOutlet CustomLabel *choiceTherapyLabel;
//@property (retain, nonatomic) IBOutlet CustomSegmentedControl *therapyTypeSelector;
@property (nonatomic, retain) TherapyType *therapytype;
@property (retain, nonatomic) IBOutlet RadioButton *therapyChoiceSpecialistButton;
@property (retain, nonatomic) IBOutlet RadioButton *personalChoiceButton;

@property (retain, nonatomic) IBOutlet RadioButtonGroup *therapyChoiceButtonGroup;
//@property (retain, nonatomic) IBOutlet RadioButton *therapyTypeChangeButton;





#pragma mark IBActions
- (IBAction)prevPatientClicked:(id)sender;
- (IBAction)ageSelectorChanged:(id)sender;
- (IBAction)genderSelectorChanged:(id)sender;
- (IBAction)consultationSelectorChanged:(id)sender;
- (IBAction)nextPatientClicked:(id)sender;
- (IBAction)addDiagnoseButtonClicked:(id)sender;
- (IBAction)visitDateClicked:(id)sender;
- (IBAction)deletePatientButtonClicked:(id)sender;

//Ravi Bukk: Added for logging feature
- (void)sendLogInfo:(NSNotification *) notification;

#pragma mark - Public methods
- (void)requestCloseWithBlock:(PatientClosedBlock)onCloseBlock;
- (void)willGoBackground;
- (BOOL)requestDismiss;

//Ravi_Bukka: CR#2: Patient Difference and Local Notification
- (BOOL)connected;

@end
