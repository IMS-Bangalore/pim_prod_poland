//
//  ModalWebViewController.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 15/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "ModalWebViewController.h"
#import <QuartzCore/QuartzCore.h>

@implementation ModalWebViewController

@synthesize delegate = _delegate;
@synthesize popupView = _popupView;
@synthesize webViewHolderView = _webViewHolderView;
@synthesize contentWebView = _contentWebView;
@synthesize url = _url;
@synthesize returnButton = _returnButton;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void) loadContentUrl {
    self.contentWebView.alpha = 0;
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:self.url];
    [self.contentWebView loadRequest:requestObj];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Get Popover layer to corner radius
    
//Ravi_Bukka
    [_returnButton setTitle:NSLocalizedString(@"MODALWEB_RETURN_MAIN_MENU", nil) forState:UIControlStateNormal];
    [_returnButton setTitle:NSLocalizedString(@"MODALWEB_RETURN_MAIN_MENU", nil) forState:UIControlStateHighlighted];
    
    CALayer * popupLayer = [_popupView layer];
    [popupLayer setMasksToBounds:YES];
    [popupLayer setCornerRadius:10.0];
    // Add border
    [popupLayer setBorderWidth:2.0];
    [popupLayer setBorderColor:[[UIColor blackColor] CGColor]];
    _contentWebView.delegate = self;
    _contentWebView.scrollView.delegate = self;
    _contentWebView.scrollView.scrollEnabled = YES;
    _contentWebView.scrollView.showsHorizontalScrollIndicator = NO;
    _contentWebView.scrollView.bounces = NO;
    
    [self loadContentUrl];
}

- (void)viewDidUnload
{
    [self setPopupView:nil];
    [self setWebViewHolderView:nil];
    [self setContentWebView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

- (void)setUrl:(NSURL *)anUrl {
    if (_url != anUrl) {
        [anUrl retain];
        [_url release];
        _url = anUrl;
        [self loadContentUrl];
    }
}

- (void)dealloc {
    [_popupView release];
    [_webViewHolderView release];
    [_contentWebView release];
    [_url release];
    _delegate = nil;
    [super dealloc];
}

- (void)closeModal {
    [self.delegate modalWebViewControllerRequestClose:self];
}

- (IBAction)backButtonClicked:(id)sender {
    [self.delegate modalWebViewControllerRequestClose:self];
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [UIView transitionWithView:webView duration:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        webView.alpha = 1;
    } completion:nil];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.x > 0)
        scrollView.contentOffset = CGPointMake(0, scrollView.contentOffset.y);
}

@end
