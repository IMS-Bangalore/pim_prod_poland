//
//  PrescriptionAndSickFundVViewController.h
//  IMS Digital EPM
//
//  Created by Bukka, Ravi (Bangalore) on 16/12/13.
//
//

#import <UIKit/UIKit.h>
#import "PredictiveTextField.h"

@class PrescriptionAndSickFundVViewController;
@protocol PrescriptionAndSickFundVViewControllerDelegate <NSObject>
- (void)PrescriptionAndSickFundSelectionController:(PrescriptionAndSickFundVViewController*)popup didSelectPrescription:(NSString*)prescriptionName;
- (void)PrescriptionAndSickFundSelectionController:(PrescriptionAndSickFundVViewController*)popup didSelectionSickFund:(NSString*)sickfundName;
@end




@interface PrescriptionAndSickFundVViewController : UIViewController <PredictiveTextFieldDelegate> 

@property (retain, nonatomic) IBOutlet UIButton *okButton;
@property (retain, nonatomic) IBOutlet UIButton *singlePrescrButton;
@property (retain, nonatomic) IBOutlet UIButton *monthlyPrescrButton;
@property (retain, nonatomic) IBOutlet UIButton *trimesterPrescrButton;
@property (retain, nonatomic) IBOutlet UIButton *otherSickFundButton;
@property (retain, nonatomic) IBOutlet UITextField *otherTextField;

@property (retain, nonatomic) IBOutlet UIButton *okaButton;
@property (retain, nonatomic) IBOutlet UIButton *ogaButton;
@property (retain, nonatomic) IBOutlet UIButton *oaeeButton;
@property (retain, nonatomic) IBOutlet UIButton *opadButton;


@property (nonatomic, assign) id<PrescriptionAndSickFundVViewControllerDelegate> delegate;

@property (retain, nonatomic) IBOutletCollection(UIButton) NSArray* buttons;

@property (retain, nonatomic) NSString *prescriptionString;
@property (retain, nonatomic) NSString *sickFundString;





@property (retain, nonatomic) IBOutlet UILabel *prescriptionTypeLabel;
@property (retain, nonatomic) IBOutlet UILabel *sickFundLabel;
@property (nonatomic, assign) BOOL buttonSelectedPres;
@property (nonatomic, assign) BOOL buttonSelectedSickFund;

- (IBAction)accepterButtonClicked:(id)sender;

@end
