//
//  PrescriptionAndSickFundVViewController.m
//  IMS Digital EPM
//
//  Created by Bukka, Ravi (Bangalore) on 16/12/13.
//
//

#import "PrescriptionAndSickFundVViewController.h"


@interface PrescriptionAndSickFundVViewController ()
- (IBAction)buttonClicked:(id)sender;
@end

@implementation PrescriptionAndSickFundVViewController

@synthesize okButton = _okButton;
@synthesize otherTextField = _otherTextField;


@synthesize prescriptionTypeLabel = _prescriptionTypeLabel;
@synthesize sickFundLabel = _sickFundLabel;
@synthesize delegate = _delegate;



@synthesize buttons = _buttons;

@synthesize singlePrescrButton = _singlePrescrButton;
@synthesize monthlyPrescrButton = _monthlyPrescrButton;
@synthesize trimesterPrescrButton = _trimesterPrescrButton;
@synthesize otherSickFundButton = _otherSickFundButton;

@synthesize buttonSelectedPres = _buttonSelectedPres;
@synthesize buttonSelectedSickFund = _buttonSelectedSickFund;

@synthesize prescriptionString = _prescriptionString;
@synthesize sickFundString = _sickFundString;

@synthesize okaButton = _okaButton;
@synthesize ogaButton = _ogaButton;
@synthesize oaeeButton = _oaeeButton;
@synthesize opadButton = _opadButton;



//**********************
// initWithDelegate
//**********************
- (id)init {
    self = [super initWithNibName:@"PrescriptionAndSickFundVViewController" bundle:nil];
    if (self) {
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}




- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   
//    
//    PATIENT_IFPUBLIC
//
    
    _buttonSelectedPres = NO;
    _buttonSelectedSickFund = NO;
    
    [_okButton setTitle:NSLocalizedString(@"MULTICHOICE_ACCEPT",nil) forState:UIControlStateNormal];
    [_okButton setTitle:NSLocalizedString(@"MULTICHOICE_ACCEPT",nil) forState:UIControlStateHighlighted];
    
    [_singlePrescrButton setTitle:NSLocalizedString(@"PATIENT_SINGLE",nil) forState:UIControlStateNormal];
    [_singlePrescrButton setTitle:NSLocalizedString(@"PATIENT_SINGLE",nil) forState:UIControlStateHighlighted];
    
    [_monthlyPrescrButton setTitle:NSLocalizedString(@"PATIENT_MONTHLY",nil) forState:UIControlStateNormal];
    [_monthlyPrescrButton setTitle:NSLocalizedString(@"PATIENT_MONTHLY",nil) forState:UIControlStateHighlighted];
    
    [_trimesterPrescrButton setTitle:NSLocalizedString(@"PATIENT_TRIMESTER",nil) forState:UIControlStateNormal];
    [_trimesterPrescrButton setTitle:NSLocalizedString(@"PATIENT_TRIMESTER",nil) forState:UIControlStateHighlighted];
    
    
    [_otherSickFundButton setTitle:NSLocalizedString(@"PATIENT_OTHER",nil) forState:UIControlStateNormal];
    [_otherSickFundButton setTitle:NSLocalizedString(@"PATIENT_OTHER",nil) forState:UIControlStateHighlighted];
    

    [_okaButton setTitle:NSLocalizedString(@"PATIENT_IKA",nil) forState:UIControlStateNormal];
    [_okaButton setTitle:NSLocalizedString(@"PATIENT_IKA",nil) forState:UIControlStateHighlighted];
    
    [_ogaButton setTitle:NSLocalizedString(@"PATIENT_OGA",nil) forState:UIControlStateNormal];
    [_ogaButton setTitle:NSLocalizedString(@"PATIENT_OGA",nil) forState:UIControlStateHighlighted];
    
    [_oaeeButton setTitle:NSLocalizedString(@"PATIENT_OAEE",nil) forState:UIControlStateNormal];
    [_oaeeButton setTitle:NSLocalizedString(@"PATIENT_OAEE",nil) forState:UIControlStateHighlighted];
    
    [_opadButton setTitle:NSLocalizedString(@"PATIENT_OPAD",nil) forState:UIControlStateNormal];
    [_opadButton setTitle:NSLocalizedString(@"PATIENT_OPAD",nil) forState:UIControlStateHighlighted];
    
    _sickFundLabel.text = NSLocalizedString(@"PATIENT_SICKFUND",nil);
    _prescriptionTypeLabel.text = NSLocalizedString(@"PATIENT_PRESCRIPTION_TYPE",nil);
 
    

    
}



- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Private methods
- (void)refreshUI {
}




#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _prescriptionString = NULL;
    _sickFundString = NULL;
  
    _otherTextField.userInteractionEnabled = NO;
    
     UIFont* font = [UIFont boldSystemFontOfSize:10.f];
    
    
    
    // Link button events
    for (UIButton* button in _buttons) {
        [button setImage:nil forState:UIControlStateNormal];
        [button setImage:nil forState:UIControlStateHighlighted];
        [button setImage:nil forState:UIControlStateDisabled];
        [button setImage:nil forState:UIControlStateSelected];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [button setContentMode:UIViewContentModeCenter];
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 8)];
        button.titleLabel.font = font;
        
        [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    // Trigger UI refresh
    [self refreshUI];
    
}

- (void)viewDidUnload
{
    [self setOkButton:nil];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

- (IBAction)buttonClicked:(id)sender {
    
    NSInteger index = [sender tag];
    if (index == 0 || index == 1 || index == 2) {
    for(int i=0;i<3;i++){
		[[self.buttons objectAtIndex:i] setBackgroundImage:[UIImage imageNamed:@"bola_modal_inact.png"] forState:UIControlStateNormal];
        
	}
	[sender setBackgroundImage:[UIImage imageNamed:@"bola_modal_act.png"] forState:UIControlStateNormal];
        
        UIButton *button = [_buttons objectAtIndex:index];
        _prescriptionString = button.titleLabel.text;
       
        
    }
    
    if (index == 3 || index == 4 || index == 5 || index == 6 || index == 7 ) {
        for(int i=3;i<8;i++){
            [[self.buttons objectAtIndex:i] setBackgroundImage:[UIImage imageNamed:@"bola_modal_inact.png"] forState:UIControlStateNormal];
            
        }
        [sender setBackgroundImage:[UIImage imageNamed:@"bola_modal_act.png"] forState:UIControlStateNormal];
        
     //   UIButton *button = [_buttons objectAtIndex:index];
        _sickFundString = [sender titleLabel].text;
        
        if ([_sickFundString isEqualToString:@"ΆΛΛΟ"]) {
            
            _otherTextField.userInteractionEnabled = YES;
            _otherTextField.placeholder=@"Παρακαλώ εισάγετε Αξία";
            _otherTextField.font = [UIFont systemFontOfSize:14.0];
            }
        else {
            _otherTextField.userInteractionEnabled = NO;
            _otherTextField.placeholder=@"";
        }
        
    }
    
}

- (IBAction)accepterButtonClicked:(id)sender {
    
    if ([_sickFundString isEqualToString:@"ΆΛΛΟ"] && _otherTextField.text!=nil) {
        _okButton.enabled=YES;
        _sickFundString = _otherTextField.text;
        
        NSLog(@"sickfund string %@", _sickFundString);
    }
    

    if (_prescriptionString != NULL && _sickFundString != NULL) {
        [_delegate PrescriptionAndSickFundSelectionController:self didSelectPrescription:_prescriptionString];
        [_delegate PrescriptionAndSickFundSelectionController:self didSelectionSickFund:_sickFundString];
    }
    
}
- (void)dealloc {

    [super dealloc];
}

@end

