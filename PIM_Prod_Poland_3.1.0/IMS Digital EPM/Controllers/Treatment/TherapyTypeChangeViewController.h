//
//  TherapyTypeChangeViewController.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 12/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PredictiveTextField.h"
#import "RadioButton.h"
#import "RadioButtonGroup.h"
#import "TherapyReplacementReason.h"
#import "Medicament.h"

@class TherapyTypeChangeViewController;
@protocol TherapyTypeChangeViewControllerDelegate <NSObject>

- (void)therapyTypeChangeViewController:(TherapyTypeChangeViewController*)controller didSelectReplacementReason:(TherapyReplacementReason*)replacementReason andMedicament:(Medicament*)medicament;
-(void) therapyTypeChangeViewController:(TherapyTypeChangeViewController*)controller didSelectReplacementReason:(TherapyReplacementReason*)replacementReason andUserMedicament:(NSString*)userMedicament;

@end

@interface TherapyTypeChangeViewController : UIViewController <PredictiveTextFieldDelegate, RadioButtonGroupDelegate>

@property (retain, nonatomic) IBOutlet UIButton *okButton;
@property (retain, nonatomic) IBOutlet PredictiveTextField *medicamentTextField;
@property (retain, nonatomic) IBOutlet RadioButtonGroup *therapyButtonGroup;
@property (nonatomic, assign) id<TherapyTypeChangeViewControllerDelegate> delegate;

//Ravi_Bukka: Added for localization
@property (retain, nonatomic) IBOutlet UILabel *therapyChangeLabel;
@property (retain, nonatomic) IBOutlet UILabel *productReplaceLabel;
@property (retain, nonatomic) IBOutlet RadioButton *lackOfEfficiencyButton;
@property (retain, nonatomic) IBOutlet RadioButton *toleranceButton;
@property (retain, nonatomic) IBOutlet RadioButton *priceButton;
@property (retain, nonatomic) IBOutlet RadioButton *reasonButton;
@property (retain, nonatomic) IBOutlet RadioButton *preferanceButton;


- (IBAction)okButtonClicked:(id)sender;
- (void) clear;
@end
