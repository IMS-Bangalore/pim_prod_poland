//
//  ChatMessageCell.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 05/12/12.
//
//

#import <UIKit/UIKit.h>
#import "ChatMessage.h"

@interface ChatMessageCell : UITableViewCell

#pragma mark - Properties
@property (nonatomic, strong) ChatMessage* chatMessage;

#pragma mark - IBOutlets
@property (retain, nonatomic) IBOutlet UILabel *dateLabel;
@property (retain, nonatomic) IBOutlet UILabel *messageLabel;

#pragma mark - Public methods
- (CGFloat)heightForMessage:(NSString*)message;

@end
