//
//  ChatMessageCell.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 05/12/12.
//
//

#import "ChatMessageCell.h"

static NSString* const kDateFormat = @"dd/MM/yyyy HH:mm";

@implementation ChatMessageCell

#pragma mark - Init

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        _messageLabel.text = NSLocalizedString(@"Preguntado el 31/12/2012", nil);
        _dateLabel.text = NSLocalizedString(@"Preguntado el 31/12/2012", nil);
    }
    return self;
}

#pragma mark - Selected

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Setters

- (void)setChatMessage:(ChatMessage *)chatMessage {
    _chatMessage = chatMessage;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:kDateFormat];
    if (chatMessage.question) {
        self.dateLabel.text = [NSString stringWithFormat:NSLocalizedString(@"QUESTION_DATE_LABEL_TEXT", nil), [[dateFormatter stringFromDate:_chatMessage.date] substringToIndex:10], [[dateFormatter stringFromDate:_chatMessage.date] substringFromIndex:11]];
    } else {
        self.dateLabel.text = [NSString stringWithFormat:NSLocalizedString(@"ANSWER_DATE_LABEL_TEXT", nil), [[dateFormatter stringFromDate:_chatMessage.date] substringToIndex:10], [[dateFormatter stringFromDate:_chatMessage.date] substringFromIndex:11]];
    }
    self.messageLabel.text = chatMessage.message;
    [dateFormatter release];
}

#pragma mark - Public methods

- (CGFloat)heightForMessage:(NSString*)message {
    CGFloat height = self.frame.size.height;
    CGFloat offset = self.frame.size.height - _messageLabel.frame.size.height;
    CGSize textSize = [message sizeWithFont:_messageLabel.font constrainedToSize:CGSizeMake(_messageLabel.frame.size.width, 10000.0) lineBreakMode:_messageLabel.lineBreakMode];
    height = MAX(height, textSize.height + offset);
    
    return height;
}

#pragma mark - Dealloc

- (void)dealloc {
    [_messageLabel release];
    [_dateLabel release];
    [super dealloc];
}

@end
