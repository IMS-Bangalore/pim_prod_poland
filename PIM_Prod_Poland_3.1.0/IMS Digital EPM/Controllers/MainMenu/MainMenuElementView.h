//
//  MainMenuElementView.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 02/10/12.
//
//

#import <UIKit/UIKit.h>
#import "MainMenuItem.h"
#import "TCPassthroughView.h"
#import "TCClickableUIView.h"

@class MainMenuElementView;
@protocol MainMenuElementViewDelegate <NSObject>
- (BOOL)mainMenuElementshouldAnimateClick:(MainMenuElementView*)menuView;
- (void)mainMenuElementClicked:(MainMenuElementView*)menuView;
@end

@interface MainMenuElementView : TCPassthroughView

#pragma mark - Properties
@property (nonatomic, retain) MainMenuItem* menuItem;
@property (nonatomic, assign, getter = isButtonSelected) BOOL buttonSelected;
@property (nonatomic, assign) id<MainMenuElementViewDelegate> delegate;

#pragma mark - IBOutlets
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UIImageView *selectedBackgroundImage;
@property (retain, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (retain, nonatomic) IBOutlet TCClickableUIView *highlightView;
@property (retain, nonatomic) IBOutlet UIView *notificationBallView;
@property (retain, nonatomic) IBOutlet UILabel *notificationNumberLabel;

#pragma mark - IBActions
- (IBAction)buttonClicked:(id)sender;

#pragma mark - Public methods
- (void)setButtonSelected:(BOOL)buttonSelected animated:(BOOL)animated;

@end
