//
//  Diagnosis+Complete.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 15/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Diagnosis.h"

@interface Diagnosis (Complete)
/** @brief returns true if all the required fields of the diagnosis are complete */
- (BOOL)isComplete;

/** @brief returns true if none of the fields are set */
- (BOOL)isEmpty;

@end
