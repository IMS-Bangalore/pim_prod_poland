//
//  SyncActionQueue.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 27/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "SyncActionQueue.h"
#import "EntityFactory.h"


NSString* const kSyncActionAdd = @"ADD";
NSString* const kSyncActionUpdate = @"UPDATE";
NSString* const kSyncActionDelete = @"DELETE";

static NSString* const kSyncActionSortKey = @"creationTime";


@interface SyncActionQueue()
@property (readonly) EntityFactory* entityFactory;
@end

@implementation SyncActionQueue
@synthesize entityFactory = _entityFactory;

#pragma mark SingletonAccess
static SyncActionQueue* sharedQueueInstance = nil;
+ (SyncActionQueue*)sharedQueue {
    if (sharedQueueInstance == nil) {
        sharedQueueInstance = [[SyncActionQueue alloc] init];
    }
    return sharedQueueInstance;
}

#pragma mark - Init and dealloc
- (id)init {
    if (( self = [super init] )) {
        _entityFactory = [EntityFactory sharedEntityFactory];
    }
    return self;
}


#pragma mark - Private methods

#pragma mark - Public methods
- (SyncAction*)firstConfirmedSyncAction {
    NSArray* results = [self confirmedSyncActions];
    
    SyncAction* action = nil;
    if (results.count > 0) {
        action = [results objectAtIndex:0];
    }
    return action;
}

- (NSArray*)confirmedSyncActions {
#ifdef __OFFLINE_VERSION__
    return nil;
#endif
    
    DAssert([NSThread isMainThread], @"This method must be called from Main Thread");
    
    // Fetch SyncActions with 'readyToSync' state set to YES
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:kSyncActionSortKey ascending:YES];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"readyToSync == YES"];
    NSArray* confirmedActions = [[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([SyncAction class]) predicate:predicate sortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    return confirmedActions;
}

- (NSArray*)pendingSyncActions {
#ifdef __OFFLINE_VERSION__
    return nil;
#endif
    
    DAssert([NSThread isMainThread], @"This method must be called from Main Thread");
    
    // Fetch SyncActions with 'readyToSync' state set to NO
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:kSyncActionSortKey ascending:YES];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"readyToSync == NO"];
    NSArray* pendingActions = [[EntityFactory sharedEntityFactory] fetchEntities:NSStringFromClass([SyncAction class]) predicate:predicate sortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    return pendingActions;
}

- (void)confirmSyncAction:(SyncAction*)syncAction {
#ifdef __OFFLINE_VERSION__
    return;
#endif
    DAssert([NSThread isMainThread], @"This method must be called from Main Thread");
    
    syncAction.readyToSync = [NSNumber numberWithBool:YES];
}

- (void)confirmSyncActions:(NSSet*)syncActions {
#ifdef __OFFLINE_VERSION__
    return;
#endif
    
    for (SyncAction* action in syncActions) {
        [self confirmSyncAction:action];
    }
}

- (void)confirmAllSyncActions {
#ifdef __OFFLINE_VERSION__
    return;
#endif
    
    for (SyncAction* action in [self pendingSyncActions]) {
        [self confirmSyncAction:action];
    }
}

- (void)cancelSyncAction:(SyncAction*)syncAction {
#ifdef __OFFLINE_VERSION__
    return;
#endif
    
    [_entityFactory deleteObject:syncAction];
}

- (void)cancelSyncActions:(NSSet*)syncActions {
#ifdef __OFFLINE_VERSION__
    return;
#endif
    
    for (SyncAction* action in syncActions) {
        [self cancelSyncAction:action];
    }
}

- (void)cancelAllSyncActions {
#ifdef __OFFLINE_VERSION__
    return;
#endif
    
    for (SyncAction* action in [self pendingSyncActions]) {
        [self cancelSyncAction:action];
    }
    for (SyncAction* action in [self confirmedSyncActions]) {
        [self cancelSyncAction:action];
    }
}

- (SyncAction*)createDeleteActionWithObject:(Synchronizable*)syncObject fromObject:(Synchronizable*)fromSyncObject {
#ifdef __OFFLINE_VERSION__
    return nil;
#endif
    
    // Instantiate the SyncAction object
    SyncAction* action = [_entityFactory insertNewWithEntityName:NSStringFromClass([SyncAction class])];
    action.creationTime = [NSDate date];
    action.actionType = kSyncActionDelete;
    action.targetObject = syncObject;
    action.secondaryTarget = fromSyncObject;
    action.readyToSync = [NSNumber numberWithBool:NO];
    
    return action;
}

- (SyncAction*)createAddActionWithObject:(Synchronizable*)syncObject {
#ifdef __OFFLINE_VERSION__
    return nil;
#endif
    
    // Instantiate the SyncAction object
    SyncAction* action = [_entityFactory insertNewWithEntityName:NSStringFromClass([SyncAction class])];
    action.creationTime = [NSDate date];
    action.actionType = kSyncActionAdd;
    action.targetObject = syncObject;
    action.readyToSync = [NSNumber numberWithBool:NO];
    
    return action;
}

- (SyncAction*)createUpdateActionWithObject:(Synchronizable*)syncObject {
#ifdef __OFFLINE_VERSION__
    return nil;
#endif
    
    // Instantiate the SyncAction object
    SyncAction* action = [_entityFactory insertNewWithEntityName:NSStringFromClass([SyncAction class])];
    action.creationTime = [NSDate date];
    action.actionType = kSyncActionUpdate;
    action.targetObject = syncObject;
    action.readyToSync = [NSNumber numberWithBool:NO];
    
    return action;
}

@end
