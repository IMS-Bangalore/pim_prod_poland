//
//  ChangeNotificationOperation.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 29/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "ChangeNotificationOperation.h"
#import "Synchronizable.h"
#import "Diagnosis.h"
#import "Patient.h"
#import "DoctorInfo.h"
#import "Treatment.h"
#import "RequestFactory.h"
#import "BaseRequest.h"

@interface ChangeNotificationOperation()
@property (nonatomic, retain) BaseRequest* request;
@end

@implementation ChangeNotificationOperation
@synthesize syncObjectID = _syncObjectID;
@synthesize operationType = _operationType;
@synthesize request = _request;
@synthesize delegate = _delegate;
@synthesize errorBlock = _errorBlock;
@synthesize successBlock = _successBlock;
@synthesize syncActionID = _syncActionID;


#pragma mark - Init and dealloc
- (id)initWithObjectID:(NSManagedObjectID*)syncObjectID operationType:(ChangeNotificationOperationType)operationType syncActionID:(NSManagedObjectID*)syncActionID delegate:(id<ChangeNotificationOperationDelegate>)delegate {
    if (( self = [super init] )) {
        _syncObjectID = [syncObjectID retain];
        _operationType = operationType;
        _syncActionID = [syncActionID retain];
        self.delegate = delegate;
    }
    return self;
}

- (id)initWithObjectID:(NSManagedObjectID*)syncObjectID operationType:(ChangeNotificationOperationType)operationType syncActionID:(NSManagedObjectID*)syncActionID successBlock:(ChangeNotificationOperationSuccessBlock)successBlock errorBlock:(ChangeNotificationOperationErrorBlock)errorBlock {
    if (( self = [super init] )) {
        _syncObjectID = [syncObjectID retain];
        _operationType = operationType;
        _syncActionID = [syncActionID retain];
        self.successBlock = successBlock;
        self.errorBlock = errorBlock;
    }
    return self;
}

- (void)dealloc {
    [_syncObjectID release];
    [_syncActionID release];
    [_request release];
    [_errorBlock release];
    [_successBlock release];
    _delegate = nil;
    [super dealloc];
}


#pragma mark - Private methods
- (void)onComplete {
    if (![self isCancelled]) {
        if (_delegate != nil) {
            [_delegate changeNotificationOperationDidComplete:self];
        }
        if (_successBlock != nil) {
            _successBlock();
        }
    }
}

- (void)onError:(NSError*)error {
    if (![self isCancelled]) {
        if (_delegate != nil) {
            [_delegate changeNotificationOperation:self didFailWithError:error];
        }
        if (_errorBlock != nil) {
            _errorBlock(error);
        }
    }
}

- (Synchronizable*)fetchObject {
    NSManagedObjectContext* moc = [[[NSManagedObjectContext alloc] init] autorelease];
    moc.persistentStoreCoordinator = _syncObjectID.persistentStore.persistentStoreCoordinator;
    
    Synchronizable* object = (Synchronizable*)[moc objectWithID:self.syncObjectID];
    return object;
}

//Ravi_Bukka: Webservice Information, create, update, delete for all three
- (BaseRequest*)createRequestWithObject:(Synchronizable*)object {
    NSString* entityName = [_syncObjectID.entity managedObjectClassName];
    
    RequestFactory* requestFactory = [RequestFactory sharedInstance];
    
    BaseRequest* request = nil;
    if ([entityName isEqualToString:NSStringFromClass([DoctorInfo class])]) {
        DoctorInfoSyncType syncType;
        switch (self.operationType) {
            case kChangeNotificationOperationAdd: {
                syncType = kDoctorInfoSyncTypeInsert;
                break;
            }
            case kChangeNotificationOperationUpdate: {
                syncType = kDoctorInfoSyncTypeUpdate;
                break;
            }
            case kChangeNotificationOperationDelete: {
                DLog(@"Unsupported operation: Doctor delete");
                return nil;
            }
        }
        
        request = [requestFactory createDoctorInfoSyncRequestWithSyncType:syncType doctorInfo:(DoctorInfo*)object onComplete:^{
            [self onComplete];
        } onError:^(NSError *error) {
            [self onError:error];
        }];
    }
    else if ([entityName isEqualToString:NSStringFromClass([Patient class])]) {
        PatientSyncType syncType;
        switch (self.operationType) {
            case kChangeNotificationOperationAdd: {
                syncType = kPatientSyncTypeInsert;
                break;
            }
            case kChangeNotificationOperationUpdate: {
                syncType = kPatientSyncTypeUpdate;
                break;
            }
            case kChangeNotificationOperationDelete: {
                syncType = kPatientSyncTypeDelete;
                break;
            }
        }
        
        request = [requestFactory createPatientSyncRequestWithSyncType:syncType patient:(Patient*)object onComplete:^{
            [self onComplete];
        } onError:^(NSError *error) {
            [self onError:error];
        }];
    }
    else if ([entityName isEqualToString:NSStringFromClass([Diagnosis class])]) {
        DiagnosisSyncType syncType;
        switch (self.operationType) {
            case kChangeNotificationOperationAdd: {
                syncType = kDiagnosisSyncTypeInsert;
                break;
            }
            case kChangeNotificationOperationUpdate: {
                syncType = kDiagnosisSyncTypeUpdate;
                break;
            }
            case kChangeNotificationOperationDelete: {
                syncType = kDiagnosisSyncTypeDelete;
                break;
            }
        }
        
        request = [requestFactory createDiagnosisSyncRequestWithSyncType:syncType diagnosis:(Diagnosis*)object onComplete:^{
            [self onComplete];
        } onError:^(NSError *error) {
            [self onError:error];
        }];
    }
    else if ([entityName isEqualToString:NSStringFromClass([Treatment class])]) {
        TreatmentSyncType syncType;
        switch (self.operationType) {
            case kChangeNotificationOperationAdd: {
                syncType = kTreatmentSyncTypeInsert;
                break;
            }
            case kChangeNotificationOperationUpdate: {
                syncType = kTreatmentSyncTypeUpdate;
                break;
            }
            case kChangeNotificationOperationDelete: {
                syncType = kTreatmentSyncTypeDelete;
                break;
            }
        }
        
        request = [requestFactory createTreatmentSyncRequestWithSyncType:syncType treatment:(Treatment*)object onComplete:^{
            [self onComplete];
        } onError:^(NSError *error) {
            [self onError:error];
        }];
    }
    else {
        DLog(@"Unsupported operation with entity %@", _syncObjectID.entity);
    }
    
    return request;
}

#pragma mark - Public methods


#pragma mark - NSOperation
- (void)main {
    // Operation execution
    
    // Obtain the object from Persistent Store
    Synchronizable* object = [self fetchObject];
    if (![self isCancelled]) {
        if (object != nil) {
            self.request = [self createRequestWithObject:object];
            if (![self isCancelled]) {
                [_request start];
            }
        }
        else {
            DLog(@"No object found for ID %@", _syncObjectID);
        }
    }
}

@end
