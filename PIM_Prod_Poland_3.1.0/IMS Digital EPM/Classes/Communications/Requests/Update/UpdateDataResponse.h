//
//  UpdateDataResponse.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 30/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpdateDataResponse : NSObject

/** @brief Array of diagnosis objects to be insert or updated */
@property (nonatomic, retain) NSArray *diagnosisArray;
/** @brief Array of diagnosis identifiers to be removed */
@property (nonatomic, retain) NSArray *diagnosisDeleteArray;
/** @brief Array of medicament objects to be insert or updated */
@property (nonatomic, retain) NSArray *medicamentArray;
/** @brief Array of diagnosis identifiers to be removed */
@property (nonatomic, retain) NSArray *medicamentDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *effectsArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *effectsDeleteArray;

//Ravi_Bukka: Added for Checkdataversion Implementation
//Deepak_Carpenter : Added for UnitType in checkDataVersion
//Deepak_Carpenter : Added for UnitType in checkDataVersion
@property (nonatomic, retain) NSArray *unitTypeArray;
@property (nonatomic, retain) NSArray *unitTypeDeleteArray;

@property (nonatomic, retain) NSArray *drugIndicatorArray;
@property (nonatomic, retain) NSArray *drugIndicatorDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *drugReimbursementArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *drugReimbursementDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *durationTypeArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *durationTypeDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *frequencyArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *frequencyDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *patientInsuranceArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *patientInsuranceDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *otherSpecialitiesArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *otherSpecialitiesDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *provinceArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *provinceDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *specialitiesArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *specialitiesDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *therapyTypeArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *therapyTypeDeleteArray;
/** @brief Array of effects objects to be insert or updated */
@property (nonatomic, retain) NSArray *placeOfVisitArray;
/** @brief Array of effects identifiers to be removed */
@property (nonatomic, retain) NSArray *placeOfVisitDeleteArray;

/** @brief Array of Dosage Units objects to be insert or updated */
@property (nonatomic, retain) NSArray *dosageUnitsArray;
/** @brief Array of Dosage Units identifiers to be removed */
@property (nonatomic, retain) NSArray *dosageUnitsDeleteArray;


@end
