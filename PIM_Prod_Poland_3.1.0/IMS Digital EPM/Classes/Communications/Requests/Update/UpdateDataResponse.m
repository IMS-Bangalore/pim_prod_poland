//
//  UpdateDataResponse.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 30/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "UpdateDataResponse.h"

@implementation UpdateDataResponse

@synthesize diagnosisArray = _diagnosisArray;
@synthesize diagnosisDeleteArray = _diagnosisDeleteArray;
@synthesize medicamentArray = _medicamentArray;
@synthesize medicamentDeleteArray = _medicamentDeleteArray;
@synthesize effectsArray = _effectsArray;
@synthesize effectsDeleteArray = _effectsDeleteArray;

//Deepak_Carpenter : Added for checkDataVersion
@synthesize drugIndicatorArray=_drugIndicatorArray;
@synthesize drugIndicatorDeleteArray=_drugIndicatorDeleteArray;
@synthesize drugReimbursementArray=_drugReimbursementArray;
@synthesize drugReimbursementDeleteArray=_drugReimbursementDeleteArray;
@synthesize durationTypeArray=_durationTypeArray;
@synthesize durationTypeDeleteArray=_durationTypeDeleteArray;
@synthesize frequencyArray=_frequencyArray;
@synthesize frequencyDeleteArray=_frequencyDeleteArray;
@synthesize patientInsuranceArray=_patientInsuranceArray;
@synthesize patientInsuranceDeleteArray=_patientInsuranceDeleteArray;
@synthesize otherSpecialitiesArray=_otherSpecialitiesArray;
@synthesize otherSpecialitiesDeleteArray=_otherSpecialitiesDeleteArray;
@synthesize provinceArray=_provinceArray;
@synthesize provinceDeleteArray=_provinceDeleteArray;
@synthesize specialitiesArray=_specialitiesArray;
@synthesize specialitiesDeleteArray=_specialitiesDeleteArray;
@synthesize therapyTypeArray=_therapyTypeArray;
@synthesize therapyTypeDeleteArray=_therapyTypeDeleteArray;
@synthesize placeOfVisitArray=_placeOfVisitArray;
@synthesize placeOfVisitDeleteArray=_placeOfVisitDeleteArray;

//Utapl: Added for units checkdataversion implementation
@synthesize dosageUnitsArray=_dosageUnitsArray;
@synthesize dosageUnitsDeleteArray=_dosageUnitsDeleteArray;


-(NSString *)description {
    return [NSString stringWithFormat:
            @"diagnisisArray: %@;\n"
            "diagnosisDeleteArray: %@;\n"
            "medicamentArray: %@;\n"
            "medicamentDeleteArray: %@;\n"
            "effectsArray: %@;\n"
            "effectsDeleteArray: %@;\n"
            "_drugIndicatorArray: %@;\n"
            "_drugIndicatorDeleteArray: %@;\n"
            "_drugReimbursementArray: %@;\n"
            "_drugReimbursementDeleteArray: %@;\n"
            "DurationArray: %@;\n"
            "DurationDeleteArray: %@;\n"
            "FrequencyArray: %@;\n"
            "FreauencyDeleteArray: %@\n"
            "_patientInsuranceArray: %@;\n"
            "_patientInsuranceDeleteArray: %@;\n"
            "_otherSpecialitiesArray: %@;\n"
            "_otherSpecialitiesDeleteArray: %@;\n"
            "ProvinceArray: %@;\n"
            "ProvinceDeleteArray: %@;\n"
            "SpecialitiesArray: %@;\n"
            "SpecialitiesDeleteArray: %@;\n"
            "TherapyTypeArray: %@;\n"
            "TherapyTypeDeleteArray: %@;\n"
            "placeOfVisitArray: %@;\n"
            "placeOfVisitDeleteArray: %@;\n"
            "dosageUnitArray: %@;\n"
            "dosageUnitDeleteArray: %@;\n",
            _diagnosisArray, _diagnosisDeleteArray,
            _medicamentArray, _medicamentDeleteArray,
            _effectsArray, _effectsDeleteArray,
            _drugIndicatorArray,_drugIndicatorDeleteArray,
            _drugReimbursementArray,_drugReimbursementDeleteArray,
            _durationTypeArray,_durationTypeDeleteArray,
            _frequencyArray,_frequencyDeleteArray,
            _patientInsuranceArray,_patientInsuranceDeleteArray,
            _otherSpecialitiesArray,_otherSpecialitiesDeleteArray,
            _provinceArray,_provinceDeleteArray,
            _specialitiesArray,_specialitiesDeleteArray,
            _therapyTypeArray,_therapyTypeDeleteArray,
            _placeOfVisitArray,_placeOfVisitDeleteArray,_dosageUnitsArray,_dosageUnitsDeleteArray];
            
}

@end
