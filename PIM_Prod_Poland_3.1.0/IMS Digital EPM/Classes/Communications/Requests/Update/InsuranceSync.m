//
//  InsuranceSync.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 11/09/14.
//
//

#import "InsuranceSync.h"

@implementation InsuranceSync
@synthesize identifier = _identifier;
@synthesize name = _name;



-(NSString *)description {
    return [NSString stringWithFormat:@"%@(%@, %@)", self.class, _identifier, _name];
}


@end
