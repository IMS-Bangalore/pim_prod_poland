//
//  OtherSpecialitiesSync.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 30/09/14.
//
//


#import "OtherSpecialitiesSync.h"

@implementation OtherSpecialitiesSync
@synthesize identifier = _identifier;
@synthesize name = _name;

-(NSString *)description {
    return [NSString stringWithFormat:@"%@(%@, %@)", self.class, _identifier, _name];
}
@end
