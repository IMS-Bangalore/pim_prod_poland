//
//  UpdateDataRequest.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 30/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "UpdateDataRequest.h"
#import "DiagnosisTypeSync.h"
#import "MedicamentSync.h"
#import "PresentationSync.h"
#import "EffectsSync.h"
#import "NSDictionary+ArrayForKey.h"
//Deepak_Carpenter: Added for checkDataVersion
#import "DrugIndicatorSync.h"
#import "DrugReimbursementSync.h"
#import "DurationTypeSync.h"
#import "FrequencySync.h"
#import "InsuranceSync.h"
#import "OtherSpecialitiesSync.h"
#import "ProvinceSync.h"
#import "SpecialitiesSync.h"
#import "TherapyTypeSync.h"
#import "PlaceOfVisitSync.h"
#import "UnitTypeSync.h"

// Request Name
static NSString* const kRequestName  = @"CheckDataVersion";
// Content dictionary
static NSString* const kUserIdKey = @"userId";
static NSString* const kLastUpdatedTimeKey = @"LastUpdatedTime"; // Format 2010/12/12
static NSString* const kLastUpdatedTimeDateFormat = @"YYYY/MM/dd HH:mm:ss";
static NSString* const kOffset = @"Offset";
static NSString* const kOffsetInitialValue = @"0";

// Response
// Generic
static NSString* const kNewFields = @"NewFields";
static NSString* const kNewData = @"NewData";
static NSString* const kItem = @"item";

// Delete Fields
static NSString* const kDeletedFields = @"DeletedFields";
static NSString* const kDeletedData = @"DeletedData";




// Diagnosis list
static NSString* const kDiagnosisListSizeKey = @"DiagnosesListSize";
static NSString* const kDiagnosisListKey = @"DiagnosesList";
//static NSString* const kDiagnosisKey = @"diagnosis";
//static NSString* const kDiagnosisIdKey = @"diagnosisId";
//static NSString* const kDiagnosisNameKey = @"diagnosisName";
static NSString* const kDiagnosisIdKey = @"id";
static NSString* const kDiagnosisDFCCKey = @"DFCC";
static NSString* const kDiagnosisNameKey = @"DoctorWording";
static NSString* const kDiagnosisICD10Key = @"ICD10";



// Diagnoses delete fields
static NSString* const kDiagnosisListDeleteSizeKey = @"DiagnosesListDeleteSize";
static NSString* const kDiagnosisListDeleteKey = @"DiagnosesListDelete";
//static NSString* const kDiagnosisIdDeleteKey = @"diagnosisIdDelete";
static NSString* const kDiagnosisIdDeleteKey = @"Id";


// Medicament/Product
//static NSString* const kMedicineListSizeKey = @"medicineListSize";
//static NSString* const kMedicineListKey = @"medicineList";
//static NSString* const kMedicineKey = @"medicine";
//static NSString* const kMedicineIdKey = @"medicineId";
//static NSString* const kMedicineNameKey = @"medicineName";
//static NSString* const kParametersKey = @"parameters";
//static NSString* const kParameterKey = @"parameter";
//static NSString* const kDoseUnitIdKey = @"doseUnitId";
//static NSString* const kFilterKey = @"filter";
//static NSString* const kFormatKey = @"format";
//static NSString* const kImsCodeKey = @"imsCode";

static NSString* const kMedicineListSizeKey = @"ProductsListSize";
static NSString* const kMedicineListKey = @"ProductsList";
static NSString* const kMedicineKey = @"id";
static NSString* const kMedicineIdKey = @"ProductCode";
static NSString* const kMedicineNameKey = @"Description";
static NSString* const kPackKey = @"Pack";
static NSString* const kFCCCodeKey = @"FCCCode";
static NSString* const kLocalFormKey = @"LocalForm";
static NSString* const kFormDescriptionKey = @"FormDescription";
static NSString* const kProductTypeKey = @"ProductType";


// Medicament/Product delete
static NSString* const kMedicineListDeleteSizeKey = @"ProductsListDeleteSize";
static NSString* const kMedicineListDeleteKey = @"ProductsListDelete";
static NSString* const kMedicineIdDeleteKey = @"Id";
//Deepak_Carpenter : Added for dosageUnits/unitType
static NSString* const kUnitTypeListDeleteSizeKey = @"UnitTypeListDeleteSize";
static NSString* const kUnitTypeListDeleteKey = @"UnitTypeListDelete";
static NSString* const kUnitTypeIdDeleteKey = @"Id";

// Effects list
static NSString* const kEffectsListKey = @"DesiredEffectList";
static NSString* const kEffectsListSizeKey = @"DesiredEffectListSize";
static NSString* const kEffectIdKey = @"id";
static NSString* const kEffectValueKey = @"Value";
static NSString* const kEffectDescriptionKey = @"Name";

// Effects delete
static NSString* const kEffectsListDeleteSizeKey = @"DesiredEffectListDeleteSize";
static NSString* const kEffectsListDeleteKey = @"DesiredEffectListDelete";
static NSString* const kEffectsIdDeleteKey = @"Id";

//Deepak_Carpenter: Added for checkDataVersion
//--------------------------------AgeTypes----------------------------//
//// AgeTypes list
//static NSString* const kAgeTypesListKey = @"AgeTypesList";
//static NSString* const kAgeTypesListSizeKey = @"AgeTypesListSize";
//static NSString* const kAgeTypesIdKey = @"id";
//static NSString* const kAgeTypesValueKey = @"Value";
//static NSString* const kAgeTypesDescriptionKey = @"Name";
//// AgeTypes delete
//static NSString* const kAgeTypesListDeleteSizeKey = @"AgeTypesListDeleteSize";
//static NSString* const kAgeTypesListDeleteKey = @"AgeTypesListDelete";
//static NSString* const kAgeTypesIdDeleteKey = @"Id";
//
////--------------------------------Centre Fields----------------------------//
////Centre List
//static NSString* const kCentreListKey = @"CentreList";
//static NSString* const kCentreListSizeKey = @"CentreListSize";
//static NSString* const kCentreListIdKey = @"id";
//static NSString* const kCentreListValueKey = @"Value";
//static NSString* const kCentreListDescriptionKey = @"Name";
//// Centre delete
//static NSString* const kCentreListDeleteSizeKey = @"CentreListDeleteSize";
//static NSString* const kCentreListDeleteKey = @"CentreListDelete";
//static NSString* const kCentreIdDeleteKey = @"Id";
//
////--------------------------------Consult Type----------------------------//
////Consult Type Fields
//static NSString* const kConsultTypeListKey = @"ConsultTypeList";
//static NSString* const kConsultTypeListSizeKey = @"ConsultTypeListSize";
//static NSString* const kConsultTypeIdKey = @"id";
//static NSString* const kConsultTypeValueKey = @"Value";
//static NSString* const kConsultTypeNameKey = @"Name";
////Consult delete
//static NSString* const kConsultTypeDeleteSizeKey = @"ConsultTypeListDeleteSize";
//static NSString* const kConsultTypeDeleteKey = @"ConsultTypeListDelete";
//static NSString* const kConsultTypeIdDeleteKey = @"Id";
//
//--------------------------------PlaceOfVisit----------------------------//
// PlaceOfVisit list
static NSString* const kPlaceOfVisitListKey = @"InsuranceList";
static NSString* const kPlaceOfVisitListSizeKey = @"InsuranceListSize";
static NSString* const kPlaceOfVisitIdKey = @"id";
static NSString* const kPlaceOfVisitValueKey = @"Value";
static NSString* const kPlaceOfVisitDescriptionKey = @"Name";
// PlaceOfVisit delete
static NSString* const kPlaceOfVisitListDeleteSizeKey = @"InsuranceListDeleteSize";
static NSString* const kPlaceOfVisitListDeleteKey = @"InsuranceListDelete";
static NSString* const kPlaceOfVisitIdDeleteKey = @"Id";

//--------------------------------Duration----------------------------//
// Duration list
static NSString* const kDurationListKey = @"DurationList";
static NSString* const kDurationListSizeKey = @"DurationListSize";
static NSString* const kDurationIdKey = @"id";
static NSString* const kDurationValueKey = @"Value";
static NSString* const kDurationDescriptionKey = @"Name";
// Duration delete
static NSString* const kDurationListDeleteSizeKey = @"DurationListDeleteSize";
static NSString* const kDurationListDeleteKey = @"DurationListDelete";
static NSString* const kDurationIdDeleteKey = @"Id";

//--------------------------------DoseFrequency----------------------------//
// DoseFrequency list
static NSString* const kDoseFrequencyListKey = @"FrequenciesList";
static NSString* const kDoseFrequencyListSizeKey = @"FrequenciesListSize";
static NSString* const kDoseFrequencyIdKey = @"id";
static NSString* const kDoseFrequencyValueKey = @"Value";
static NSString* const kDoseFrequencyDescriptionKey = @"Name";
// DoseFrequency delete
static NSString* const kDoseFrequencyListDeleteSizeKey = @"FrequenciesListDeleteSize";
static NSString* const kDoseFrequencyListDeleteKey = @"FrequenciesListDelete";
static NSString* const kDoseFrequencyIdDeleteKey = @"Id";

//--------------------------------InsuranceList----------------------------//
// InsuranceList list
static NSString* const kInsuranceListKey = @"PatientInsuranceList";
static NSString* const kInsuranceListSizeKey = @"PatientInsuranceListSize";
static NSString* const kInsuranceIdKey = @"id";
static NSString* const kInsuranceValueKey = @"Value";
static NSString* const kInsuranceDescriptionKey = @"Name";
// InsuranceList delete
static NSString* const kInsuranceListDeleteSizeKey = @"PatientInsuranceListDeleteSize";
static NSString* const kInsuranceListDeleteKey = @"PatientInsuranceListDelete";
static NSString* const kInsuranceIdDeleteKey = @"Id";


//--------------------------------DrugIndicator----------------------------//
// DrugIndicator list
static NSString* const kDrugIndicatorListKey = @"DrugIndicatorList";
static NSString* const kDrugIndicatorListSizeKey = @"DrugIndicatorListSize";
static NSString* const kDrugIndicatorIdKey = @"id";
static NSString* const kDrugIndicatorValueKey = @"Value";
static NSString* const kDrugIndicatorDescriptionKey = @"Name";
// DrugIndicator delete
static NSString* const kDrugIndicatorListDeleteSizeKey = @"DrugIndicatorListDeleteSize";
static NSString* const kDrugIndicatorListDeleteKey = @"DrugIndicatorListDelete";
static NSString* const kDrugIndicatorIdDeleteKey = @"Id";


//--------------------------------DrugReimbersement----------------------------//
// DrugReimbersement list
static NSString* const kDrugReimbersementListKey = @"DrugReimbursementList";
static NSString* const kDrugReimbersementListSizeKey = @"DDrugReimbursementListSize";
static NSString* const kDrugReimbersementIdKey = @"id";
static NSString* const kDrugReimbersementValueKey = @"Value";
static NSString* const kDrugReimbersementDescriptionKey = @"Name";
// DrugReimbersement delete
static NSString* const kDrugReimbersementListDeleteSizeKey = @"DrugReimbursementListDeleteSize";
static NSString* const kDrugReimbersementListDeleteKey = @"DrugReimbursementListDelete";
static NSString* const kDrugReimbersementIdDeleteKey = @"Id";

//--------------------------------Provinces----------------------------//
// Provinces list
static NSString* const kProvincesListKey = @"ProvincesList";
static NSString* const kProvincesListSizeKey = @"ProvincesListSize";
static NSString* const kProvincesIdKey = @"id";
static NSString* const kProvincesValueKey = @"Value";
static NSString* const kProvincesDescriptionKey = @"Name";
// Provinces delete
static NSString* const kProvincesListDeleteSizeKey = @"ProvincesListDeleteSize";
static NSString* const kProvincesListDeleteKey = @"ProvincesListDelete";
static NSString* const kProvincesIdDeleteKey = @"Id";




//--------------------------------Speciality----------------------------//
// Speciality list
static NSString* const kSpecialityListKey = @"SpecialitiesList";
static NSString* const kSpecialityListSizeKey = @"SpecialitiesListSize";
static NSString* const kSpecialityIdKey = @"id";
static NSString* const kSpecialityValueKey = @"Value";
static NSString* const kSpecialityDescriptionKey = @"Name";
// Speciality delete
static NSString* const kSpecialityListDeleteSizeKey = @"SpecialitiesListDeleteSize";
static NSString* const kSpecialityListDeleteKey = @"SpecialitiesListDelete";
static NSString* const kSpecialityIdDeleteKey = @"Id";


//OtherSpeciality----------------------------//
// OtherSpeciality list
static NSString* const kOtherSpecialityListKey = @"OtherSpecialitiesList";
static NSString* const kOtherSpecialityListSizeKey = @"OtherSpecialitiesListSize";
static NSString* const kOtherSpecialityIdKey = @"id";
static NSString* const kOtherSpecialityValueKey = @"Value";
static NSString* const kOtherSpecialityDescriptionKey = @"Name";
// OtherSpeciality delete
static NSString* const kOtherSpecialityListDeleteSizeKey = @"OtherSpecialitiesListDeleteSize";
static NSString* const kOtherSpecialityListDeleteKey = @"OtherSpecialitiesListDelete";
static NSString* const kOtherSpecialityIdDeleteKey = @"Id";


//--------------------------------TherapyElection----------------------------//
// TherapyElection list
static NSString* const kTherapyElectionListKey = @"TherapyElectionList";
static NSString* const kTherapyElectionListSizeKey = @"TherapyElectionListSize";
static NSString* const kTherapyElectionIdKey = @"id";
static NSString* const kTherapyElectionValueKey = @"Value";
static NSString* const kTherapyElectionDescriptionKey = @"Name";
// TherapyElection delete
static NSString* const kTherapyElectionListDeleteSizeKey = @"TherapyElectionListDeleteSize";
static NSString* const kTherapyElectionListDeleteKey = @"TherapyElectionListDelete";
static NSString* const kTherapyElectionIdDeleteKey = @"Id";

//--------------------------------TherapyType----------------------------//
// TherapyType list
static NSString* const kTherapyTypeListKey = @"TherapyTypeList";
static NSString* const kTherapyTypeListSizeKey = @"TherapyTypeListSize";
static NSString* const kTherapyTypeIdKey = @"id";
static NSString* const kTherapyTypeValueKey = @"Value";
static NSString* const kTherapyTypeDescriptionKey = @"Name";
// TherapyType delete
static NSString* const kTherapyTypeListDeleteSizeKey = @"TherapyTypeListDeleteSize";
static NSString* const kTherapyTypeListDeleteKey = @"TherapyTypeListDelete";
static NSString* const kTherapyTypeIdDeleteKey = @"Id";


//--------------------------------DosageUnits----------------------------//
// DosageUnits list
static NSString* const kDosageUnitsListKey = @"DosageUnitsList";
static NSString* const kDosageUnitsListSizeKey = @"DosageUnitsListSize";
static NSString* const kDosageUnitsIdKey = @"id";
static NSString* const kDosageUnitsValueKey = @"Value";
static NSString* const kDosageUnitsDescriptionKey = @"Name";
// DosageUnits delete
static NSString* const kDosageUnitsListDeleteSizeKey = @"DosageUnitsListDeleteSize";
static NSString* const kDosageUnitsListDeleteKey = @"DosageUnitsListDelete";
static NSString* const kDosageUnitsIdDeleteKey = @"Id";


//--------------------------------Universities----------------------------//
//// Universities list
//static NSString* const kUniversitiesListKey = @"UniversitiesList";
//static NSString* const kUniversitiesListSizeKey = @"UniversitiesListSize";
//static NSString* const kUniversitiesIdKey = @"id";
//static NSString* const kUniversitiesValueKey = @"Value";
//static NSString* const kUniversitiesDescriptionKey = @"Name";
//// Universities delete
//static NSString* const kUniversitiesListDeleteSizeKey = @"UniversitiesListDeleteSize";
//static NSString* const kUniversitiesListDeleteKey = @"UniversitiesListDelete";
//static NSString* const kUniversitiesIdDeleteKey = @"Id";
//
//--------------------------------TherapyReasons----------------------------//
// TherapyReasons list
//static NSString* const kTherapyReasonsListKey = @"TherapyReasonsList";
//static NSString* const kTherapyReasonsListSizeKey = @"TherapyReasonsListSize";
//static NSString* const kTherapyReasonsIdKey = @"id";
//static NSString* const kTherapyReasonsValueKey = @"Value";
//static NSString* const kTherapyReasonsDescriptionKey = @"Name";
//// TherapyReasons delete
//static NSString* const kTherapyReasonsListDeleteSizeKey = @"TherapyReasonsListDeleteSize";
//static NSString* const kTherapyReasonsListDeleteKey = @"TherapyReasonsListDelete";
//static NSString* const kTherapyReasonsIdDeleteKey = @"Id";
//

//Ravi_Bukka: Removed for New CheckDataVersion Implementation
/*
 // Error domain
 static NSString* const kErrorDomain  = @"es.lumata.UpdateDataRequest";
 
 // Request Name
 static NSString* const kRequestName  = @"checkDataVersion";
 // Content dictionary
 static NSString* const kUserIdKey = @"userId";
 static NSString* const kLastUpdatedTimeKey = @"lastUpdatedTime"; // Format 2010/12/12
 static NSString* const kLastUpdatedTimeDateFormat = @"YYYY/MM/dd HH:mm:ss";
 static NSString* const kOffset = @"offset";
 static NSString* const kOffsetInitialValue = @"0";
 // Response
 // Diagnosis list
 static NSString* const kDiagnosisListSizeKey = @"diagnosisListSize";
 static NSString* const kDiagnosisListKey = @"diagnosisList";
 static NSString* const kDiagnosisKey = @"diagnosis";
 static NSString* const kDiagnosisIdKey = @"diagnosisId";
 static NSString* const kDiagnosisNameKey = @"diagnosisName";
 // Diagnosis delete
 static NSString* const kDiagnosisListDeleteSizeKey = @"diagnosisListDeleteSize";
 static NSString* const kDiagnosisListDeleteKey = @"diagnosisListDelete";
 static NSString* const kDiagnosisIdDeleteKey = @"diagnosisIdDelete";
 // Medicament
 static NSString* const kMedicineListSizeKey = @"medicineListSize";
 static NSString* const kMedicineListKey = @"medicineList";
 static NSString* const kMedicineKey = @"medicine";
 static NSString* const kMedicineIdKey = @"medicineId";
 static NSString* const kMedicineNameKey = @"medicineName";
 static NSString* const kParametersKey = @"parameters";
 static NSString* const kParameterKey = @"parameter";
 static NSString* const kDoseUnitIdKey = @"doseUnitId";
 static NSString* const kFilterKey = @"filter";
 static NSString* const kFormatKey = @"format";
 static NSString* const kImsCodeKey = @"imsCode";
 // Medicament delete
 static NSString* const kMedicineListDeleteKey = @"medicineListDelete";
 static NSString* const kMedicineIdDeleteKey = @"medicineIdDelete";
 // Effects list
 static NSString* const kEffectsListKey = @"effectsList";
 static NSString* const kEffectsListSizeKey = @"effectsListSize";
 static NSString* const kEffectIdKey = @"effectId";
 static NSString* const kEffectKey = @"effect";
 static NSString* const kEffectDescriptionKey = @"effectDescription";
 // Effects delete
 static NSString* const kEffectsListDeleteSizeKey = @"effectsListDeleteSize";
 static NSString* const kEffectsListDeleteKey = @"effectsListDelete";
 static NSString* const kEffectsIdDeleteKey = @"effectsIdDelete";
 
 */

@implementation UpdateDataRequest

//-(AgeSync*) ageFromDictionary:(NSDictionary*)ageDictionary {
//    AgeSync* age = [[AgeSync alloc] init];
//    age.identifier = [ageDictionary objectForKey:kAgeValueKey];
//    age.name = [ageDictionary objectForKey:kAgeNameKey];
//    return [age autorelease];
//}
//
//
//-(CentreListSync*) centreFromDictionary:(NSDictionary*)centreDictionary {
//    CentreListSync* centre = [[CentreListSync alloc] init];
//    centre.identifier = [centreDictionary objectForKey:kCentreListValueKey];
//    centre.name = [centreDictionary objectForKey:kCentreListDescriptionKey];
//    return [centre autorelease];
//}
//
//-(ConsultTypeSync*) consultFromDictionary:(NSDictionary*)consultDictionary {
//    ConsultTypeSync* consult = [[ConsultTypeSync alloc]init];
//    consult.identifier = [consultDictionary objectForKey:kConsultTypeIdKey];
//    consult.name = [consultDictionary objectForKey:kConsultTypeNameKey];
//    return [consult autorelease];
//}

//Deepak_Carpenter: Added for UnitType
//-(UnitTypeSync*) UnitTypeFromDictionary:(NSDictionary*)UnitTypeDictionary {
//    NSLog(@"UnitType Dictionary %@",UnitTypeDictionary);
//    UnitTypeSync* unitType = [[UnitTypeSync alloc]init];
//    unitType.identifier = [UnitTypeDictionary objectForKey:kUnitTypeIdKey];
//    unitType.name = [UnitTypeDictionary objectForKey:kUnitTypeNameKey];
//    return [unitType autorelease];
//}

-(DiagnosisTypeSync*) diagnosisFromDictionary:(NSDictionary*)diagnosisDictionary {
    
    NSLog(@"Diagnosis dictionary -- %@", diagnosisDictionary);
    
    DiagnosisTypeSync* diagnosis = [[DiagnosisTypeSync alloc] init];
    diagnosis.identifier = [diagnosisDictionary objectForKey:kDiagnosisDFCCKey];
    diagnosis.name = [diagnosisDictionary objectForKey:kDiagnosisNameKey];
    return diagnosis ;
}

-(EffectsSync*) effectsFromDictionary:(NSDictionary*)effectsDictionary {
    EffectsSync* effect = [[EffectsSync alloc] init];
    effect.identifier = [effectsDictionary objectForKey:kEffectValueKey];
    effect.name = [effectsDictionary objectForKey:kEffectDescriptionKey];
    return effect ;
}


-(PresentationSync*) presentationFromDictionary:(NSDictionary*)presentationDictionary {
    PresentationSync* presentation = [[PresentationSync alloc] init];
    presentation.identifier = [presentationDictionary objectForKey:kFCCCodeKey];
    presentation.name = [presentationDictionary objectForKey:kLocalFormKey];
    presentation.dosageUnitId = [presentationDictionary objectForKey:kFormDescriptionKey];
    return presentation ;
}

-(MedicamentSync*) medicamentFromDictionary:(NSDictionary*)medicamentDictionary {
    //Ravi_Bukka: Commented for new Checkdataversion implementation.
    
    //    MedicamentSync* medicamentSync = [[MedicamentSync alloc] init];
    //    medicamentSync.name = [medicamentDictionary objectForKey:kMedicineNameKey];
    //    medicamentSync.identifier = [medicamentDictionary objectForKey:kMedicineIdKey];
    //    medicamentSync.productTypeId = [medicamentDictionary objectForKey:kFilterKey];
    //    NSMutableArray* presentationArray = [[NSMutableArray alloc] init];
    //    // Presentation form
    //    NSArray* obtainedPresentationInfo = [[medicamentDictionary objectForKey:kParametersKey] arrayForKey:kParameterKey];
    //    // Retrieved info can be an array or a single dictionary element
    //    for (NSDictionary* presentationDictionary in obtainedPresentationInfo) {
    //        [presentationArray addObject:[self presentationFromDictionary:presentationDictionary]];
    //    }
    //    medicamentSync.presentations = presentationArray;
    //    [presentationArray release];
    //    return [medicamentSync autorelease];
    
    
    //Ravi_Bukka: Added for new Checkdataversion implementation.
    
    MedicamentSync* medicamentSync = [[MedicamentSync alloc] init];
    medicamentSync.name = [medicamentDictionary objectForKey:kMedicineNameKey];
    medicamentSync.identifier = [medicamentDictionary objectForKey:kMedicineIdKey];
    medicamentSync.productTypeId = [medicamentDictionary objectForKey:kProductTypeKey];
    NSMutableArray* presentationArray = [[NSMutableArray alloc] init];
    
    // Presentation form
    
    [presentationArray addObject:[self presentationFromDictionary:medicamentDictionary]];
    
    medicamentSync.presentations = presentationArray;
    return medicamentSync ;
    
}
//Deepak_Carpenter: Added for checkDataVersion

//--------------DoseFrequency-------------//
-(FrequencySync*) FrequencyFromDictionary:(NSDictionary*)FrequencyDictionary {
    FrequencySync* frequency = [[FrequencySync alloc] init];
    frequency.identifier = [FrequencyDictionary objectForKey:kDoseFrequencyValueKey];
    frequency.name = [FrequencyDictionary objectForKey:kDoseFrequencyDescriptionKey];
    return frequency ;
}

//--------------Duration-------------//
-(DurationTypeSync*) durationTypeFromDictionary:(NSDictionary*)durationTypeDictionary {
    DurationTypeSync* durationType = [[DurationTypeSync alloc] init];
    durationType.identifier = [durationTypeDictionary objectForKey:kDurationValueKey];
    durationType.name = [durationTypeDictionary objectForKey:kDurationDescriptionKey];
    return durationType;
}

//--------------Insurance-------------//
-(InsuranceSync*) InsuranceFromDictionary:(NSDictionary*)InsuranceDictionary {
    InsuranceSync* insurance = [[InsuranceSync alloc] init];
    insurance.identifier = [InsuranceDictionary objectForKey:kInsuranceValueKey];
    insurance.name = [InsuranceDictionary objectForKey:kInsuranceDescriptionKey];
    return insurance;
}
//--------------DrugIndicator-------------//
-(DrugIndicatorSync*) DrugIndicatorFromDictionary:(NSDictionary*)DrugIndicatorDictionary {
    DrugIndicatorSync* drugIndicator = [[DrugIndicatorSync alloc] init];
    drugIndicator.identifier = [DrugIndicatorDictionary objectForKey:kDrugIndicatorValueKey];
    drugIndicator.name = [DrugIndicatorDictionary objectForKey:kDrugIndicatorDescriptionKey];
    return drugIndicator;
}

//--------------DrugReimbersement-------------//
-(DrugReimbursementSync*) DrugReimbursementFromDictionary:(NSDictionary*)DrugReimbursementDictionary {
    DrugReimbursementSync* drugReimbursement = [[DrugReimbursementSync alloc] init];
    drugReimbursement.identifier = [DrugReimbursementDictionary objectForKey:kDrugReimbersementValueKey];
    drugReimbursement.name = [DrugReimbursementDictionary objectForKey:kDrugReimbersementDescriptionKey];
    return drugReimbursement ;
}

////--------------PlaceOfVisit-------------//
-(PlaceOfVisitSync*) placeOfVisitFromDictionary:(NSDictionary*)placeOfVisitDictionary {
    PlaceOfVisitSync* placeOfVisit = [[PlaceOfVisitSync alloc] init];
    placeOfVisit.identifier = [placeOfVisitDictionary objectForKey:kPlaceOfVisitValueKey];
    placeOfVisit.name = [placeOfVisitDictionary objectForKey:kPlaceOfVisitDescriptionKey];
    return placeOfVisit;
}
//--------------Province-------------//
-(ProvinceSync*) ProvinceFromDictionary:(NSDictionary*)ProvinceDictionary {
    ProvinceSync* province = [[ProvinceSync alloc] init];
    province.identifier = [ProvinceDictionary objectForKey:kProvincesValueKey];
    province.name = [ProvinceDictionary objectForKey:kProvincesDescriptionKey];
    return province;
}

//--------------Speciality-------------//
-(SpecialitiesSync*) specialitiesFromDictionary:(NSDictionary*)specialitiesDictionary {
    SpecialitiesSync* specialities = [[SpecialitiesSync alloc] init];
    specialities.identifier = [specialitiesDictionary objectForKey:kSpecialityValueKey];
    specialities.name = [specialitiesDictionary objectForKey:kSpecialityDescriptionKey];
    return specialities ;
}
//--------------OtherSpeciality-------------//
-(OtherSpecialitiesSync*) otherSpecialitiesFromDictionary:(NSDictionary*)otherSpecialitiesDictionary {
    OtherSpecialitiesSync* otherSpecialities = [[OtherSpecialitiesSync alloc] init];
    otherSpecialities.identifier = [otherSpecialitiesDictionary objectForKey:kOtherSpecialityValueKey];
    otherSpecialities.name = [otherSpecialitiesDictionary objectForKey:kOtherSpecialityDescriptionKey];
    return otherSpecialities;
}

//--------------TherapyType-------------//
-(TherapyTypeSync*) TherapyTypeFromDictionary:(NSDictionary*)TherapyTypeDictionary {
    TherapyTypeSync* therapyType = [[TherapyTypeSync alloc] init];
    therapyType.identifier = [TherapyTypeDictionary objectForKey:kTherapyTypeValueKey];
    therapyType.name = [TherapyTypeDictionary objectForKey:kTherapyTypeDescriptionKey];
    return therapyType;
}

//--------------DosageUnits-------------//
-(UnitTypeSync*) DosageUnitsFromDictionary:(NSDictionary*)DosageUnitsDictionary {
    UnitTypeSync* dosageUnits = [[UnitTypeSync alloc] init];
    dosageUnits.identifier = [DosageUnitsDictionary objectForKey:kDosageUnitsValueKey];
    dosageUnits.name = [DosageUnitsDictionary objectForKey:kDosageUnitsDescriptionKey];
    return dosageUnits ;
}

-(UpdateDataResponse*) responseFromDictionary:(NSDictionary*)responseDictionary {
    
    NSLog(@"response dictionary === %@", responseDictionary);
    
    UpdateDataResponse* response = [[UpdateDataResponse alloc] init];
    
    //    NSMutableArray *ageArray = [[NSMutableArray alloc] init];
    //    NSMutableArray *centreArray = [[NSMutableArray alloc] init];
    //    NSMutableArray *consultArray = [[NSMutableArray alloc] init];
    NSMutableArray *desiredEffectArray = [[NSMutableArray alloc] init];
    NSMutableArray *diagnosisArray = [[NSMutableArray alloc] init];
    NSMutableArray *medicamArray = [[NSMutableArray alloc] init];
    
    //Deepak_Carpenter: Added for CheckDataVersion
    NSMutableArray *dosageUnitsArray = [[NSMutableArray alloc] init];
    NSMutableArray *drugIndicatorMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray *drugReimArray = [[NSMutableArray alloc] init];
    
    NSMutableArray *doseFrequencyMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray *durationMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray *insuranceMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray *placeofVisitMutableArray = [[NSMutableArray alloc] init];

    NSMutableArray *provincesMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray *specialitiesMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray *otherSpecialitiesMutableArray = [[NSMutableArray alloc] init];
    
     NSMutableArray *therapyTypeMutableArray = [[NSMutableArray alloc] init];
    
        NSMutableArray *dosUnitsArray = [[NSMutableArray alloc] init];
    
    
    
    NSArray *newFieldresult = [[responseDictionary objectForKey:kNewFields] arrayForKey:kNewData];
    
    NSLog(@"new fields result %@", newFieldresult);
    
    NSArray *deletedFieldresult = [[responseDictionary objectForKey:kDeletedFields] arrayForKey:kDeletedData];
    
    NSLog(@"deleted fields result %@", deletedFieldresult);
    
    
    
    
    
    
    for (NSDictionary *tempDic in newFieldresult)
    {
        //        if ( [tempDic objectForKey:kAgeListKey]){
        //
        //            NSArray *agArray = [[tempDic objectForKey:kAgeListKey] arrayForKey:kItem];
        //            NSLog(@" print age = %@", agArray);
        //            for (NSDictionary* ageDictionary in agArray) {
        //                [ageArray addObject:[self ageFromDictionary:ageDictionary]];
        //            }
        //            response.ageArray = ageArray;
        //            [ageArray release];
        //
        //        }
        //
        //        else if ( [tempDic objectForKey:kCentreListKey]){
        //
        //            NSArray *cntArray = [[tempDic objectForKey:kCentreListKey] arrayForKey:kItem];
        //            NSLog(@" print centre = %@", cntArray);
        //            for (NSDictionary* centreDictionary in cntArray) {
        //                [centreArray addObject:[self centreFromDictionary:centreDictionary]];
        //            }
        //                response.centreArray = centreArray;
        //                [centreArray release];
        //
        //        }
        //        else if ([tempDic objectForKey:kConsultTypeListKey]){
        //
        //            NSArray *cnsArray = [[tempDic objectForKey:kConsultTypeListKey] arrayForKey:kItem];
        //            NSLog(@" print consult type = %@", cnsArray);
        //            for (NSDictionary* consultDictionary in cnsArray) {
        //                [consultArray addObject:[self centreFromDictionary:consultDictionary]];
        //            }
        //            response.centreArray = consultArray;
        //            [consultArray release];
        //
        //        }
        if ([tempDic objectForKey:kEffectsListKey]){
            
            NSArray *dsrEffectArray = [[tempDic objectForKey:kEffectsListKey] arrayForKey:kItem];
            NSLog(@" print desired effect = %@", dsrEffectArray);
            for (NSDictionary* effectDictionary in dsrEffectArray) {
                [desiredEffectArray addObject:[self effectsFromDictionary:effectDictionary]];
            }
            response.effectsArray = desiredEffectArray;
            
        }
        
        
        
        else if ([tempDic objectForKey:kDiagnosisListKey]) {
            
            NSArray *diagArray = [[tempDic objectForKey:kDiagnosisListKey] arrayForKey:kItem];
            NSLog(@"print diagnosis = %@", diagArray);
            for (NSDictionary* diagnosisDictionary in diagArray) {
                [diagnosisArray addObject:[self diagnosisFromDictionary:diagnosisDictionary]];
            }
            response.diagnosisArray = diagnosisArray;
        }
        
        else if ([tempDic objectForKey:kMedicineListKey]) {
            
            NSArray *medicaArray = [[tempDic objectForKey:kMedicineListKey] arrayForKey:kItem];
            NSLog(@"print medicine = %@", medicaArray);
            for (NSDictionary* medicamentDictionary in medicaArray) {
                [medicamArray addObject:[self medicamentFromDictionary:medicamentDictionary]];
            }
            response.medicamentArray = medicamArray;
        }
        //Deepak_Carpenter: Added for checkDataVersion
        //DosageUnits
//        else if ([tempDic objectForKey:kUnitTypeListKey]) {
//            
//            NSArray *unitArray = [[tempDic objectForKey:kUnitTypeListKey] arrayForKey:kItem];
//            NSLog(@"print diagnosis = %@", unitArray);
//            for (NSDictionary* unitsDictionary in unitArray) {
//                [dosageUnitsArray addObject:[self UnitTypeFromDictionary:unitsDictionary]];
//            }
//            response.unitTypeArray = dosageUnitsArray;
//            [dosageUnitsArray release];
//        }
        //-------------------------AgeType-----------------------//
//        else  if ([tempDic objectForKey:kAgeTypesListKey]){
//            
//            NSArray *aTArray = [[tempDic objectForKey:kAgeTypesListKey] arrayForKey:kItem];
//            NSLog(@" print ageTypes = %@", aTArray);
//            for (NSDictionary* ageTypeDictionary in aTArray) {
//                [ageTypeMutableArray addObject:[self AgeTypeFromDictionary:ageTypeDictionary]];
//            }
//            response.ageTypeArray = ageTypeMutableArray;
//            [ageTypeMutableArray release];
//            
//        }
        //-------------------------centre-----------------------//
//        else  if ([tempDic objectForKey:kCentreListKey]){
//            
//            NSArray *centreArray = [[tempDic objectForKey:kCentreListKey] arrayForKey:kItem];
//            NSLog(@" print centre = %@", centreArray);
//            for (NSDictionary* centreDictionary in centreArray) {
//                [centreTypeMutableArray addObject:[self CentreFromDictionary:centreDictionary]];
//            }
//            response.centreArray = centreTypeMutableArray;
//            [centreTypeMutableArray release];
//            
//        }
        //-------------------------consult type-----------------------//
//        else  if ([tempDic objectForKey:kConsultTypeListKey]){
//            
//            NSArray *consultTypeArray = [[tempDic objectForKey:kConsultTypeListKey] arrayForKey:kItem];
//            NSLog(@" print consult type = %@", consultTypeArray);
//            for (NSDictionary* consultTypeDictionary in consultTypeArray) {
//                [consultTypeMutableArray addObject:[self ConsultTypeFromDictionary:consultTypeDictionary]];
//            }
//            response.consultTypeArray = consultTypeMutableArray;
//            [consultTypeMutableArray release];
//            
//        }
//------------------------DrugIndicator-----------------------//
//        else if ([tempDic objectForKey:kUnitTypeListKey]) {
//            
//            NSArray *unitArray = [[tempDic objectForKey:kUnitTypeListKey] arrayForKey:kItem];
//            NSLog(@"print diagnosis = %@", unitArray);
//            for (NSDictionary* unitsDictionary in unitArray) {
//                [dosageUnitsArray addObject:[self UnitTypeFromDictionary:unitsDictionary]];
//            }
//            response.unitTypeArray = dosageUnitsArray;
//            [dosageUnitsArray release];
//        }
//------------------------Frequency-----------------------//
        else  if ([tempDic objectForKey:kDoseFrequencyListKey]){
            
            NSArray *doseFrequncyArray = [[tempDic objectForKey:kDoseFrequencyListKey] arrayForKey:kItem];
            NSLog(@" print doseFrequency = %@", doseFrequncyArray);
            for (NSDictionary* doseFrequencyDictionary in doseFrequncyArray) {
                [doseFrequencyMutableArray addObject:[self FrequencyFromDictionary:doseFrequencyDictionary]];
            }
            response.frequencyArray = doseFrequencyMutableArray;
            
        }
//-------------------------Duration-----------------------//
        else if ([tempDic objectForKey:kDurationListKey]){
            
            NSArray *duraArray = [[tempDic objectForKey:kDurationListKey] arrayForKey:kItem];
            NSLog(@" print duration = %@", duraArray);
            for (NSDictionary* durationDictionary in duraArray) {
                [durationMutableArray addObject:[self durationTypeFromDictionary:durationDictionary]];
            }
            response.durationTypeArray = durationMutableArray;
            
        }
        
//-------------------------DrugIndicator-----------------------//
        else if ([tempDic objectForKey:kDrugIndicatorListKey]){
            
            NSArray *drugIndicatorArray = [[tempDic objectForKey:kDrugIndicatorListKey] arrayForKey:kItem];
            NSLog(@" print DrugIndicator = %@", drugIndicatorArray);
            for (NSDictionary* drugIndicatorDictionary in drugIndicatorArray) {
                [drugIndicatorMutableArray addObject:[self DrugIndicatorFromDictionary:drugIndicatorDictionary]];
            }
            response.drugIndicatorArray = drugIndicatorMutableArray;
            
    }
        
//-------------------------DrugReimbersement-----------------------//
        else if ([tempDic objectForKey:kDrugReimbersementListKey]){
            
            NSArray *drugReimbersementArray = [[tempDic objectForKey:kDrugReimbersementListKey] arrayForKey:kItem];
            NSLog(@" print DrugReimbersement = %@", drugReimbersementArray);
            for (NSDictionary* drugReimbersementDictionary in drugReimbersementArray) {
                [drugReimArray addObject:[self DrugReimbursementFromDictionary:drugReimbersementDictionary]];
            }
            response.drugReimbursementArray= drugReimArray;
            
        }
        //-------------------------Gender-----------------------//
//        else if ([tempDic objectForKey:kGendersListKey]){
//            
//            NSArray *gArray = [[tempDic objectForKey:kGendersListKey] arrayForKey:kItem];
//            NSLog(@" print gender = %@", gArray);
//            for (NSDictionary* genderDictionary in gArray) {
//                [gendersMutableArray addObject:[self GenderFromDictionary:genderDictionary]];
//            }
//            response.genderArray = gendersMutableArray;
//            [gendersMutableArray release];
//            
//        }
        //-------------------------Insurance-----------------------//
        else if ([tempDic objectForKey:kInsuranceListKey]){
            
            NSArray *insArray = [[tempDic objectForKey:kInsuranceListKey] arrayForKey:kItem];
            NSLog(@" print insurance = %@", insArray);
            for (NSDictionary* insuDictionary in insArray) {
                [insuranceMutableArray addObject:[self InsuranceFromDictionary:insuDictionary]];
            }
            response.patientInsuranceArray = insuranceMutableArray;
            
        }
        else if ([tempDic objectForKey:kPlaceOfVisitListKey]){
            
            NSArray *pOVArray = [[tempDic objectForKey:kPlaceOfVisitListKey] arrayForKey:kItem];
            NSLog(@" print pov = %@", pOVArray);
            for (NSDictionary* pOVDictionary in pOVArray) {
                [placeofVisitMutableArray addObject:[self placeOfVisitFromDictionary:pOVDictionary]];
            }
            response.placeOfVisitArray = placeofVisitMutableArray;
            //        [placeofVisitMutableArray release];
            
        }
        //-------------------------PathologyType-----------------------//
//        else if ([tempDic objectForKey:kPathologyTypeListKey]){
//            
//            NSArray *pTArray = [[tempDic objectForKey:kPathologyTypeListKey] arrayForKey:kItem];
//            NSLog(@" print pathologyType = %@", pTArray);
//            for (NSDictionary* pTDictionary in pTArray) {
//                [pathologyTypeMutableArray addObject:[self PathologyTypeFromDictionary:pTDictionary]];
//            }
//            response.pathologyArray = pathologyTypeMutableArray;
//            [pathologyTypeMutableArray release];
//            
//        }
    //-------------------------Province-----------------------//
        else if ([tempDic objectForKey:kProvincesListKey]){
            
            NSArray *provArray = [[tempDic objectForKey:kProvincesListKey] arrayForKey:kItem];
            NSLog(@" print province = %@", provArray);
            for (NSDictionary* provinceDictionary in provArray) {
                [provincesMutableArray addObject:[self ProvinceFromDictionary:provinceDictionary]];
            }
            response.provinceArray = provincesMutableArray;
            
        }
        //-------------------------Recurrance-----------------------//
//        else if ([tempDic objectForKey:kRecurranceListKey]){
//            
//            NSArray *recArray = [[tempDic objectForKey:kRecurranceListKey] arrayForKey:kItem];
//            NSLog(@" print recurrance = %@", recArray);
//            for (NSDictionary* recDictionary in recArray) {
//                [recurranceMutableArray addObject:[self RecurrenceFromDictionary:recDictionary]];
//            }
//            response.recurrenceArray = recurranceMutableArray;
//            [recurranceMutableArray release];
//            
//        }
        //-------------------------Smoker-----------------------//
//        else if ([tempDic objectForKey:kSmokerListKey]){
//            
//            NSArray *smoArray = [[tempDic objectForKey:kSmokerListKey] arrayForKey:kItem];
//            NSLog(@" print smoker = %@", smoArray);
//            for (NSDictionary* smoDictionary in smoArray) {
//                [smokerMutableArray addObject:[self SmokerFromDictionary:smoDictionary]];
//            }
//            response.smokerArray = smokerMutableArray;
//            [smokerMutableArray release];
//            
//        }
        //-------------------------Specialities-----------------------//
        else if ([tempDic objectForKey:kSpecialityListKey]){
            
            NSArray *splArray = [[tempDic objectForKey:kSpecialityListKey] arrayForKey:kItem];
            NSLog(@" print speciality = %@", splArray);
            for (NSDictionary* splDictionary in splArray) {
                [specialitiesMutableArray addObject:[self specialitiesFromDictionary:splDictionary]];
            }
            response.specialitiesArray = specialitiesMutableArray;
            
        }
        
        //-------------------------OtherSpecialities-----------------------//
        else if ([tempDic objectForKey:kOtherSpecialityListKey]){
            
            NSArray *otherSplArray = [[tempDic objectForKey:kOtherSpecialityListKey] arrayForKey:kItem];
            NSLog(@" print speciality = %@", otherSplArray);
            for (NSDictionary* OtherSplDictionary in otherSplArray) {
                [otherSpecialitiesMutableArray addObject:[self otherSpecialitiesFromDictionary:OtherSplDictionary]];
            }
            response.otherSpecialitiesArray = otherSpecialitiesMutableArray;
            
        }
        //-------------------------TherapySelection-----------------------//
//        else if ([tempDic objectForKey:kTherapyElectionListKey]){
//            
//            NSArray *teArray = [[tempDic objectForKey:kTherapyElectionListKey] arrayForKey:kItem];
//            NSLog(@" print therapy Election = %@", teArray);
//            for (NSDictionary* therapyElectionDictionary in teArray) {
//                [therapyElectionMutableArray addObject:[self TherapyElectionFromDictionary:therapyElectionDictionary]];
//            }
//            response.therapyElectionArray = therapyElectionMutableArray;
//            [therapyElectionMutableArray release];
//            
//        }
        //-------------------------TherapyReasons-----------------------//
//        else if ([tempDic objectForKey:kTherapyReasonsListKey]){
//            
//            NSArray *therReasonArray = [[tempDic objectForKey:kTherapyReasonsListKey] arrayForKey:kItem];
//            NSLog(@" print TherapyReason = %@", therReasonArray);
//            for (NSDictionary* therReasonDictionary in therReasonArray) {
//                [therapyReasonsMutableArray addObject:[self TherapyReasonsFromDictionary:therReasonDictionary]];
//            }
//            response.therapyReasonsArray = therapyReasonsMutableArray;
//            [therapyReasonsMutableArray release];
//            
//        }
        
        
    //-------------------------TherapyType-----------------------//
        else if ([tempDic objectForKey:kTherapyTypeListKey]){
            
            NSArray *therArray = [[tempDic objectForKey:kTherapyTypeListKey] arrayForKey:kItem];
            NSLog(@" print TherapyType = %@", therArray);
            for (NSDictionary* therDictionary in therArray) {
                [therapyTypeMutableArray addObject:[self TherapyTypeFromDictionary:therDictionary]];
            }
            response.therapyTypeArray = therapyTypeMutableArray;
            
        }
        
        //-------------------------DosageUnits-----------------------//
        else if ([tempDic objectForKey:kDosageUnitsListKey]){
            
            NSArray *UnitssArray = [[tempDic objectForKey:kDosageUnitsListKey] arrayForKey:kItem];
            NSLog(@" print UNITS effect = %@", UnitssArray);
            for (NSDictionary* dosageUnitsDictionary in UnitssArray) {
                [dosUnitsArray addObject:[self DosageUnitsFromDictionary:dosageUnitsDictionary]];
            }
            response.dosageUnitsArray = dosUnitsArray;
            //            [dosUnitsArray release];
            
        }

        
    }
    
    for (NSDictionary *tempDic in deletedFieldresult) {
        
        
        if ( [tempDic objectForKey:kEffectsListDeleteKey]){
            
            NSArray *effectDelArray = [tempDic objectForKey:kEffectsIdDeleteKey];
            NSLog(@" print deleted effects = %@", effectDelArray);
            response.effectsDeleteArray = effectDelArray;
            
        }
        
        else  if ( [tempDic objectForKey:kMedicineListDeleteKey]){
            
            NSArray *mediDelArray = [tempDic objectForKey:kMedicineIdDeleteKey];
            NSLog(@" print deleted effects = %@", mediDelArray);
            response.medicamentDeleteArray = mediDelArray;
            
        }
        
        else  if ( [tempDic objectForKey:kDiagnosisListDeleteKey]){
            
            NSArray *diagDelArray = [tempDic objectForKey:kDiagnosisIdDeleteKey];
            NSLog(@" print deleted effects = %@", diagDelArray);
            response.diagnosisDeleteArray = diagDelArray;
            
        }
        
        else  if ( [tempDic objectForKey:kDosageUnitsListDeleteKey]){
            
            NSArray *dunitsArray = [tempDic objectForKey:kDosageUnitsIdDeleteKey];
            NSLog(@" print deleted DosageUnits = %@", dunitsArray);
            response.dosageUnitsDeleteArray = dunitsArray;
            
        }

        else  if ( [tempDic objectForKey:kDoseFrequencyListDeleteKey]){
            
            NSArray *freqArray = [tempDic objectForKey:kDoseFrequencyIdDeleteKey];
            NSLog(@" print deleted centreType = %@", freqArray);
            response.frequencyDeleteArray = freqArray;
            
        }
        else  if ( [tempDic objectForKey:kDurationListDeleteKey]){
            
            NSArray *duraArray = [tempDic objectForKey:kDurationIdDeleteKey];
            NSLog(@" print deleted duration = %@", duraArray);
            response.durationTypeDeleteArray = duraArray;
            
        }
        else  if ( [tempDic objectForKey:kDrugIndicatorListDeleteKey]){
            
            NSArray *drugIndicatorArray = [tempDic objectForKey:kDrugIndicatorIdDeleteKey];
            NSLog(@" print deleted DrugIndicator = %@", drugIndicatorArray);
            response.drugIndicatorDeleteArray = drugIndicatorArray;
            
        }
        //kDrugReimbersementListDeleteKey
        else  if ( [tempDic objectForKey:kDrugReimbersementListDeleteKey]){
            
            NSArray *drugReimbersementArray = [tempDic objectForKey:kDrugReimbersementIdDeleteKey];
            NSLog(@" print deleted DrugReimbersement = %@", drugReimbersementArray);
            
            //Deepak_Carpenter : Changed the array name as crash was happening coz of changed array name
            response.drugReimbursementDeleteArray = drugReimbersementArray                                                            ;
            
        }

        else  if ( [tempDic objectForKey:kInsuranceListDeleteKey]){
            
            NSArray *insuranceArra = [tempDic objectForKey:kInsuranceIdDeleteKey];
            NSLog(@" print deleted insurance = %@", insuranceArra);
            response.patientInsuranceDeleteArray = insuranceArra;
            
        }
        else  if ( [tempDic objectForKey:kPlaceOfVisitListDeleteKey]){
            
            NSArray *placeArray = [tempDic objectForKey:kPlaceOfVisitIdDeleteKey];
            NSLog(@" print deleted Place Of Visit = %@", placeArray);
            response.placeOfVisitDeleteArray = placeArray;
            
        }
        else  if ( [tempDic objectForKey:kProvincesListDeleteKey]){
            
            NSArray *provinArray = [tempDic objectForKey:kProvincesIdDeleteKey];
            NSLog(@" print deleted Province = %@", provinArray);
            response.provinceDeleteArray = provinArray;
            
        }
//        else  if ( [tempDic objectForKey:kRecurranceListDeleteKey]){
//            
//            NSArray *recurrArray = [tempDic objectForKey:kRecurranceIdDeleteKey];
//            NSLog(@" print deleted recurrance= %@", recurrArray);
//            response.recurrenceDeleteArray = recurrArray;
//            
//        }
//        else  if ( [tempDic objectForKey:kSmokerListDeleteKey]){
//            
//            NSArray *SmokerArray = [tempDic objectForKey:kSmokerIdDeleteKey];
//            NSLog(@" print deleted Smoker= %@", SmokerArray);
//            response.smokerDeleteArray = SmokerArray;
//            
//        }
        else  if ( [tempDic objectForKey:kSpecialityListDeleteKey]){
            
            NSArray *SpecArray = [tempDic objectForKey:kSpecialityIdDeleteKey];
            NSLog(@" print deleted speciality = %@", SpecArray);
            response.specialitiesDeleteArray = SpecArray;
            
        }
        else  if ( [tempDic objectForKey:kOtherSpecialityListDeleteKey]){
            
            NSArray *OtherSpecArray = [tempDic objectForKey:kOtherSpecialityIdDeleteKey];
            NSLog(@" print deleted Otherspeciality = %@", OtherSpecArray);
            response.otherSpecialitiesDeleteArray = OtherSpecArray;
            
        }

//        else  if ( [tempDic objectForKey:kTherapyElectionListDeleteKey]){
//            
//            NSArray *reaElecArray = [tempDic objectForKey:kTherapyElectionIdDeleteKey];
//            NSLog(@" print deleted therapy Election = %@", reaElecArray);
//            response.therapyElectionDeleteArray = reaElecArray;
//            
//        }
//        else  if ( [tempDic objectForKey:kTherapyReasonsListDeleteKey]){
//            
//            NSArray *tElecArray = [tempDic objectForKey:kTherapyReasonsIdDeleteKey];
//            NSLog(@" print deleted therapy Reasons = %@", tElecArray);
//            response.therapyReasonsDeleteArray = tElecArray;
//            
//        }
        
        else  if ( [tempDic objectForKey:kTherapyTypeListDeleteKey]){
            
            NSArray *tTypeArray = [tempDic objectForKey:kTherapyTypeIdDeleteKey];
            NSLog(@" print deleted therapy type = %@", tTypeArray);
            response.therapyTypeDeleteArray = tTypeArray;
            
        }
//        else  if ( [tempDic objectForKey:kUniversitiesListDeleteKey]){
//            
//            NSArray *uTypeArray = [tempDic objectForKey:kUniversitiesIdDeleteKey];
//            NSLog(@" print deleted universities = %@", uTypeArray);
//            response.universitiesDeleteArray = uTypeArray;
//
//        }
        
        
    }
    
    return response;
    
    
    
    
}



//    // Create response object
//    UpdateDataResponse* response = [[UpdateDataResponse alloc] init];
//    // Diagnosis array
//    NSMutableArray* diagnosisArray = [[NSMutableArray alloc] init];
//    NSArray* obtainedDiagnoseInfo = [[responseDictionary objectForKey:kDiagnosisListKey] arrayForKey:kDiagnosisKey];
//    // Retrieved info can be an array or a single dictionary element
//    for (NSDictionary* diagnosisDictionary in obtainedDiagnoseInfo) {
//        [diagnosisArray addObject:[self diagnosisFromDictionary:diagnosisDictionary]];
//    }
//    response.diagnosisArray = diagnosisArray;
//    [diagnosisArray release];
//    // Diagnosis delete array
//    NSArray* diagnosisDeleteArray = [[responseDictionary objectForKey:kDiagnosisListDeleteKey] arrayForKey:kDiagnosisIdDeleteKey];
//    response.diagnosisDeleteArray = diagnosisDeleteArray;
//
//    // Medicament array
//    NSMutableArray* medicamentArray = [[NSMutableArray alloc] init];
//    NSArray* obtainedMedicamentInfo = [[responseDictionary objectForKey:kMedicineListKey] arrayForKey:kMedicineKey];
//    // Retrieved info can be an array or a single dictionary element
//    for (NSDictionary* medicamentDictionary in obtainedMedicamentInfo) {
//        [medicamentArray addObject:[self medicamentFromDictionary:medicamentDictionary]];
//    }
//    response.medicamentArray = medicamentArray;
//    [medicamentArray release];
//    // Medicament delete array
//    NSArray* medicamentDeleteArray = [[responseDictionary objectForKey:kMedicineListDeleteKey] arrayForKey:kMedicineIdDeleteKey];
//    response.medicamentDeleteArray = medicamentDeleteArray;
//
//    // Desired effect array
//    NSMutableArray* desiredEffectArray = [[NSMutableArray alloc] init];
//    NSArray* obtainedDesiredEffectInfo = [[responseDictionary objectForKey:kEffectsListKey] arrayForKey:kEffectKey];
//    // Retrieved info can be an array or a single dictionary element
//    for (NSDictionary* effectDictionary in obtainedDesiredEffectInfo) {
//        [desiredEffectArray addObject:[self effectsFromDictionary:effectDictionary]];
//    }
//    response.effectsArray = desiredEffectArray;
//    [desiredEffectArray release];
//    // Medicament delete array
//    NSArray* effectDeleteArray = [[responseDictionary objectForKey:kEffectsListDeleteKey] arrayForKey:kEffectsIdDeleteKey];
//    response.effectsDeleteArray = effectDeleteArray;
//    return [response autorelease];
//}

//Ravi_Bukka: Added for CheckDataVersion Implementation
-(id) initUpdateDataRequestWithDoctorInfo:(DoctorInfo*)doctorInfo andOffset:(NSString*)offset lastUpdate:(NSDate*)lastUpdate onComplete:(UpdateDataResponseBlock)onComplete onError:(RequestErrorBlock)onError {
    self = [super init];
    if (self) {
        // Set request name
        self.name = kRequestName;
        // Add request content
        [self addParam:doctorInfo.identifier forKey:kUserIdKey];
        if (offset == nil) {
            offset = kOffsetInitialValue;
        }
        [self addParam:offset forKey:kOffset];
        // Format collaboration date
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:kLastUpdatedTimeDateFormat];
        [self addParam:[dateFormatter stringFromDate:lastUpdate] forKey:kLastUpdatedTimeKey];
        // Set response handler blocks
        self.onError = onError;
        //Deepak_Carpenter : Added to handle self as you can't handle self in block
        __weak typeof(self) weakSelf = self;

        self.onComplete = ^(NSDictionary* responseDictionary)  {
            NSString* offset = [responseDictionary objectForKey:kOffset];
            onComplete([weakSelf responseFromDictionary:responseDictionary], offset);
        };
    }
    return self;
}

@end