//
//  PresentationSync.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 30/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "PresentationSync.h"

@implementation PresentationSync

@synthesize dosageUnitId = _dosageUnitId;
@synthesize name = _name;
@synthesize identifier = _identifier;

- (void)dealloc {
    [_dosageUnitId release];
    [_name release];
    [_identifier release];
    [super dealloc];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"%@(%@, %@, %@)", self.class, _identifier, _name, _dosageUnitId]; 
}

@end
