 //
//  SendStatisticsRequest.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 12/12/12.
//
//

#import "SendStatisticsRequest.h"

static NSString* const kCreatePatientEventName = @"PATIENT";
static NSString* const kUpdatePatientEventName = @"UPDATE";
static NSString* const kEnterHelpEventName = @"HELP";
static NSString* const kStartSessionEventName = @"SESSIONTIME";
static NSString* const kLoginEventName = @"LOGIN";

static NSString* const kDateFormat = @"yyyy-MM-dd HH:mm:ss";

// Error domain
static NSString* const kErrorDomain  = @"es.lumata.ActivateUserRequest";
// Request Name
static NSString* const kRequestName  = @"insertStatistics";


// Content dictionary
static NSString* const kUserIdKey = @"userId";
static NSString* const kStatsListKey = @"statisticList";
static NSString* const kStatsListItemKey = @"statistic";
static NSString* const kElapsedTimeKey = @"elapsedTime";
static NSString* const kDateStartKey = @"dateStart";
static NSString* const kOperationKey = @"operation";
static NSString* const kPatientIdKey = @"idPatient";
static NSString* const kDateEndKey = @"dateEnd";

@implementation SendStatisticsRequest

- (id)initSendStatisticsRequestWithUserId:(NSString*)userId andStats:(NSArray*)stats onComplete:(TCRequestSuccessBlock)onComplete onError:(TCRequestErrorBlock)onError {
    self = [super init];
    if (self) {
        // Set request name
        self.name = kRequestName;
        // Add request content
        [self addParam:userId forKey:kUserIdKey];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:kDateFormat];
        
        NSMutableArray *statsParams = [NSMutableArray arrayWithCapacity:stats.count];
        for (StatsEvent* statsEvent in stats) {
            NSMutableArray* statsProperties = [NSMutableArray arrayWithObject:[TCRequestParam paramWithKey:kOperationKey andValue:statsEvent.event]];
            if (![statsEvent.event isEqualToString:kCreatePatientEventName]) {
                [statsProperties addObject:[TCRequestParam paramWithKey:kDateStartKey andValue:[dateFormatter stringFromDate:statsEvent.startDate]]];
            }
            if ([statsEvent.event isEqualToString:kStartSessionEventName]) {
                [statsProperties addObject:[TCRequestParam paramWithKey:kDateEndKey andValue:[dateFormatter stringFromDate:statsEvent.endDate]]];
            }
            if ([statsEvent.event isEqualToString:kCreatePatientEventName] || [statsEvent.event isEqualToString:kUpdatePatientEventName]) {
                [statsProperties addObject:[TCRequestParam paramWithKey:kPatientIdKey andValue:statsEvent.patientId.stringValue]];
            }
            if ([statsEvent.event isEqualToString:kCreatePatientEventName] || [statsEvent.event isEqualToString:kUpdatePatientEventName] || [statsEvent.event isEqualToString:kStartSessionEventName]) {
                [statsProperties addObject:[TCRequestParam paramWithKey:kElapsedTimeKey andValue:statsEvent.timeElapsed.stringValue]];
            }
            
            [statsParams addObject:[TCRequestParam paramWithKey:kStatsListItemKey andParams:statsProperties]];
        }
        
        [dateFormatter release];
        
        [self addParam:[TCRequestParam paramWithKey:kStatsListKey andParams:statsParams]];
        
        // Set response handler blocks
        self.onError = onError;
        self.onComplete = ^(NSDictionary* responseDictionary) {
            onComplete();
        };
    }
    return self;
}

@end
