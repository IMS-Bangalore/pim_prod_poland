//
//  DoctorInfoSyncRequest.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "BaseRequest.h"
#import "DoctorInfo.h"


typedef enum {
    kDoctorInfoSyncTypeInsert,
    kDoctorInfoSyncTypeUpdate,
} DoctorInfoSyncType;

typedef enum {
    kCenterTypeValueHealthCenter = 1,
    kCenterTypeValuePublicHospital = 2,
    kCenterTypeValuePrivateHospital = 3,
    kCenterTypeValueConsultation = 4,
    kCenterTypeValueOtherCenter = 5
} CenterTypeValue;

/* @brief Request to synchronize local doctor info with remote server */
@interface DoctorInfoSyncRequest : BaseRequest

/**
 @brief Creates a request to synchronize local doctor info with remote server
 @param syncType Defines the type of synchronization
 @param doctorInfo Doctor info to synchronize
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(id) initDoctorInfoSyncRequestWithSyncType:(DoctorInfoSyncType)syncType doctorInfo:(DoctorInfo*)doctorInfo onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError;

@end
