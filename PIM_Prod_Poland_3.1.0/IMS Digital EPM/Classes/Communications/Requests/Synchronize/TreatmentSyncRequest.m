//
//  TreatmentSyncRequest.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "TreatmentSyncRequest.h"
#import "Diagnosis.h"
#import "Patient.h"
#import "DoctorInfo.h"
#import "Presentation.h"
#import "Effect.h"
#import "Dosage.h"
#import "Medicament.h"
#import "UnitType.h"
#import "Frequency.h"
#import "DurationType.h"
#import "TherapyType.h"
#import "TherapyReplacementReason.h"
#import "OtherSpecialty.h"
#import "DrugReimbursement.h"
#import "PatientInsurance.h"
#import "ProductType.h"
#import "DurationType.h"
#import "DrugIndicator.h"

//Ravi_Bukka: For Polish these fields are added newly


//Ravi_Bukka: For Polish these fields are not available

// Request Name
static NSString* const kInsertRequestName  = @"insertMedicine";
static NSString* const kUpdateRequestName  = @"updateMedicine";
static NSString* const kRemoveRequestName  = @"removeMedicine";
// Content dictionary
static NSString* const kUserIdKey =         @"userId";
static NSString* const kPatientIdKey =      @"PatientId";
static NSString* const kDiagnosisIdKey =    @"DiagnosisWindow";
static NSString* const kTreatmentIdKey =    @"TreatmentWindow";

//static NSString* const kPresentationIdKey =  @"Medicine_id";
//static NSString* const kPresentationIdKey =  @"Medicine";
static NSString* const kPresentationIdKey =  @"Products_Id";
//Deepak_Carpenter : Added to resolve manual product issue
static NSString* const kProductCodeKey =  @"ProductCode_Id";

//Deepak_Carpenter: Added for manual data
static NSString* const kProductValueKey =  @"ProductsDescription";
static NSString* const kPresentationValueKey = @"ProductsPresentation";
static NSString* const kDesiredEffectValueKey =  @"DesiredEffectManual";

//static NSString* const kPresentationKey =  @"FormatName";
//static NSString* const kDesiredEffectIdKey =  @"DesiredEffect";
//static NSString* const kDesiredEffectNameKey =  @"DesiredEffectName";

//Kanchan Nair: For Polish these fields are added newly
static NSString *const kDrugReimbursement = @"DrugReimbursement_Id";
static NSString *const kPatientInsurance = @"PatientInsurance_Id";
static NSString *const kNewDrug  = @"DrugIndicator_Id";


static NSString* const kFrequencyKey =  @"DoseFrequency_Id";
static NSString* const kDurationQuantityKey =  @"DurationQuantity";
static NSString* const kDurationType =  @"Duration_Id";

static NSString* const kPrescribedQuantityKey =  @"PrescribedQuantity_Value";

//static NSString* const kTherapyElectionKey =  @"TherapyElection_Id";
static NSString* const kTherapyElectionOtherKey =  @"TherapyElectionOther_Id";
//static NSString* const kTherapyKindKey =  @"TherapyKind";
static NSString* const kTherapyKindKey =  @"Therapy_Id";
//static NSString* const kTherapyKindChangeKey =  @"TherapyKindChange";
//static NSString* const kTherapyKindChangeKey =  @"TherapyReason_Id";
static NSString* const kOtherCommentsKey =  @"OtherComments_Value";

static NSString* const kEmptyID = @"";
static NSString* const kUserDefinedValueID = @"1";
static NSString* const kDoseUnitEmptyID = @"A";

static NSString* const kDoseQuantityKey =  @"DoseQuantity";
static NSString* const kDoseUnitKey =  @"DoseUnits";

static NSString* const kLTD = @"LongDuration";
static NSString* const kOnDemand = @"OnDemand";
static NSString* const kUniqueDose = @"UniqueDose";

//Polish added by kanchan (08/05/14)
static NSString* const kLTDValueYes =     @"1";
static NSString* const kLTDValueNo =      @"2";

@implementation TreatmentSyncRequest

-(id) initTreatmentSyncRequestWithSyncType:(TreatmentSyncType)syncType treatment:(Treatment*)treatment onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    self = [super init];
    if (self) {
        // Set request name
        switch (syncType) {
            case kTreatmentSyncTypeInsert:
                self.name = kInsertRequestName;
                break;
            case kTreatmentSyncTypeUpdate:
                self.name = kUpdateRequestName;
                break;
            case kTreatmentSyncTypeDelete:
                self.name = kRemoveRequestName;
                break;
        }
        
        Diagnosis* diagnosis = treatment.diagnosis;
        if (diagnosis == nil) {
            diagnosis = (Diagnosis*)treatment.parentEntity;
        }
        
        Patient* patient = diagnosis.patient;
        if (patient == nil) {
            patient = (Patient*)diagnosis.parentEntity;
        }
        
        DoctorInfo* doctorInfo = patient.doctorInfo;
        if (doctorInfo == nil) {
            doctorInfo = (DoctorInfo*)patient.parentEntity;
        }
        
        // Add request content
        [self addParam:doctorInfo.identifier forKey:kUserIdKey];
        [self addParam:patient.identifier forKey:kPatientIdKey];
        [self addParam:diagnosis.identifier forKey:kDiagnosisIdKey];
        [self addParam:treatment.identifier forKey:kTreatmentIdKey];
        
        
        // If action is insert or update, include additional info to request
        
        
        if (syncType != kTreatmentSyncTypeDelete) {
            BOOL userDefined = treatment.userMedicament.length > 0 || treatment.userPresentation.length > 0;
           
           // [self addParam:(userDefined ? kUserDefinedValueID : treatment.presentation.identifier) forKey:kPresentationIdKey defaultValue:kEmptyID];
            if(userDefined){
                [self addParam:treatment.medicament.identifier forKey:kProductCodeKey defaultValue:kEmptyID];
            }else
                [self addParam:@"" forKey:kProductCodeKey defaultValue:kEmptyID];
            
            [self addParam:treatment.presentation.identifier forKey:kPresentationIdKey defaultValue:kEmptyID];
            
            [self addParam:treatment.userMedicament forKey:kProductValueKey defaultValue:kEmptyID];
            [self addParam:treatment.userPresentation forKey:kPresentationValueKey defaultValue:kEmptyID];
                
       
          
//                [self addParam:treatment.userPresentation forKey:kPresentationValueKey defaultValue:kEmptyID];
//            [self addParam:treatment.userMedicament forKey:kProductValueKey defaultValue:kEmptyID];
            
//            NSString* presentationName = treatment.presentation != nil ? treatment.presentation.name : treatment.userPresentation;
//            [self addParam:(userDefined ? presentationName : nil) forKey:kPresentationKey];
            
//            [self addParam:treatment.desiredEffect.identifier forKey:kDesiredEffectIdKey defaultValue:kEmptyID];
//            [self addParam:treatment.userDesiredEffect forKey:kDesiredEffectNameKey];
           
            //Kanchan Nair: Added as per new design
            [self addParam:treatment.drugIndicator.identifier forKey:kNewDrug];

            [self addParam:treatment.drugReimbursement.identifier forKey:kDrugReimbursement];
//            [self addParam:treatment.patientInsurance.identifier forKey:kPatientInsurance];

            NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"US"] autorelease]];
            [numberFormatter setPositiveFormat:@"#,##0.#"];
             NSLog(@"Doseunits %@ DoseQuantity %@",treatment.dosage.units,treatment.dosage.quantity);
            
            //Deepak_Carpenter: Added for unit picker view ( only dosequantity will go when ondemand & uniquedose checked)
            if ([treatment.dosage.onDemand boolValue]) {
                [self addNumberParam:@1 forKey:kDoseUnitKey defaultValue:@""];
            } else {
                [self addNumberParam:treatment.dosage.units forKey:kDoseUnitKey defaultValue:@""];
            }
            [self addParam:([treatment.dosage.onDemand boolValue] ? kLTDValueYes : kLTDValueNo) forKey:kOnDemand];
            [self addParam:([treatment.dosage.uniqueDose boolValue] ? kLTDValueYes : kLTDValueNo) forKey:kUniqueDose];
            [self addParam:[numberFormatter stringFromNumber:treatment.dosage.quantity] forKey:kDoseQuantityKey defaultValue:@""];
        
            [numberFormatter release];
            

            [self addParam:treatment.dosage.frequency.identifier forKey:kFrequencyKey defaultValue:kEmptyID];
           
            [self addNumberParam:treatment.dosage.duration forKey:kDurationQuantityKey defaultValue:@""];
            [self addNumberParam:treatment.dosage.recipeCount forKey:kPrescribedQuantityKey];
            
            [self addParam:treatment.dosage.comments forKey:kOtherCommentsKey];
            [self addParam:treatment.therapyChoiceReason.identifier forKey:kTherapyKindKey defaultValue:kEmptyID];
            [self addParam:treatment.recommendationSpecialist.identifier forKey:kTherapyElectionOtherKey defaultValue:kEmptyID];
 //           [self addParam:treatment.therapyType.identifier forKey:kTherapyKindKey defaultValue:kEmptyID];
//            [self addParam:treatment.replaceReason.identifier forKey:kTherapyKindChangeKey defaultValue:kEmptyID];
        //Deepak_Carpenter:
          //  [self addNumberParam:treatment.dosage.uniqueDose forKey:kUniqueDose];
       //     [self addNumberParam:treatment.dosage.longDurationTreatment forKey:kLTD];
        //    [self addNumberParam:treatment.dosage.onDemand forKey:kOnDemand];

       
            //Polish added by kanchan (08/05/14)
            [self addParam:(treatment.dosage.longDurationTreatment.boolValue ? kLTDValueYes : kLTDValueNo) forKey:kLTD];
            [self addParam:treatment.dosage.durationType.identifier forKey:kDurationType];
        }
        // Set response handler blocks
        self.onError = onError;
        self.onComplete = ^(NSDictionary* responseDictionary) {
            onComplete();
        };
    }
    return self;
}

@end
