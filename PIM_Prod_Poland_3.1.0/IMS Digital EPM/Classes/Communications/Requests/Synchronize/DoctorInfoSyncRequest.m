//
//  DoctorInfoSyncRequest.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "DoctorInfoSyncRequest.h"
#import "Doctor.h"
#import "Gender.h"
#import "Province.h"
#import "Specialty.h"
#import "University.h"
#import "MedicalCenter.h"
#import "DoctorType.h"
#import "PracticeType.h"

// Error domain
static NSString* const kErrorDomain  = @"es.lumata.DoctorInfoSyncRequest";
// Request Name
static NSString* const kInsertRequestName  = @"insertDoctor";
static NSString* const kUpdateRequestName  = @"insertDoctor";


static NSString* const kUserIdKey =                 @"userId";
static NSString* const kAgeKey =                    @"Age";
static NSString* const kGenderKey =                 @"Gender_Id";
static NSString* const kDoctorCommentsKey =         @"Comments";
static NSString* const kDoctorEmailKey =            @"Email";
static NSString* const kYearOfDegreeKey =           @"Year";
static NSString* const kCountyOfPracticeKey =       @"Province_Id";
static NSString* const kSpecializationKey =         @"Speciality_Id";
static NSString* const kSecondSpecializationKey =   @"OtherSpeciality_Id";


//static NSString* const kWeeklyHoursKey =            @"weeklyHours";
//static NSString* const kWeeklyPatientsKey =         @"weeklyPatients";

static NSString* const kWeeklyHoursKey =            @"WorkingHours";
static NSString* const kWeeklyPatientsKey =         @"PatientsPerWeek";

//Ravi_Bukka: For Polish these fields are added newly
static NSString* const kDoctorType =                @"DoctorType_Id";
static NSString* const kPracticeType =              @"PracticeType_Id";

//Kanchan: CR#14 New field added Out Patient working Hours
static NSString* const kOutpatientWorkingHours =  @"OutPatientWorkingHours";



static NSString* const kValueYes =     @"1";
static NSString* const kValueNo =      @"0";









@implementation DoctorInfoSyncRequest

//Ravi_Bukka: Webservice here I can do
-(id) initDoctorInfoSyncRequestWithSyncType:(DoctorInfoSyncType)syncType doctorInfo:(DoctorInfo*)doctorInfo onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    self = [super init];
    
    if (self) {
        // Set request name
        switch (syncType) {
            case kDoctorInfoSyncTypeInsert:
                self.name = kInsertRequestName;
                break;
            case kDoctorInfoSyncTypeUpdate:
                self.name = kUpdateRequestName;
                break;
        }
        Doctor* doctor = [doctorInfo doctor];
        


        // Add request content
        [self addParam:doctorInfo.identifier forKey:kUserIdKey];
        [self addNumberParam:doctor.age forKey:kAgeKey];
        [self addParam:doctor.gender.identifier forKey:kGenderKey];
        
        
        [self addParam:doctor.graduationYear forKey:kYearOfDegreeKey];
        [self addParam:doctor.province.identifier forKey:kCountyOfPracticeKey];
        [self addParam:doctor.mainSpeciality.identifier forKey:kSpecializationKey];
        [self addParam:doctor.secondarySpecialty.identifier forKey:kSecondSpecializationKey];
        // Weekly
        [self addNumberParam:doctorInfo.weeklyActivityHours forKey:kWeeklyHoursKey];
        [self addNumberParam:doctorInfo.averageWeeklyPatients forKey:kWeeklyPatientsKey];
        
        [self addParam:doctorInfo.comments forKey:kDoctorCommentsKey];
        [self addParam:doctorInfo.email forKey:kDoctorEmailKey];
        
        [self addNumberParam:doctorInfo.doctorType forKey:kDoctorType];
        [self addNumberParam:doctorInfo.practiceType forKey:kPracticeType];
        
        //Kanchan: CR#14 New field added Out Patient working Hours
        [self addNumberParam:doctorInfo.outPatientWorkingHours forKey:kOutpatientWorkingHours];
        
        // Set response handler blocks
        self.onError = onError;
        self.onComplete = ^(NSDictionary* responseDictionary) {
            
            
            onComplete();
        };
    }
    
    return self;
    
}

@end