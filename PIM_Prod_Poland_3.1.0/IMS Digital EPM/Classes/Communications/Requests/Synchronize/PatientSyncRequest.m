//
//  PatientSyncRequest.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "PatientSyncRequest.h"
#import "DoctorInfo.h"
#import "Age.h"
#import "AgeType.h"
#import "ConsultType.h"
#import "Gender.h"
#import "Insurance.h"
#import "Smoker.h"
#import "PlaceOfVisit.h"
#import "PatientInsurance.h"
#import "TherapyType.h"
#import "OtherSpecialty.h"




// Request Name
static NSString* const kInsertRequestName  = @"insertPatient";
static NSString* const kUpdateRequestName  = @"updatePatient";
static NSString* const kRemoveRequestName  = @"removePatient";
// Content dictionary
static NSString* const kUserIdKey =             @"userId";
static NSString* const kPatientIdKey =          @"PatientId";
static NSString* const kGenderKey =             @"Gender_Id";
static NSString* const kAgeKey =                @"Age";
static NSString* const kAgeTypeKey =            @"AgeType_Id";
//static NSString* const kVisitTypeKey =          @"VisitType_Id";
static NSString* const kVisitDateKey =          @"VisitDate";
static NSString* const kVisitDateFormat =       @"yyyy/MM/dd";

static NSString* const kPlaceOfVisitKey = @"PlaceOfVisit_Id";
static NSString* const kTypeOfContactKey = @"PatientContact_Id";

static NSString* const kEmptyID =               @"";
static NSString* const kEmptyVisitDate =        @"2014/01/01";


//Kanchan
static NSString *const kPatientInsurance = @"PatientInsurance_Id";
//static NSString* const kPatientInsuranceValue =@"PatientInsuranceOther";
static NSString* const kTherapyKey =             @"Therapy_Id";

static NSString* const kTherapyElectionOtherKey =  @"TherapyElectionOther_Id";


//static NSString* const kInsuranceIdKey =@"InsuranceID";
//static NSString* const kUserInsurance =@"userInsurance";


//Ravi_Bukka: Commented for integrating new PatientInfo service
/*
 // Error domain
 static NSString* const kErrorDomain  = @"es.lumata.PatientSyncRequest";
 // Request Name
 static NSString* const kInsertRequestName  = @"insertPatient";
 static NSString* const kUpdateRequestName  = @"updatePatient";
 static NSString* const kRemoveRequestName  = @"removePatient";
 // Content dictionary
 static NSString* const kUserIdKey =             @"userId";
 static NSString* const kPatientIdKey =          @"patientId";
 static NSString* const kRemovePatientIdKey =    @"patientWindowId";
 static NSString* const kGenderKey =             @"sex";
 static NSString* const kAgeKey =                @"age";
 static NSString* const kAgeTypeKey =            @"ageId";
 static NSString* const kVisitTypeKey =          @"visitType";
 static NSString* const kVisitDateKey =          @"dateOfVisit";
 static NSString* const kVisitDateFormat =       @"yyyy/MM/dd";
 
 static NSString* const kEmptyID =               @"1";
 static NSString* const kEmptyVisitDate =        @"0000/00/00"; */




@implementation PatientSyncRequest

-(id) initPatientSyncRequestWithSyncType:(PatientSyncType)syncType patient:(Patient*)patient onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    self = [super init];
    
    if (self) {
        // Set request name
        switch (syncType) {
            case kPatientSyncTypeInsert:
                self.name = kInsertRequestName;
                break;
            case kPatientSyncTypeUpdate:
                self.name = kUpdateRequestName;
                break;
            case kPatientSyncTypeDelete:
                self.name = kRemoveRequestName;
                break;
        }
        // Add request content
        DoctorInfo* doctorInfo = patient.doctorInfo;
        if (doctorInfo == nil) {
            doctorInfo = (DoctorInfo*)patient.parentEntity;
        }
        [self addParam:doctorInfo.identifier forKey:kUserIdKey];
        
        // If action is remove, only add patient id
        if (syncType == kPatientSyncTypeDelete) {
            [self addParam:patient.identifier forKey:kPatientIdKey];
        } else {
            
            [self addParam:patient.identifier forKey:kPatientIdKey];
            [self addNumberParam:patient.age.value forKey:kAgeKey];
            [self addParam:patient.age.ageType.identifier forKey:kAgeTypeKey defaultValue:kEmptyID];
            [self addParam:patient.gender.identifier forKey:kGenderKey defaultValue:kEmptyID];
            [self addParam:patient.insurance.identifier forKey:kPlaceOfVisitKey defaultValue:@""];
            [self addNumberParam:patient.typeOfContact forKey:kTypeOfContactKey];
            //NSLog(@"type of contact -- %@", patient.typeOfContact);
            //NSLog(@"place of visit -- %@", patient.insurance.identifier);

            [self addParam:patient.patientInsurance.identifier forKey:kPatientInsurance defaultValue:@""];
            
            NSString* visitDate = nil;
            if (patient.visitDate) {
                // Visit date
                NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:kVisitDateFormat];
                visitDate = [dateFormatter stringFromDate:patient.visitDate];
                [dateFormatter release];
            }
            [self addParam:visitDate forKey:kVisitDateKey defaultValue:kEmptyVisitDate];
        }
        // Set response handler blocks
        self.onError = onError;
        self.onComplete = ^(NSDictionary* responseDictionary) {
            onComplete();
            
            NSLog(@"Patient response %@", responseDictionary);
        };
    }
    
    return self;
    
    
    //Ravi_Bukka: Commented for integrating new PatientInfo service
    /*    if (self) {
     // Set request name
     switch (syncType) {
     case kPatientSyncTypeInsert:
     self.name = kInsertRequestName;
     break;
     case kPatientSyncTypeUpdate:
     self.name = kUpdateRequestName;
     break;
     case kPatientSyncTypeDelete:
     self.name = kRemoveRequestName;
     break;
     }
     // Add request content
     DoctorInfo* doctorInfo = patient.doctorInfo;
     if (doctorInfo == nil) {
     doctorInfo = (DoctorInfo*)patient.parentEntity;
     }
     [self addParam:doctorInfo.identifier forKey:kUserIdKey];
     
     // If action is remove, only add patient id
     if (syncType == kPatientSyncTypeDelete) {
     [self addParam:patient.identifier forKey:kRemovePatientIdKey];
     } else {
     [self addParam:patient.identifier forKey:kPatientIdKey];
     [self addNumberParam:patient.age.value forKey:kAgeKey];
     [self addParam:patient.age.ageType.identifier forKey:kAgeTypeKey defaultValue:kEmptyID];
     [self addParam:patient.consultType.identifier forKey:kVisitTypeKey defaultValue:kEmptyID];
     [self addParam:patient.gender.identifier forKey:kGenderKey defaultValue:kEmptyID];
     NSString* visitDate = nil;
     if (patient.visitDate) {
     // Visit date
     NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
     [dateFormatter setDateFormat:kVisitDateFormat];
     visitDate = [dateFormatter stringFromDate:patient.visitDate];
     [dateFormatter release];
     }
     [self addParam:visitDate forKey:kVisitDateKey defaultValue:kEmptyVisitDate];
     }
     // Set response handler blocks
     self.onError = onError;
     self.onComplete = ^(NSDictionary* responseDictionary) {
     onComplete();
     };
     }
     
     return self; */
    
}

@end
