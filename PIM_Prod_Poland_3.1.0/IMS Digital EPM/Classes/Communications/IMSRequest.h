//
//  IMSRequest.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 21/12/12.
//
//

#import "TCXMLRequest.h"


@interface IMSRequest : TCXMLRequest

-(NSString *)createBodyContent;

@end
