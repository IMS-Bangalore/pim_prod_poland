//
//  RequestFactory.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 26/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "RequestFactory.h"
#import "RequestLauncher.h"

@interface RequestFactory()

@property (nonatomic, retain) RequestLauncher *requestLauncher;
@property (nonatomic, retain) TCRequestLauncher *tcRequestLauncher;

@end

@implementation RequestFactory

static RequestFactory *_sharedInstance;

@synthesize requestLauncher = _requestLauncher;
@synthesize tcRequestLauncher = _tcRequestLauncher;

- (id)init {
    self = [super init];
    if (self) {
        _requestLauncher = [[RequestLauncher alloc] init];
        _tcRequestLauncher = [[TCRequestLauncher alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_requestLauncher release];
    [_tcRequestLauncher release];
    [super dealloc];
}

+(RequestFactory*) sharedInstance {
    if (_sharedInstance == nil) {
        _sharedInstance = [[RequestFactory alloc] init];
    }
    return _sharedInstance;
}

#pragma mark Session

-(TCBaseRequest*) createLoginRequestWithUserName:(NSString*)userName password:(NSString*)password appVersion:(NSString*)appVersion removePreviousData:(BOOL)removePreviousData onComplete:(LoginResponseBlock)onComplete onError:(TCRequestErrorBlock)onError {
    TCBaseRequest* request = [[LoginRequest alloc] initLoginRequestWithUserName:userName password:password appVersion:appVersion removePreviousData:removePreviousData onComplete:onComplete onError:onError];
    request.requestLauncher = _tcRequestLauncher;
    return [request autorelease];
}

-(BaseRequest*) createActivateUserRequestWithDoctorInfo:(DoctorInfo*)doctorInfo onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    BaseRequest* request = [[ActivateUserRequest alloc] initActivateUserRequestWithDoctorInfo:doctorInfo onComplete:onComplete onError:onError];
    request.requestLauncher = _requestLauncher;
    return [request autorelease];
}

-(BaseRequest*) createChangePasswordRequestWithUserName:(NSString*)userName prevPassword:(NSString*)prevPassword password:(NSString*)password onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    BaseRequest* request = [[ChangePasswordRequest alloc] initChangePasswordRequestWithUserName:userName prevPassword:prevPassword password:password onComplete:onComplete onError:onError];
    request.requestLauncher = _requestLauncher;
    return [request autorelease];
}

-(BaseRequest*) createRemindPasswordRequestWithDoctorId:(NSString*)doctorId onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    BaseRequest* request = [[RemindPasswordRequest alloc] initRemindPasswordRequestWithDoctorId:doctorId onComplete:onComplete onError:onError];
    request.requestLauncher = _requestLauncher;
    return [request autorelease];
}

-(BaseRequest*) createCountdownRequestWithDoctorInfo:(DoctorInfo*)doctorInfo onComplete:(CountdownResponseBlock)onComplete onError:(RequestErrorBlock)onError {
    BaseRequest* request = [[CountdownRequest alloc] initCountdownRequestWithDoctorInfo:doctorInfo onComplete:onComplete onError:onError];
    request.requestLauncher = _requestLauncher;
    return [request autorelease];
}

#pragma mark Synchronize

-(BaseRequest*) createDoctorInfoSyncRequestWithSyncType:(DoctorInfoSyncType)syncType doctorInfo:(DoctorInfo*)doctorInfo onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    BaseRequest* request = [[DoctorInfoSyncRequest alloc] initDoctorInfoSyncRequestWithSyncType:syncType doctorInfo:doctorInfo onComplete:onComplete onError:onError];
    request.requestLauncher = _requestLauncher;
    return [request autorelease];

}

-(BaseRequest*) createPatientSyncRequestWithSyncType:(PatientSyncType)syncType patient:(Patient*)patient onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    BaseRequest* request = [[PatientSyncRequest alloc] initPatientSyncRequestWithSyncType:syncType patient:patient onComplete:onComplete onError:onError];
    request.requestLauncher = _requestLauncher;
    return [request autorelease];
}

-(BaseRequest*) createDiagnosisSyncRequestWithSyncType:(DiagnosisSyncType)syncType diagnosis:(Diagnosis*)diagnosis onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    BaseRequest* request = [[DiagnosisSyncRequest alloc] initDiagnosisSyncRequestWithSyncType:syncType diagnosis:diagnosis onComplete:onComplete onError:onError];
    request.requestLauncher = _requestLauncher;
    return [request autorelease];
}

-(BaseRequest*) createTreatmentSyncRequestWithSyncType:(TreatmentSyncType)syncType treatment:(Treatment*)treatment onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    BaseRequest* request = [[TreatmentSyncRequest alloc] initTreatmentSyncRequestWithSyncType:syncType treatment:treatment onComplete:onComplete onError:onError];
    request.requestLauncher = _requestLauncher;
    return [request autorelease];
}

#pragma mark Update
//Deepak_Carpenter : Added for checkdataversion Implementation
-(BaseRequest*) createUpdateDataRequestWithDoctorInfo:(DoctorInfo*)doctorInfo andOffset:(NSString*)offset lastUpdate:(NSDate*)lastUpdate onComplete:(UpdateDataResponseBlock)onComplete onError:(RequestErrorBlock)onError {
    BaseRequest* request = [[UpdateDataRequest alloc] initUpdateDataRequestWithDoctorInfo:doctorInfo andOffset:(NSString*)offset lastUpdate:lastUpdate onComplete:onComplete onError:onError];
    request.requestLauncher = _requestLauncher;
    return [request autorelease];
}

/////

- (BaseRequest *)createGetDoctorInfoRequestWithUserId:(NSString *)userId onComplete:(GetDoctorInfoResponseBlock)onComplete onError:(RequestErrorBlock)onError {
    BaseRequest* request = [[GetDoctorInfoRequest alloc] initGetDoctorInfoRequestWithUserId:userId onComplete:onComplete onError:onError];
    request.requestLauncher = _requestLauncher;
    return [request autorelease];
}

#pragma mark - Chat

- (BaseRequest*)createChatSendQuestionRequestWithUserId:(NSString *)userId andChatMessage:(ChatMessage *)chatMessage onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    BaseRequest* request = [[ChatSendQuestionRequest alloc] initChatSendQuestionRequestWithUserId:userId andChatMessage:chatMessage onComplete:onComplete onError:onError];
    request.requestLauncher = _requestLauncher;
    return [request autorelease];
}

- (BaseRequest*)createGetAnswersRequestWithUserId:(NSString *)userId onComplete:(ChatGetAnswersResponseBlock)onComplete onError:(RequestErrorBlock)onError {
    BaseRequest* request = [[ChatGetAnswersRequest alloc] initGetAnswersRequestWithUserId:userId onComplete:onComplete onError:onError];
    request.requestLauncher = _requestLauncher;
    return [request autorelease];
}

#pragma mark - Statistics

- (TCBaseRequest *)createSendStatisticsRequestWithUserId:(NSString *)userId andStats:(NSArray*)stats onComplete:(TCRequestSuccessBlock)onComplete onError:(TCRequestErrorBlock)onError; {
    TCBaseRequest* request = [[SendStatisticsRequest alloc] initSendStatisticsRequestWithUserId:userId andStats:stats onComplete:onComplete onError:onError];
    request.requestLauncher = _tcRequestLauncher;
    return [request autorelease];
}

#pragma mark Notifications

- (TCBaseRequest *)createCreateDeviceRequestWithToken:(NSString *)token andDeviceAlias:(NSString *)deviceAlias onComplete:(CreateDeviceResponseBlock)onComplete onError:(RequestErrorBlock)onError {
    TCBaseRequest* request = [[CreateDeviceRequest alloc] initCreateDeviceRequestWithToken:token andDeviceAlias:deviceAlias onComplete:onComplete onError:onError];
    request.requestLauncher = _tcRequestLauncher;
    return  [request autorelease];
}

- (TCBaseRequest *)createGetDeviceNotificationsRequestWithDevice:(Device *)device onComplete:(GetDeviceNotificationsResponseBlock)onComplete onError:(RequestErrorBlock)onError {
    TCBaseRequest* request = [[GetDeviceNotificationsRequest alloc] initGetDeviceNotificationsRequestWithDevice:device onComplete:onComplete onError:onError];
    request.requestLauncher = _tcRequestLauncher;
    return  [request autorelease];
}

@end
