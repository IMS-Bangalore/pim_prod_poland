//
//  RequestParam.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 27/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "RequestParam.h"

@implementation RequestParam

@synthesize key = _key;
@synthesize value = _value;

- (void)dealloc {
    [_key release];
    [_value release];
    [super dealloc];
}

-(id) initWithKey:(NSString*)key andValue:(NSString*)value {
    self = [super init];
    if (self) {
        _key = [key retain];
        _value = [value retain];
    }
    return self;
}

+(RequestParam*) paramWithKey:(NSString*)key andValue:(NSString*)value {
    return [[[RequestParam alloc] initWithKey:key andValue:value] autorelease];
}

@end
