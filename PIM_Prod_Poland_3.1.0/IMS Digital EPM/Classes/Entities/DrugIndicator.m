//
//  DrugIndicator.m
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 28/03/14.
//
//

#import "DrugIndicator.h"
#import "Treatment.h"


@implementation DrugIndicator

@dynamic deleted;
@dynamic identifier;
@dynamic name;
@dynamic treatment;

@end
