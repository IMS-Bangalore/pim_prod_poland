//
//  Treatment.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 21/07/14.
//
//

#import "Treatment.h"
#import "Diagnosis.h"
#import "Dosage.h"
#import "DrugIndicator.h"
#import "DrugReimbursement.h"
#import "Effect.h"
#import "Medicament.h"
#import "OtherSpecialty.h"
#import "Presentation.h"
#import "TherapyReplacementReason.h"
#import "TherapyType.h"


@implementation Treatment

@dynamic index;
@dynamic userDesiredEffect;
@dynamic userDrugIndicator;
@dynamic userDrugReimbursement;
@dynamic userMedicament;
@dynamic userPresentation;
@dynamic userReplacingMedicament;
@dynamic desiredEffect;
@dynamic diagnosis;
@dynamic dosage;
@dynamic drugIndicator;
@dynamic drugReimbursement;
@dynamic medicament;
@dynamic presentation;
@dynamic replaceReason;
@dynamic replacingMedicament;
@dynamic therapyChoiceReason;
@dynamic therapyType;
@dynamic recommendationSpecialist;

@end
