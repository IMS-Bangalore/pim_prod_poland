//
//  Synchronizable.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 02/04/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Synchronizable.h"
#import "SyncAction.h"
#import "Synchronizable.h"


@implementation Synchronizable

@dynamic identifier;
@dynamic actions;
@dynamic secondaryTargetActions;
@dynamic parentEntity;
@dynamic childEntities;

@end
