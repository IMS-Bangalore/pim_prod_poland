//
//  Patient.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 21/07/14.
//
//

#import "Patient.h"
#import "Age.h"
#import "ConsultType.h"
#import "Diagnosis.h"
#import "DoctorInfo.h"
#import "Gender.h"
#import "Insurance.h"
#import "PatientInsurance.h"
#import "PlaceOfVisit.h"
#import "SickFund.h"
#import "Smoker.h"
#import "TherapyType.h"


@implementation Patient

@dynamic index;
@dynamic userInsurance;
@dynamic userPatientInsurace;
@dynamic userPlaceOfVisit;
@dynamic userSickFund;
@dynamic visitDate;
@dynamic typeOfContact;
@dynamic age;
@dynamic consultType;
@dynamic diagnostic;
@dynamic doctorInfo;
@dynamic gender;
@dynamic insurance;
@dynamic patientInsurance;
@dynamic placeOfVisit;
@dynamic sickFund;
@dynamic smoker;
@dynamic therapyChoiceReason;
@dynamic therapyType;

@end

@implementation Patient (Enums)

- (TypeOfContact)typeOfContactEnum {
    if (self.typeOfContact && self.typeOfContact > TypeOfContactUndefined && self.typeOfContact.integerValue < TypeOfContactCount) {
        return (TypeOfContact)self.typeOfContact.integerValue;
    }
    return TypeOfContactUndefined;
}

- (void)setTypeOfContactEnum:(TypeOfContact)typeOfContact {
    if (typeOfContact > TypeOfContactUndefined && typeOfContact < TypeOfContactCount) {
        self.typeOfContact = @(typeOfContact);
    } else {
        self.typeOfContact = nil;
    }
}

@end
