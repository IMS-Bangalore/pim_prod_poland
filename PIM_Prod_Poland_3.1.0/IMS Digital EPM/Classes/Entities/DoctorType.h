//
//  DoctorType.h
//  IMS Digital EPM
//
//  Created by Bukka, Ravi (Bangalore) on 24/02/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Doctor;

@interface DoctorType : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *doctor;
@end

@interface DoctorType (CoreDataGeneratedAccessors)

- (void)addDoctorObject:(Doctor *)value;
- (void)removeDoctorObject:(Doctor *)value;
- (void)addDoctor:(NSSet *)values;
- (void)removeDoctor:(NSSet *)values;

@end
