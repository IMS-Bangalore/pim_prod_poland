//
//  Doctor.m
//  IMS Digital EPM
//
//  Created by Bukka, Ravi (Bangalore) on 24/02/14.
//
//

#import "Doctor.h"
#import "DoctorInfo.h"
#import "DoctorType.h"
#import "Gender.h"
#import "MedicalCenter.h"
#import "PracticeType.h"
#import "Province.h"
#import "Specialty.h"
#import "University.h"


@implementation Doctor

@dynamic age;
@dynamic graduationYear;
@dynamic passwordHash;
@dynamic userID;
@dynamic doctorInfo;
@dynamic doctorType;
@dynamic gender;
@dynamic mainSpeciality;
@dynamic medicalCenter;
@dynamic practiceType;
@dynamic province;
@dynamic secondarySpecialty;
@dynamic university;

@end
