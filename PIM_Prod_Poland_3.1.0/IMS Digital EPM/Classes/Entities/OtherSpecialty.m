//
//  OtherSpecialty.m
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 07/03/14.
//
//

#import "OtherSpecialty.h"
#import "Patient.h"


@implementation OtherSpecialty

@dynamic identifier;
@dynamic name;
@dynamic patient;

@end
