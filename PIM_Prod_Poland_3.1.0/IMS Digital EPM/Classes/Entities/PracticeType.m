//
//  PracticeType.m
//  IMS Digital EPM
//
//  Created by Bukka, Ravi (Bangalore) on 24/02/14.
//
//

#import "PracticeType.h"
#import "Doctor.h"


@implementation PracticeType

@dynamic identifier;
@dynamic name;
@dynamic doctor;

@end
