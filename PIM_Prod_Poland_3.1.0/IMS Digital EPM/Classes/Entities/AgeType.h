//
//  AgeType.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Age;

@interface AgeType : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSSet *ages;
@end

@interface AgeType (CoreDataGeneratedAccessors)

- (void)addAgesObject:(Age *)value;
- (void)removeAgesObject:(Age *)value;
- (void)addAges:(NSSet *)values;
- (void)removeAges:(NSSet *)values;
@end
