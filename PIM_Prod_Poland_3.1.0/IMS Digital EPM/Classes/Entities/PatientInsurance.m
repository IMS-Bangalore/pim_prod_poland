//
//  PatientInsurance.m
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 06/03/14.
//
//

#import "PatientInsurance.h"
#import "Patient.h"


@implementation PatientInsurance

@dynamic deleted;
@dynamic identifier;
@dynamic name;
@dynamic patient;

@end
