//
//  ConsultType.m
//  IMS Digital EPM
//
//  Created by Bukka, Ravi (Bangalore) on 24/02/14.
//
//

#import "ConsultType.h"
#import "Patient.h"


@implementation ConsultType

@dynamic identifier;
@dynamic name;
@dynamic patients;

@end
