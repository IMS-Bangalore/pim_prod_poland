//
//  Presentation.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 18/12/12.
//
//

#import "Presentation.h"
#import "Medicament.h"
#import "Treatment.h"
#import "UnitType.h"


@implementation Presentation

@dynamic identifier;
@dynamic name;
@dynamic deleted;
@dynamic medicament;
@dynamic treatments;
@dynamic unitType;

@end
