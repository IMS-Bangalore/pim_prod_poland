//
//  SyncAction.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 02/04/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "SyncAction.h"
#import "Synchronizable.h"


@implementation SyncAction

@dynamic actionType;
@dynamic creationTime;
@dynamic readyToSync;
@dynamic secondaryTarget;
@dynamic targetObject;

@end
