//
//  Diagnosis.h
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 28/03/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Synchronizable.h"

@class DiagnosisType, Pathology, Patient, Treatment, VisitType;

@interface Diagnosis : Synchronizable

@property (nonatomic, retain) NSNumber * index;
@property (nonatomic, retain) NSNumber * needsTreatment;
@property (nonatomic, retain) NSString * userDiagnosis;
@property (nonatomic, retain) DiagnosisType *diagnosisType;
@property (nonatomic, retain) Pathology *pathology;
@property (nonatomic, retain) Patient *patient;
@property (nonatomic, retain) NSSet *treatments;
@property (nonatomic, retain) VisitType *visitType;
@end

@interface Diagnosis (CoreDataGeneratedAccessors)

- (void)addTreatmentsObject:(Treatment *)value;
- (void)removeTreatmentsObject:(Treatment *)value;
- (void)addTreatments:(NSSet *)values;
- (void)removeTreatments:(NSSet *)values;

@end
