//
//  SickFund.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 10/02/14.
//
//

#import "SickFund.h"
#import "Patient.h"


@implementation SickFund

@dynamic name;
@dynamic identifier;
@dynamic deleted;
@dynamic patients;

@end
