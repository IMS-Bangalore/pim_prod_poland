//
//  Presentation.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 18/12/12.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Medicament, Treatment, UnitType;

@interface Presentation : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) Medicament *medicament;
@property (nonatomic, retain) NSSet *treatments;
@property (nonatomic, retain) UnitType *unitType;
@end

@interface Presentation (CoreDataGeneratedAccessors)

- (void)addTreatmentsObject:(Treatment *)value;
- (void)removeTreatmentsObject:(Treatment *)value;
- (void)addTreatments:(NSSet *)values;
- (void)removeTreatments:(NSSet *)values;
@end
