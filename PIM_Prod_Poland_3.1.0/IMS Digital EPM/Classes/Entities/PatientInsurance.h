//
//  PatientInsurance.h
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 06/03/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Patient;

@interface PatientInsurance : NSManagedObject

@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *patient;
@end

@interface PatientInsurance (CoreDataGeneratedAccessors)

- (void)addPatientObject:(Patient *)value;
- (void)removePatientObject:(Patient *)value;
- (void)addPatient:(NSSet *)values;
- (void)removePatient:(NSSet *)values;

@end
