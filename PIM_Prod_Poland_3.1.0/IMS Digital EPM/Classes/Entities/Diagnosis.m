//
//  Diagnosis.m
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 28/03/14.
//
//

#import "Diagnosis.h"
#import "DiagnosisType.h"
#import "Pathology.h"
#import "Patient.h"
#import "Treatment.h"
#import "VisitType.h"


@implementation Diagnosis

@dynamic index;
@dynamic needsTreatment;
@dynamic userDiagnosis;
@dynamic diagnosisType;
@dynamic pathology;
@dynamic patient;
@dynamic treatments;
@dynamic visitType;

@end
