//
//  SyncAction.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 02/04/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Synchronizable;

@interface SyncAction : NSManagedObject

@property (nonatomic, retain) NSString * actionType;
@property (nonatomic, retain) NSDate * creationTime;
@property (nonatomic, retain) NSNumber * readyToSync;
@property (nonatomic, retain) Synchronizable *secondaryTarget;
@property (nonatomic, retain) Synchronizable *targetObject;

@end
