//
//  Medicament.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 18/12/12.
//
//

#import "Medicament.h"
#import "Presentation.h"
#import "ProductType.h"
#import "RecentMedicament.h"
#import "Treatment.h"


@implementation Medicament

@dynamic identifier;
@dynamic name;
@dynamic deleted;
@dynamic presentations;
@dynamic productType;
@dynamic recentUses;
@dynamic replacedInTreatments;
@dynamic treatments;

@end
