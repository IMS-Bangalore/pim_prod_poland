//
//  DoctorInfo.m
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 05/10/15.
//
//

#import "DoctorInfo.h"
#import "Doctor.h"
#import "MedicalCenter.h"
#import "Patient.h"
#import "RecentMedicament.h"


@implementation DoctorInfo

@dynamic averageWeeklyPatients;
@dynamic comments;
@dynamic doctorType;
@dynamic email;
@dynamic fifthCollaborationDate;
@dynamic fifthCollaborationDateEnd;
@dynamic firstCollaborationDate;
@dynamic firstCollaborationDateEnd;
@dynamic fourthCollaborationDate;
@dynamic fourthCollaborationDateEnd;
@dynamic otherMedicalCenter;
@dynamic practiceType;
@dynamic secondCollaborationDate;
@dynamic secondCollaborationDateEnd;
@dynamic thirdCollaborationDate;
@dynamic thirdCollaborationDateEnd;
@dynamic weeklyActivityHours;
@dynamic outPatientWorkingHours;
@dynamic doctor;
@dynamic medicalCenters;
@dynamic patients;
@dynamic recentMedicaments;

@end
