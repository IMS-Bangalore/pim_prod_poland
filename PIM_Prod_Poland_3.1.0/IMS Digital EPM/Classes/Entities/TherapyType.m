//
//  TherapyType.m
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 07/03/14.
//
//

#import "TherapyType.h"
#import "Patient.h"


@implementation TherapyType

@dynamic identifier;
@dynamic name;
@dynamic patient;

@end
