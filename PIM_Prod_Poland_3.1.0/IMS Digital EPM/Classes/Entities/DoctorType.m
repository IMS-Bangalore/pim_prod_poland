//
//  DoctorType.m
//  IMS Digital EPM
//
//  Created by Bukka, Ravi (Bangalore) on 24/02/14.
//
//

#import "DoctorType.h"
#import "Doctor.h"


@implementation DoctorType

@dynamic identifier;
@dynamic name;
@dynamic doctor;

@end
