//
//  DoctorInfo.h
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 05/10/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Synchronizable.h"

@class Doctor, MedicalCenter, Patient, RecentMedicament;

@interface DoctorInfo : Synchronizable

@property (nonatomic, retain) NSNumber * averageWeeklyPatients;
@property (nonatomic, retain) NSString * comments;
@property (nonatomic, retain) NSNumber * doctorType;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSDate * fifthCollaborationDate;
@property (nonatomic, retain) NSDate * fifthCollaborationDateEnd;
@property (nonatomic, retain) NSDate * firstCollaborationDate;
@property (nonatomic, retain) NSDate * firstCollaborationDateEnd;
@property (nonatomic, retain) NSDate * fourthCollaborationDate;
@property (nonatomic, retain) NSDate * fourthCollaborationDateEnd;
@property (nonatomic, retain) NSString * otherMedicalCenter;
@property (nonatomic, retain) NSNumber * practiceType;
@property (nonatomic, retain) NSDate * secondCollaborationDate;
@property (nonatomic, retain) NSDate * secondCollaborationDateEnd;
@property (nonatomic, retain) NSDate * thirdCollaborationDate;
@property (nonatomic, retain) NSDate * thirdCollaborationDateEnd;
@property (nonatomic, retain) NSNumber * weeklyActivityHours;
@property (nonatomic, retain) NSNumber * outPatientWorkingHours;
@property (nonatomic, retain) Doctor *doctor;
@property (nonatomic, retain) NSSet *medicalCenters;
@property (nonatomic, retain) NSSet *patients;
@property (nonatomic, retain) NSSet *recentMedicaments;
@end

@interface DoctorInfo (CoreDataGeneratedAccessors)

- (void)addMedicalCentersObject:(MedicalCenter *)value;
- (void)removeMedicalCentersObject:(MedicalCenter *)value;
- (void)addMedicalCenters:(NSSet *)values;
- (void)removeMedicalCenters:(NSSet *)values;

- (void)addPatientsObject:(Patient *)value;
- (void)removePatientsObject:(Patient *)value;
- (void)addPatients:(NSSet *)values;
- (void)removePatients:(NSSet *)values;

- (void)addRecentMedicamentsObject:(RecentMedicament *)value;
- (void)removeRecentMedicamentsObject:(RecentMedicament *)value;
- (void)addRecentMedicaments:(NSSet *)values;
- (void)removeRecentMedicaments:(NSSet *)values;

@end
