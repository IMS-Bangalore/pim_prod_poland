//
//  DrugIndicator.h
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 28/03/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Treatment;

@interface DrugIndicator : NSManagedObject

@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *treatment;
@end

@interface DrugIndicator (CoreDataGeneratedAccessors)

- (void)addTreatmentObject:(Treatment *)value;
- (void)removeTreatmentObject:(Treatment *)value;
- (void)addTreatment:(NSSet *)values;
- (void)removeTreatment:(NSSet *)values;

@end
