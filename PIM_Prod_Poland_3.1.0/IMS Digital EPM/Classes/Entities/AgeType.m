//
//  AgeType.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "AgeType.h"
#import "Age.h"


@implementation AgeType

@dynamic name;
@dynamic identifier;
@dynamic ages;

@end
