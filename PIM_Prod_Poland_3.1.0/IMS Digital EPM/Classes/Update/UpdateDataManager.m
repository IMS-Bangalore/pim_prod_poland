//
//  UpdateDataManager.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 30/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "UpdateDataManager.h"
#import "RequestFactory.h"
#import "EntityFactory.h"
#import "DiagnosisType.h"
#import "Effect.h"
#import "Presentation.h"
#import "Medicament.h"
#import "DiagnosisTypeSync.h"
#import "MedicamentSync.h"
#import "PresentationSync.h"
#import "EffectsSync.h"
#import "UnitType.h"
//Deepak_Carpenter: Added for checkdataversion
#import "DurationType.h"
#import "DurationTypeSync.h"
#import "Frequency.h"
#import "FrequencySync.h"
#import "Specialty.h"
#import "SpecialitiesSync.h"
#import "OtherSpecialty.h"
#import "OtherSpecialitiesSync.h"
#import "PatientInsurance.h"
#import "InsuranceSync.h"
#import "Insurance.h"
#import "PlaceOfVisitSync.h"
#import "Province.h"
#import "ProvinceSync.h"
#import "TherapyType.h"
#import "TherapyTypeSync.h"
#import "DrugIndicator.h"
#import "DrugIndicatorSync.h"
#import "DrugReimbursement.h"
#import "DrugReimbursementSync.h"

#import "UnitTypeSync.h"
#import "OneTimeDataUpdate.h"

static NSString* const kOffsetInitValue = @"0";

#pragma mark Private methods declaration
@interface UpdateDataManager()

@property (nonatomic, retain) BaseRequest *updateRequest;
@property (nonatomic, readonly) NSManagedObjectContext* managedObjectContext;
@property (nonatomic, readonly) EntityFactory *entityFactory;
@property (nonatomic, retain) MetaInfo *metaInfo;
@property (nonatomic, retain) NSOperationQueue* operationQueue;
@property (nonatomic, getter = isUpdating) BOOL updating;

-(void) notifyUpdateError:(NSError*)error;
-(void) updateMedicamentArray:(NSArray*)medicamentArray;
-(void) removePresentationsInArray:(NSArray*)presentationDeleteArray;
-(void) updateDiagnosisArray:(NSArray*)diagnosisArray;
-(void) removeDiagnosisInArray:(NSArray*)diagnosisRemoveArray;
-(void) refreshUpdateDate;

@end

#pragma mark Implementation
@implementation UpdateDataManager

@synthesize updateRequest = _updateRequest;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize entityFactory = _entityFactory;
@synthesize metaInfo = _metaInfo;
@synthesize operationQueue = _operationQueue;
@synthesize updating = _updating;

- (id)init {
    self = [super init];
    if (self) {
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.maxConcurrentOperationCount = 1;
        _entityFactory = [EntityFactory sharedEntityFactory];
        _updating = NO;
    }
    return self;
}


- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext == nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        _managedObjectContext.persistentStoreCoordinator = _entityFactory.persistentStoreCoordinator;
    }
    return _managedObjectContext;
}

#pragma mark Update Action

-(NSDate*) yesterday {
    NSDate *now = [NSDate date];
    int daysToAdd = -1;  
    
    // set up date components
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:daysToAdd];
    
    // create a calendar
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDate *yesterday = [gregorian dateByAddingComponents:components toDate:now options:0];
    DLog(@"Yesterday: %@", yesterday);
    return yesterday;
}

-(void) updateWithDoctorInfo:(DoctorInfo*)doctorInfo andOffset:(NSString*)offset {
    // Avoid duplicated requests
    if (self.updating) {
        return;
    }
    
    [_operationQueue addOperationWithBlock:^{
        
        [self.updateRequest cancel];
        self.metaInfo = [_entityFactory getMetaInfoWithContext:self.managedObjectContext];
        DoctorInfo *doctorInfoInContext = [self.managedObjectContext existingObjectWithID:doctorInfo.objectID error:NULL];
        NSDate* lastUpdate = self.metaInfo.lastUpdate;
        // lastUpdate = [lastUpdate dateByAddingTimeInterval:-1 * 365 * 24 * 60 * 60];
        // Check if last update was before yesterday
        //    if ([lastUpdate earlierDate:[self yesterday]] == lastUpdate) {
        self.updateRequest = [[RequestFactory sharedInstance] createUpdateDataRequestWithDoctorInfo:doctorInfoInContext andOffset:offset lastUpdate:lastUpdate onComplete:^(UpdateDataResponse *response, NSString* responseOffset) {
            [_operationQueue addOperationWithBlock:^{
                BOOL endRequest = [responseOffset isEqualToString:kOffsetInitValue];
                [self updateDataWithResponse:response mustSave:endRequest];
                if (!endRequest) {
                    [self updateWithDoctorInfo:doctorInfoInContext andOffset:responseOffset];
                }
                else {
                    self.updating = NO;
                }
            }];
            self.updateRequest = nil;
        } onError:^(NSError *error) {
            [self notifyUpdateError:error];
            self.updating = NO;
            self.updateRequest = nil;
        }];
        [self.updateRequest start];
        //    } else {
        //        DLog(@"Update not needed. Last update: %@", lastUpdate);
        //    }
    }];
}

-(void) updateDataWithResponse:(UpdateDataResponse*)updateResponse mustSave:(BOOL)save {
    [self updateMedicamentArray:updateResponse.medicamentArray];
    [self updateDiagnosisArray:updateResponse.diagnosisArray];
    [self removePresentationsInArray:updateResponse.medicamentDeleteArray];
    [self removeDiagnosisInArray:updateResponse.diagnosisDeleteArray];
    [self updateEffectsArray:updateResponse.effectsArray];
    [self removeEffectsInArray:updateResponse.effectsDeleteArray];
//Deepak_Carpenter : Added for additional checkdataversion fields
    [self updateDurationArray:updateResponse.durationTypeArray];
    [self removeDurationInArray:updateResponse.durationTypeDeleteArray];
    [self updateDoseFrequencyArray:updateResponse.frequencyArray];
    [self removeDoseFrequencyInArray:updateResponse.frequencyDeleteArray];
    [self updateSpecialityArray:updateResponse.specialitiesArray];
    [self removeSpecialityInArray:updateResponse.specialitiesDeleteArray];
    [self updateOtherSpecialityArray:updateResponse.otherSpecialitiesArray];
    [self removeOtherSpecialityInArray:updateResponse.otherSpecialitiesDeleteArray];
    [self updateInsuranceArray:updateResponse.patientInsuranceArray];
    [self removeInsuranceInArray:updateResponse.patientInsuranceDeleteArray];
    [self updatePlaceOfVisitArray:updateResponse.placeOfVisitArray];
    [self removePlaceOfVisitInArray:updateResponse.placeOfVisitDeleteArray];
    [self updateProvinceArray:updateResponse.provinceArray];
    [self removeProvinceInArray:updateResponse.provinceDeleteArray];
    [self updateTherapyTypeArray:updateResponse.therapyTypeArray];
    [self removeTherapyTypeInArray:updateResponse.therapyTypeDeleteArray];
    [self updateDrugIndicatorArray:updateResponse.drugIndicatorArray];
    [self removeDrugIndicatorInArray:updateResponse.drugIndicatorDeleteArray];
    [self updateDrugReimbursementArray:updateResponse.drugReimbursementArray];
    [self removeDrugReimbursementInArray:updateResponse.drugReimbursementDeleteArray];
    
//Utpal: Added for Dosage unit checkdataversion implimentation
    [self updateDosageUnitsArray:updateResponse.dosageUnitsArray];
    [self removeDosageUnitsInArray:updateResponse.dosageUnitsDeleteArray];
    
    NSError* error = nil;
    if (save) {
        DLog(@"Saving update changes..");
        [self refreshUpdateDate];
        [_managedObjectContext save:&error];
    }
    if (error != nil) {
        [_managedObjectContext rollback];
        [self notifyUpdateError:error];
    }
    
    /*One time fix data import*/
    
    OneTimeDataUpdate *oneTimeDataUpdate = [[OneTimeDataUpdate alloc] init];
    if (oneTimeDataUpdate.shouldPerformUpdate) {
        [self removePresentationsInArray:oneTimeDataUpdate.productsIds];
        [self removeDiagnosisInArray:oneTimeDataUpdate.diagnosesIds];
        oneTimeDataUpdate.shouldPerformUpdate = NO;
        NSError *error = nil;
        NSLog(@"Saving update changes..");
        [_managedObjectContext save:&error];
        if (error != nil) {
            [_managedObjectContext rollback];
        }
    }
}

-(void) notifyUpdateError:(NSError*)error {
    DLog(@"Update error: %@", error);
}

#pragma mark Update methods

-(void) updateMedicamentArray:(NSArray*)medicamentArray {
    DLog(@"Updating %ld medicaments...", (long)medicamentArray.count);
    for (MedicamentSync* medicamentSync in medicamentArray) {
        Medicament* medicament = [_entityFactory fetchEntity:NSStringFromClass([Medicament class]) usingIdentifier:medicamentSync.identifier withManagedObjectContext:_managedObjectContext];
        // If medicament is new, insert new instance
        if (medicament == nil) {
            medicament = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Medicament class]) inManagedObjectContext:_managedObjectContext]; 
            medicament.identifier = medicamentSync.identifier;
            DLog(@"Created medicament: %@", medicamentSync);
        } else {
            DLog(@"Updated medicament: %@", medicamentSync);
        }
        // Set medicament properties
        medicament.name = medicamentSync.name;
        medicament.deleted = [NSNumber numberWithBool:NO];
        // Check if product type update is needed
        if (![medicament.productType.identifier isEqualToString:medicamentSync.productTypeId]) {
            ProductType* productType = [_entityFactory fetchEntity:NSStringFromClass([ProductType class]) usingIdentifier:medicamentSync.productTypeId withManagedObjectContext:_managedObjectContext];
            medicament.productType = productType;
            DLog(@"Set product type to: %@", productType.identifier);
        }
        // Update presentations
        for (PresentationSync* presentationSync in medicamentSync.presentations) {
            Presentation* presentation = [_entityFactory fetchEntity:NSStringFromClass([Presentation class]) usingIdentifier:presentationSync.identifier withManagedObjectContext:_managedObjectContext];
            if (presentation == nil) {
                presentation = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Presentation class]) inManagedObjectContext:_managedObjectContext];
                presentation.identifier = presentationSync.identifier;
                DLog(@"Created presentation: %@", presentationSync);
            } else {
                DLog(@"Updated presentation: %@", presentationSync);
            }
            presentation.name = presentationSync.name;
            if (![presentation.unitType.identifier isEqualToString:presentationSync.dosageUnitId]) {
                presentation.unitType = [_entityFactory fetchEntity:NSStringFromClass([UnitType class]) usingIdentifier:presentationSync.dosageUnitId withManagedObjectContext:_managedObjectContext];
                if (presentation.unitType == nil) {
                    presentation.unitType = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([UnitType class]) inManagedObjectContext:_managedObjectContext];
                    presentation.unitType.identifier = presentationSync.dosageUnitId;
                }
                DLog(@"Updated unit type: %@", presentation.unitType.identifier);
            }
            presentation.medicament = medicament;
            presentation.deleted = [NSNumber numberWithBool:NO];
        }
    }
}

-(void) removePresentationsInArray:(NSArray*)presentationDeleteArray {
    DLog(@"Removing %d presentations...", (int)presentationDeleteArray.count);
    for (NSString* identifier in presentationDeleteArray) {
        Presentation* presentation = [_entityFactory fetchEntity:NSStringFromClass([Presentation class]) usingIdentifier:identifier withManagedObjectContext:_managedObjectContext];
        if (presentation != nil) {
            Medicament* medicament = presentation.medicament;
            presentation.medicament = nil;
            DLog(@"Removed presentation with id: %@", identifier);
            presentation.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:presentation];
            if (medicament.presentations.count == 0) {
                DLog(@"Removed medicament: %@", medicament.name);
                medicament.deleted = [NSNumber numberWithBool:YES];
                // [_managedObjectContext deleteObject:medicament];
            }
        }
    }
}

-(void) updateDiagnosisArray:(NSArray*)diagnosisArray {
    DLog(@"Updating %d diagnoses...", (int)diagnosisArray.count);
    for (DiagnosisTypeSync* diagnosisSync in diagnosisArray) {
        // Check if diagnosis already exists
        DiagnosisType* fetchedDiagnosis = [_entityFactory fetchEntity:NSStringFromClass([DiagnosisType class]) usingIdentifier:diagnosisSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedDiagnosis == nil) {
            // Insert instance in managed object context
            fetchedDiagnosis = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DiagnosisType class]) inManagedObjectContext:_managedObjectContext]; 
            fetchedDiagnosis.identifier = diagnosisSync.identifier;
            DLog(@"Created diagnosis: %@", diagnosisSync);
        } else {
            DLog(@"Updated diagnosis: %@", diagnosisSync);
        }
        // Update diagnosis name
        fetchedDiagnosis.name = diagnosisSync.name;
        fetchedDiagnosis.deleted = [NSNumber numberWithBool:NO];
    }
}

-(void) removeDiagnosisInArray:(NSArray*)diagnosisRemoveArray {
    DLog(@"Removing %ld diagnoses...", diagnosisRemoveArray.count);
    for (NSString* diagnosisId in diagnosisRemoveArray) {
        DiagnosisType* fetchedDiagnosis = [_entityFactory fetchEntity:NSStringFromClass([DiagnosisType class]) usingIdentifier:diagnosisId withManagedObjectContext:_managedObjectContext];
        if (fetchedDiagnosis != nil) {
            DLog(@"Removed diagnosis with id: %@", diagnosisId);
            fetchedDiagnosis.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:fetchedDiagnosis];
        }
    }
}

-(void) updateEffectsArray:(NSArray*)effectsArray {
    DLog(@"Updating %ld effects...", effectsArray.count);
    for (EffectsSync* effectSync in effectsArray) {
        // Check if diagnosis already exists
        Effect* fetchedEffect = [_entityFactory fetchEntity:NSStringFromClass([Effect class]) usingIdentifier:effectSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedEffect == nil) {
            // Insert instance in managed object context
            fetchedEffect = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Effect class]) inManagedObjectContext:_managedObjectContext];
            fetchedEffect.identifier = effectSync.identifier;
            DLog(@"Created effect: %@", effectSync);
        } else {
            DLog(@"Updated effect: %@", effectSync);
        }
        // Update effect description
        fetchedEffect.name = effectSync.name;
        fetchedEffect.deleted = [NSNumber numberWithBool:NO];
    }
}

-(void) removeEffectsInArray:(NSArray*)effectsRemoveArray {
    DLog(@"Removing %ld effects...", effectsRemoveArray.count);
    for (NSString* effectId in effectsRemoveArray) {
        Effect* fetchedEffect = [_entityFactory fetchEntity:NSStringFromClass([Effect class]) usingIdentifier:effectId withManagedObjectContext:_managedObjectContext];
        if (fetchedEffect != nil) {
            DLog(@"Removed effect with id: %@", effectId);
            fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:fetchedEffect];
        }
    }
}
//////////////////-----------------Duration---------------///////////////////
-(void) updateDurationArray:(NSArray*)otherDurationArray {
    DLog(@"Updating %ld Duration...", otherDurationArray.count);
    for (DurationTypeSync* durationSync in otherDurationArray) {
        // Check if Duration already exists
        DurationType* fetchedDuration = [_entityFactory fetchEntity:NSStringFromClass([DurationType class]) usingIdentifier:durationSync.identifier withManagedObjectContext:_managedObjectContext];
        
        // If new, create object instance
        if (fetchedDuration == nil ) {
            // Insert instance in managed object context
            fetchedDuration = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DurationType class]) inManagedObjectContext:_managedObjectContext];
            fetchedDuration.identifier = durationSync.identifier;
            
            DLog(@"Created Duration: %@", durationSync);
        } else {
            DLog(@"Updated Duration: %@", durationSync);
        }
        // Update specility description
        fetchedDuration.name = durationSync.name;
        
    }
}
-(void) removeDurationInArray:(NSArray*)durationRemoveArray {
    DLog(@"Removing %ld Duration...", durationRemoveArray.count);
    for (NSString* durationId in durationRemoveArray) {
        DurationType* fetchedDuration = [_entityFactory fetchEntity:NSStringFromClass([DurationType class]) usingIdentifier:durationId withManagedObjectContext:_managedObjectContext];
        if (fetchedDuration != nil ) {
            DLog(@"Removed Duration with id: %@", durationId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            [_managedObjectContext deleteObject:fetchedDuration];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------DoseFrequency---------------///////////////////
-(void)updateDoseFrequencyArray:(NSArray*)doseFrequencyArray {
    DLog(@"Updating %ld DoseFrequency...", doseFrequencyArray.count);
    for (FrequencySync* doseFrequencySync in doseFrequencyArray) {
        // Check if DoseFrequency already exists
        Frequency* fetchedDoseFrequency = [_entityFactory fetchEntity:NSStringFromClass([Frequency class]) usingIdentifier:doseFrequencySync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedDoseFrequency == nil) {
            // Insert instance in managed object context
            fetchedDoseFrequency = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Frequency class]) inManagedObjectContext:_managedObjectContext];
            fetchedDoseFrequency.identifier = doseFrequencySync.identifier;
            
            
            
            DLog(@"Created DoseFrequency: %@", doseFrequencySync);
        } else {
            DLog(@"Updated DoseFrequency: %@", doseFrequencySync);
        }
        // Update DoseFrequency description
        fetchedDoseFrequency.name = doseFrequencySync.name;
        
    }
}

-(void) removeDoseFrequencyInArray:(NSArray*)doseFrequencyRemoveArray {
    DLog(@"Removing %ld DoseFrequency...", doseFrequencyRemoveArray.count);
    for (NSString* DoseFrequencyId in doseFrequencyRemoveArray) {
        Frequency* fetchedFrequency = [_entityFactory fetchEntity:NSStringFromClass([Frequency class]) usingIdentifier:DoseFrequencyId withManagedObjectContext:_managedObjectContext];
        if (fetchedFrequency != nil) {
            DLog(@"Removed DoseFrequency with id: %@", DoseFrequencyId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
             [_managedObjectContext deleteObject:fetchedFrequency];
            
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------Speciality---------------///////////////////
-(void) updateSpecialityArray:(NSArray*)specialityArray {
    DLog(@"Updating %ld Speciality...", specialityArray.count);
    for (SpecialitiesSync* specialitySync in specialityArray) {
        // Check if diagnosis already exists
        Specialty* fetchedSpeciality = [_entityFactory fetchEntity:NSStringFromClass([Specialty class]) usingIdentifier:specialitySync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedSpeciality == nil) {
            // Insert instance in managed object context
            fetchedSpeciality = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Specialty class]) inManagedObjectContext:_managedObjectContext];
            fetchedSpeciality.identifier = specialitySync.identifier;
            DLog(@"Created Speciality: %@", specialitySync);
        } else {
            DLog(@"Updated Speciality: %@", specialitySync);
        }
        // Update specility description
        fetchedSpeciality.name = specialitySync.name;
        //        fetchedSpeciality.deleted = [NSNumber numberWithBool:NO];
    }
}

-(void) removeSpecialityInArray:(NSArray*)specialityRemoveArray {
    DLog(@"Removing %ld effects...", specialityRemoveArray.count);
    for (NSString* specialityId in specialityRemoveArray) {
        Specialty* fetchedSpeciality = [_entityFactory fetchEntity:NSStringFromClass([Specialty class]) usingIdentifier:specialityId withManagedObjectContext:_managedObjectContext];
        if (fetchedSpeciality != nil) {
            DLog(@"Removed Speciality with id: %@", specialityId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
             [_managedObjectContext deleteObject:fetchedSpeciality];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------OtherSpeciality---------------///////////////////
-(void) updateOtherSpecialityArray:(NSArray*)otherSpecialityArray {
    DLog(@"Updating %ld OtherSpeciality...", otherSpecialityArray.count);
    for (OtherSpecialitiesSync* otherSpecialitySync in otherSpecialityArray) {
        // Check if Speciality already exists
        OtherSpecialty* fetchedOtherSpeciality = [_entityFactory fetchEntity:NSStringFromClass([OtherSpecialty class]) usingIdentifier:otherSpecialitySync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedOtherSpeciality == nil) {
            // Insert instance in managed object context
            fetchedOtherSpeciality = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([OtherSpecialty class]) inManagedObjectContext:_managedObjectContext];
            fetchedOtherSpeciality.identifier = otherSpecialitySync.identifier;
            DLog(@"Created OtherSpeciality: %@", otherSpecialitySync);
        } else {
            DLog(@"Updated OtherSpeciality: %@", otherSpecialitySync);
        }
        // Update specility description
        fetchedOtherSpeciality.name = otherSpecialitySync.name;
        //        fetchedSpeciality.deleted = [NSNumber numberWithBool:NO];
    }
}

-(void) removeOtherSpecialityInArray:(NSArray*)otherSpecialityRemoveArray {
    DLog(@"Removing %ld OtherSpeciality...", otherSpecialityRemoveArray.count);
    for (NSString* otherSpecialityId in otherSpecialityRemoveArray) {
        OtherSpecialty* fetchedOtherSpeciality = [_entityFactory fetchEntity:NSStringFromClass([OtherSpecialty class]) usingIdentifier:otherSpecialityId withManagedObjectContext:_managedObjectContext];
        if (fetchedOtherSpeciality != nil) {
            DLog(@"Removed OtherSpeciality with id: %@", otherSpecialityId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
             [_managedObjectContext deleteObject:fetchedOtherSpeciality];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////

//////////////////-----------------Insurance---------------///////////////////
-(void)updateInsuranceArray:(NSArray*)insuranceArray {
    DLog(@"Updating %ld insurance...", insuranceArray.count);
    for (InsuranceSync* insuranceSync in insuranceArray) {
        // Check if Gender already exists
        PatientInsurance* fetchedInsurance = [_entityFactory fetchEntity:NSStringFromClass([PatientInsurance class]) usingIdentifier:insuranceSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedInsurance == nil) {
            // Insert instance in managed object context
            fetchedInsurance = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([PatientInsurance class]) inManagedObjectContext:_managedObjectContext];
            fetchedInsurance.identifier = insuranceSync.identifier;
            DLog(@"Created insurance: %@", insuranceSync);
        } else {
            DLog(@"Updated insurance: %@", insuranceSync);
        }
        // Update insurance description
        fetchedInsurance.name = insuranceSync.name;
    }
}

-(void) removeInsuranceInArray:(NSArray*)insuranceRemoveArray {
    DLog(@"Removing %ld insurance...", insuranceRemoveArray.count);
    for (NSString* insuranceId in insuranceRemoveArray) {
        PatientInsurance* fetchedInsurance = [_entityFactory fetchEntity:NSStringFromClass([PatientInsurance class]) usingIdentifier:insuranceId withManagedObjectContext:_managedObjectContext];
        if (fetchedInsurance != nil) {
            DLog(@"Removed insurance with id: %@", insuranceId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
             [_managedObjectContext deleteObject:fetchedInsurance];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////

//////////////////-----------------PlaceOfVisit---------------///////////////////
-(void)updatePlaceOfVisitArray:(NSArray*)PlaceOfVisitArray {
    DLog(@"Updating %ld PlaceOfVisit...", PlaceOfVisitArray.count);
    for (PlaceOfVisitSync* placeOfVisitSync in PlaceOfVisitArray) {
        // Check if PlaceOfVisit already exists
        Insurance* fetchedPlaceOfVisit = [_entityFactory fetchEntity:NSStringFromClass([Insurance class]) usingIdentifier:placeOfVisitSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedPlaceOfVisit == nil) {
            // Insert instance in managed object context
            fetchedPlaceOfVisit = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Insurance class]) inManagedObjectContext:_managedObjectContext];
            fetchedPlaceOfVisit.identifier = placeOfVisitSync.identifier;
            DLog(@"Created PlaceOfVisit: %@", placeOfVisitSync);
        } else {
            DLog(@"Updated PlaceOfVisit: %@", placeOfVisitSync);
        }
        // Update PlaceOfVisit description
        fetchedPlaceOfVisit.name = placeOfVisitSync.name;
    }
}

-(void) removePlaceOfVisitInArray:(NSArray*)placeOfVisitRemoveArray {
    DLog(@"Removing %ld PlaceOfVisit...", placeOfVisitRemoveArray.count);
    for (NSString* placeOfVisitId in placeOfVisitRemoveArray) {
        Insurance* fetchedPlaceOfVisit = [_entityFactory fetchEntity:NSStringFromClass([Insurance class]) usingIdentifier:placeOfVisitId withManagedObjectContext:_managedObjectContext];
        if (fetchedPlaceOfVisit != nil) {
            DLog(@"Removed PlaceOfVisit with id: %@", placeOfVisitId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
             [_managedObjectContext deleteObject:fetchedPlaceOfVisit];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------Province---------------///////////////////
-(void)updateProvinceArray:(NSArray*)provinceArray {
    DLog(@"Updating %ld Province...", provinceArray.count);
    for (ProvinceSync* provinceSync in provinceArray) {
        // Check if Province already exists
        Province* fetchedProvince = [_entityFactory fetchEntity:NSStringFromClass([Province class]) usingIdentifier:provinceSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedProvince == nil) {
            // Insert instance in managed object context
            fetchedProvince = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Province class]) inManagedObjectContext:_managedObjectContext];
            fetchedProvince.identifier = provinceSync.identifier;
            DLog(@"Created Province: %@", provinceSync);
        } else {
            DLog(@"Updated Province: %@", provinceSync);
        }
        // Update Profession description
        fetchedProvince.name = provinceSync.name;
    }
}

-(void) removeProvinceInArray:(NSArray*)provinceRemoveArray {
    DLog(@"Removing %ld Province...", provinceRemoveArray.count);
    for (NSString* ProvinceId in provinceRemoveArray) {
        Province* fetchedProvince = [_entityFactory fetchEntity:NSStringFromClass([Province class]) usingIdentifier:ProvinceId withManagedObjectContext:_managedObjectContext];
        if (fetchedProvince != nil) {
            DLog(@"Removed Province with id: %@", fetchedProvince);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
             [_managedObjectContext deleteObject:fetchedProvince];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------TherapyType---------------///////////////////
-(void)updateTherapyTypeArray:(NSArray*)therapyTypeArray {
    DLog(@"Updating %ld TherapyType...", therapyTypeArray.count);
    for (TherapyTypeSync* therapyTypeSync in therapyTypeArray) {
        // Check if TherapyType already exists
        TherapyType* fetchedTherapyType = [_entityFactory fetchEntity:NSStringFromClass([TherapyType class]) usingIdentifier:therapyTypeSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedTherapyType == nil) {
            // Insert instance in managed object context
            fetchedTherapyType = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([TherapyType class]) inManagedObjectContext:_managedObjectContext];
            fetchedTherapyType.identifier = therapyTypeSync.identifier;
            
            
            DLog(@"Created TherapyType: %@", therapyTypeSync);
        } else {
            DLog(@"Updated TherapyType: %@", therapyTypeSync);
        }
        // Update TherapyType description
        fetchedTherapyType.name = therapyTypeSync.name;
    }
}

-(void) removeTherapyTypeInArray:(NSArray*)therapyTypeRemoveArray {
    DLog(@"Removing %ld TherapyType...", therapyTypeRemoveArray.count);
    for (NSString* therapyTypeId in therapyTypeRemoveArray) {
        TherapyType* fetchedTherapyType = [_entityFactory fetchEntity:NSStringFromClass([TherapyType class]) usingIdentifier:therapyTypeId withManagedObjectContext:_managedObjectContext];
        
        if (fetchedTherapyType != nil) {
            DLog(@"Removed TherapyType with id: %@", fetchedTherapyType);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:fetche];
            // [_managedObjectContext deleteObject:fetchedTherapyType1];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------DrugIndicator---------------///////////////////
-(void)updateDrugIndicatorArray:(NSArray*)drugIndicatorArray {
    DLog(@"Updating %ld DrugIndicator...", drugIndicatorArray.count);
    for (DrugIndicatorSync* drugIndicatorSync in drugIndicatorArray) {
        // Check if DrugIndicator already exists
        DrugIndicator* fetchedDrugIndicator = [_entityFactory fetchEntity:NSStringFromClass([DrugIndicator class]) usingIdentifier:drugIndicatorSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedDrugIndicator == nil) {
            // Insert instance in managed object context
            fetchedDrugIndicator = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DrugIndicator class]) inManagedObjectContext:_managedObjectContext];
            fetchedDrugIndicator.identifier = drugIndicatorSync.identifier;
            
            
            DLog(@"Created DrugIndicator: %@", drugIndicatorSync);
        } else {
            DLog(@"Updated DrugIndicator: %@", drugIndicatorSync);
        }
        // Update TherapyType description
        fetchedDrugIndicator.name = drugIndicatorSync.name;
    }
}

-(void) removeDrugIndicatorInArray:(NSArray*)drugIndicatorRemoveArray {
    DLog(@"Removing %ld DrugIndicator...", drugIndicatorRemoveArray.count);
    for (NSString* drugIndicatorId in drugIndicatorRemoveArray) {
        DrugIndicator* fetchedDrugIndicator = [_entityFactory fetchEntity:NSStringFromClass([DrugIndicator class]) usingIdentifier:drugIndicatorId withManagedObjectContext:_managedObjectContext];
        
        if (fetchedDrugIndicator != nil) {
            DLog(@"Removed DrugIndicator with id: %@", fetchedDrugIndicator);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:fetche];
             [_managedObjectContext deleteObject:fetchedDrugIndicator];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////

//////////////////-----------------DrugReimbersement---------------///////////////////
-(void)updateDrugReimbursementArray:(NSArray*)drugReimbursementArray {
    DLog(@"Updating %ld DrugReimbersement...", drugReimbursementArray.count);
    for (DrugReimbursementSync* drugReimbursementSync in drugReimbursementArray) {
        // Check if DrugReimbursement already exists
        DrugReimbursement* fetchedDrugReimbursement = [_entityFactory fetchEntity:NSStringFromClass([DrugReimbursement class]) usingIdentifier:drugReimbursementSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedDrugReimbursement == nil) {
            // Insert instance in managed object context
            fetchedDrugReimbursement = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DrugReimbursement class]) inManagedObjectContext:_managedObjectContext];
            fetchedDrugReimbursement.identifier = drugReimbursementSync.identifier;
            
            
            DLog(@"Created DrugReimbursement: %@", drugReimbursementSync);
        } else {
            DLog(@"Updated DrugReimbursement: %@", drugReimbursementSync);
        }
        // Update TherapyType description
        fetchedDrugReimbursement.name = drugReimbursementSync.name;
    }
}

-(void) removeDrugReimbursementInArray:(NSArray*)drugReimbursementRemoveArray {
    DLog(@"Removing %ld DrugReimbursement...", drugReimbursementRemoveArray.count);
    for (NSString* drugReimbursementId in drugReimbursementRemoveArray) {
        DrugReimbursement* fetchedDrugReimbursement = [_entityFactory fetchEntity:NSStringFromClass([DrugReimbursement class]) usingIdentifier:drugReimbursementId withManagedObjectContext:_managedObjectContext];
        
        if (fetchedDrugReimbursement != nil) {
            DLog(@"Removed DrugReimbursement with id: %@", fetchedDrugReimbursement);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
            // [_managedObjectContext deleteObject:fetche];
             [_managedObjectContext deleteObject:fetchedDrugReimbursement];
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
//////////////////-----------------DosageUnits---------------///////////////////
-(void)updateDosageUnitsArray:(NSArray*)dosageUnitsArray {
    DLog(@"Updating %ld UnitType...", dosageUnitsArray.count);
    for (UnitTypeSync* dosageUnitsSync in dosageUnitsArray) {
        // Check if UnitType already exists
        UnitType* fetchedUnitType = [_entityFactory fetchEntity:NSStringFromClass([UnitType class]) usingIdentifier:dosageUnitsSync.identifier withManagedObjectContext:_managedObjectContext];
        // If new, create object instance
        if (fetchedUnitType == nil) {
            // Insert instance in managed object context
            fetchedUnitType = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([UnitType class]) inManagedObjectContext:_managedObjectContext];
            fetchedUnitType.identifier = dosageUnitsSync.identifier;
            DLog(@"Created UnitType: %@", dosageUnitsSync);
        } else {
            DLog(@"Updated UnitType: %@", dosageUnitsSync);
        }
        // Update Unittype description
        fetchedUnitType.name = dosageUnitsSync.name;
    }
}

-(void) removeDosageUnitsInArray:(NSArray*)dosageUnitsRemoveArray {
    DLog(@"Removing %ld UnitType...", dosageUnitsRemoveArray.count);
    for (NSString* UnitTypeId in dosageUnitsRemoveArray) {
        UnitType* fetchedUnitType = [_entityFactory fetchEntity:NSStringFromClass([UnitType class]) usingIdentifier:UnitTypeId withManagedObjectContext:_managedObjectContext];
        if (fetchedUnitType != nil) {
            DLog(@"Removed UnitType with id: %@", UnitTypeId);
            //  fetchedEffect.deleted = [NSNumber numberWithBool:YES];
             [_managedObjectContext deleteObject:fetchedUnitType];
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
-(void) refreshUpdateDate {
    NSDate* lastUpdate = [NSDate date];
    DLog(@"Last update set to: %@", lastUpdate);
    self.metaInfo.lastUpdate = lastUpdate;
}

@end
