//
//  TCPassthroughScrollView.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 28/06/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCPassthroughScrollView : UIScrollView

@end
