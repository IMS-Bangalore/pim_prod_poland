//
//  TCXMLRequest.m
//  TwinCodersLibrary
//
//  Created by Alex Gutiérrez on 09/10/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCXMLRequest.h"
#import "TCRequestParam.h"
#import "NSString+HTML.h"
#import "XMLReader.h"

// Request Params
static NSString* const kRequestParamFormat = @"<%@>%@</%@>";
static NSString* const kRequestParamSchemaFormat = @"%@:%@";
static NSString* const kRequestParamSeparator = @"";
static NSString* const kContentTypeHeaderValue = @"text/xml;charset=UTF-8";
// Response
static NSString* const kResponseValueKey = @"text";

@implementation TCXMLRequest

- (id)init
{
    self = [super init];
    if (self) {
        self.contentType = kContentTypeHeaderValue;
    }
    return self;
}

#pragma mark - Request launch

-(TCRequestMethod)requestMethod {
    return kTCRequestMethodPOST;
}

-(NSString*) stringFromRequestParamsArray:(NSArray*)paramsArray {
    NSString* contentString = @"";
    for (TCRequestParam* param in paramsArray) {
        if (contentString.length > 0) {
            contentString = [contentString stringByAppendingFormat:kRequestParamSeparator];
        }
        // Obtain param Key
        NSString* paramKey = self.paramsSchemaName == nil ? param.key : [NSString stringWithFormat:kRequestParamSchemaFormat, self.paramsSchemaName, param.key];
        
        // If param is complex, make recursive call
        if ([param isComplex]) {
            NSString* innerParams = [self stringFromRequestParamsArray:param.params];
            contentString = [contentString stringByAppendingFormat:kRequestParamFormat, paramKey, innerParams, paramKey];
        } else {
            contentString = [contentString stringByAppendingFormat:kRequestParamFormat, paramKey, param.value, paramKey];
        }
    }
    return contentString;
}

-(NSString*) createBodyContent {
    return [self stringFromRequestParamsArray:self.contentParams];
}

-(ASIHTTPRequest *)createAsiRequest {
    ASIHTTPRequest* asiRequest = [super createAsiRequest];
    // Create content params included in a formatted XML
    NSString* requestParamString = [self createBodyContent];
    [asiRequest appendPostData:[requestParamString dataUsingEncoding:NSUTF8StringEncoding]];
    [asiRequest buildPostBody];
    TCLog(@"\nINPUT:\n%@", requestParamString);
    return asiRequest;
}

#pragma mark - Request response

-(NSString*) stringByCleaningKey:(NSString*)key {
    return key;
}

-(NSString*) stringByCleaningValue:(NSString*)value {
    NSString* stringClearValue = nil;
    if ([value isKindOfClass:[NSString class]]) {
        stringClearValue = [(NSString*)value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        // Remove HTML characters
        stringClearValue = [stringClearValue stringByConvertingHTMLToPlainText];
    }
    return stringClearValue;
}

/** @brief Method that removes empty values and 'text' fields from response dictionary, returning a clean object */
-(id)cleanObjectFrom:(id)sourceObject {
    id returnObject = nil;
    // Clean object of a nil object returns nil
    if (sourceObject != nil) {
        // Dictionary
        if ([sourceObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary* sourceDictionary = (NSDictionary*) sourceObject;
            // Empty dictionaries are omitted
            if (sourceDictionary.count > 0) {
                NSString* innerValue = [self stringByCleaningValue:[sourceDictionary objectForKey:kResponseValueKey]];
                // Base case. If response value contains info, dictionary is cleaned to contained object
                if (innerValue.length > 0) {
                    returnObject = innerValue;
                } else {
                    // Create dictionary
                    NSMutableDictionary* auxDictionary = nil;
                    
                    // Iterate keys
                    for (NSString* key in [sourceDictionary allKeys]) {
                        // Get clean value of content
                        id innerValue = [self cleanObjectFrom:[sourceDictionary objectForKey:key]];
                        // If clean value is not nil, include it in response dictionary
                        if (innerValue != nil) {
                            if (auxDictionary == nil) {
                                auxDictionary = [NSMutableDictionary dictionary];
                            }
                            [auxDictionary setObject:innerValue forKey:[self stringByCleaningKey:key]];
                        }
                    }
                    returnObject = auxDictionary;
                }
            }
        }
        // Array
        else if ([sourceObject isKindOfClass:[NSArray class]]) {
            NSArray* sourceArray = (NSArray*) sourceObject;
            // Create array
            NSMutableArray* auxArray = [NSMutableArray array];
            // Iterate keys
            for (id arrayObject in sourceArray) {
                // Get clean value of content
                id cleanObject = [self cleanObjectFrom:arrayObject];
                // If clean value is not nil, include it in response dictionary
                if (cleanObject != nil) {
                    [auxArray addObject:cleanObject];
                }
            }
            returnObject = auxArray;
        } else if ([sourceObject isKindOfClass:[NSString class]]) {
            NSString* returnString = (NSString*) sourceObject;
            returnString = [returnString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            // If string is empty, return nil
            if (returnString.length == 0) {
                returnObject = nil;
            } else {
                returnObject = returnString;
            }
        } else {
            returnObject = sourceObject;
        }
    }
    return returnObject;
}

- (NSDictionary*)dictionaryForResponseString:(NSString*)responseString {
    NSMutableDictionary* responseDictionary = [NSMutableDictionary dictionary];
    if (!self.isDummy) {
        // Read response XML
        NSError* error = nil;
        NSStringEncoding encoding = self.encoding != 0 ? self.encoding : NSUTF8StringEncoding;
        NSDictionary* xmlDictionary = [XMLReader dictionaryForXMLString:responseString ignoreXMLAtributes:!self.obtainXMLAtributes error:&error withEncoding:encoding];
        if (error == nil) {
            NSDictionary* dictionary = [self cleanObjectFrom:xmlDictionary];
            if (dictionary.count > 0) {
                [responseDictionary addEntriesFromDictionary:dictionary];
            }
        } else {
            TCLog(@"Error parsing response XML: %@", error);
            responseDictionary = nil;
        }
    }
    return responseDictionary;
}

@end
