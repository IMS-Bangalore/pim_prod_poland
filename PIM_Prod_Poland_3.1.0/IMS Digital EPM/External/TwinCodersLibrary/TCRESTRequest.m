//
//  TCRESTRequest.m
//  TwinCodersLibrary
//
//  Created by Alex Gutiérrez on 16/11/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "TCRESTRequest.h"
#import "TCRequestParam.h"

// Segment Params
NSString* const kSegmentParamsSeparator = @"/";
// Query String Params
static NSString* const kQueryStringParamFormat = @"%@=%@";
static NSString* const kQueryStringFormat = @"?%@";
static NSString* const kQueryStringSeparator = @"&";
static NSString* const kContentType = @"application/x-www-form-urlencoded";

@implementation TCRESTRequest

- (id)init
{
    self = [super init];
    if (self) {
        self.segmentParams = [NSMutableArray array];
        self.requestMethod = kTCRequestMethodGET;
        self.contentType = kContentType;
    }
    return self;
}

-(void) addSegmentParam:(NSString*)segmentParam {
    if (segmentParam.length > 0) {
        [self.segmentParams addObject:segmentParam];
    }
}

-(NSString*) url {
    NSString* paramsString = [NSString string];
    if (self.resource != nil) {
        paramsString = [paramsString stringByAppendingPathComponent:self.resource];
    }
    if (self.segmentParams.count > 0) {
        paramsString = [paramsString stringByAppendingPathComponent:[self createSegmentParamsString]];
    }
    if (self.requestMethod == kTCRequestMethodGET && self.contentParams.count > 0) {
        paramsString = [paramsString stringByAppendingString:[self createParametersQueryString]];
    }
    return [self.baseServerUrl stringByAppendingString:paramsString];
}

-(NSString*) createSegmentParamsString {
    return [self stringForSegmentParams:self.segmentParams];
}

-(NSString*) stringForSegmentParams:(NSArray*)params {
    return [params componentsJoinedByString:kSegmentParamsSeparator];
}

-(NSString*) createParametersQueryString {
    NSString* queryString = [NSString stringWithFormat:kQueryStringFormat, [self queryStringFromRequestParamsArray:self.contentParams]];
    return queryString;
}

-(NSString*) queryStringForParameter:(TCRequestParam*)requestParam {
    return [NSString stringWithFormat:kQueryStringParamFormat, requestParam.key, requestParam.value];
}

-(NSString*) queryStringFromRequestParamsArray:(NSArray*)paramsArray {
    NSMutableArray* stringParams = [NSMutableArray arrayWithCapacity:self.contentParams.count];
    for (TCRequestParam* param in self.contentParams) {
        NSString* paramString = [self queryStringForParameter:param];
        [stringParams addObject:paramString];
    }
    NSString* queryString = [stringParams componentsJoinedByString:kQueryStringSeparator];
    return queryString;
}

-(NSString*) stringFromRequestParamsArray:(NSArray*)paramsArray {
    return [self queryStringFromRequestParamsArray:paramsArray];
}

-(NSString *)name {
    return self.resource;
}

-(ASIHTTPRequest *)createAsiRequest {
    ASIHTTPRequest* asiRequest = [super createAsiRequest];
    switch (self.requestMethod) {
        // For POST and PUT requests, include body content
        case kTCRequestMethodPOST:
        case kTCRequestMethodPUT: {
            NSString* bodyContent = [self createBodyContent];
            TCLog(@"\nINPUT:\n%@", bodyContent);
            if (bodyContent != nil) {
                [asiRequest appendPostData:[bodyContent dataUsingEncoding:NSUTF8StringEncoding]];
                [asiRequest buildPostBody];
            }
            break;
        }
        default:;
    }
    return asiRequest;
}

-(NSString*) createBodyContent {
    NSString* bodyContent = nil;
    if (self.requestMethod == kTCRequestMethodPOST) {
        bodyContent = [self stringFromRequestParamsArray:self.contentParams];
    }
    return bodyContent;
}

@end
