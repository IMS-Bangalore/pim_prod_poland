//
//  TCSessionHandler.m
//  TwinCodersLibrary
//
//  Created by Alex Gutiérrez on 11/10/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCSessionHandler.h"

@interface TCSessionHandler()

@property (nonatomic, strong) NSTimer *sessionTimer;
@property (nonatomic, strong) NSDate *lastKeepAlive;
@property (nonatomic, strong) NSMutableArray *sessionExpiredDelegates;
@property (nonatomic, strong) NSMutableDictionary* userInfo;
@property (nonatomic) BOOL sessionActive;

@end

@implementation TCSessionHandler

-(id)init {
    self = [super init];
    if (self) {
        self.sessionExpiredDelegates = [NSMutableArray array];
        self.userInfo = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark - Private methods
- (void) restartSessionTimer {
    [self cancelSessionTimer];
    TCLog(@"Session timer set to %ld seconds", (long)self.sessionDurationInSeconds);
    self.sessionTimer = [NSTimer scheduledTimerWithTimeInterval:self.sessionDurationInSeconds target:self selector:@selector(sessionTimerTriggered) userInfo:nil repeats:NO];
    self.lastKeepAlive = [NSDate date];
}

-(void) cancelSessionTimer {
    if (_sessionTimer != nil) {
        [_sessionTimer invalidate];
        self.sessionTimer = nil;
    }
}

-(void) sessionTimerTriggered {
    TCLog(@"*** Session expired timer triggered ***");
    [self closeSession];
    // Notify delegates
    for (id<SessionExpiredDelegate> delegate in [NSArray arrayWithArray:self.sessionExpiredDelegates] ) {
        [delegate sessionExpired];
    }
}

-(BOOL) isKeepAliveNeeded {
    NSTimeInterval elapsedTime = - _lastKeepAlive.timeIntervalSinceNow;
    return elapsedTime > self.keepAliveMarginInSeconds;
}

- (void)awakeFromDeepSleep {
    if (self.sessionActive && [_sessionTimer.fireDate compare:[NSDate date]] == NSOrderedAscending) {
        [_sessionTimer fire];
    }
}

#pragma mark - Session methods
-(void) startSession {
    if (!self.sessionActive) {
        self.sessionActive = YES;
        [self restartSessionTimer];
    }
}

-(void) keepAliveSession {
    if ([self isSessionActive] && [self isKeepAliveNeeded]) {
        TCLog(@"Launching keep-alive");
        [self restartSessionTimer];
        if (self.keepAliveBlock != nil) {
            self.keepAliveBlock(self);
        }
    }
}

-(void) closeSession {
    self.sessionActive = NO;
    self.sessionToken = nil;
    [self.userInfo removeAllObjects];
    [self cancelSessionTimer];
}

#pragma mark - Delegate methods

-(void) addSessionExpiredDelegate:(id<SessionExpiredDelegate>) sessionExpiredDelegate {
    if (![self.sessionExpiredDelegates containsObject:sessionExpiredDelegate]) {
        [self.sessionExpiredDelegates addObject:sessionExpiredDelegate];
    }
}
-(void) removeSessionExpiredDelegate:(id<SessionExpiredDelegate>) sessionExpiredDelegate {
    if ([self.sessionExpiredDelegates containsObject:sessionExpiredDelegate]) {
        [self.sessionExpiredDelegates removeObject:sessionExpiredDelegate];
    }
}

#pragma mark - User info methods
-(void) setUserInfoValue:(id)value forKey:(NSString *)key {
    if (value != nil) {
        [self.userInfo setValue:value forKey:key];
    } else {
        [self.userInfo removeObjectForKey:key];
    }
}

-(id) userInfoValueForKey:(NSString *)key {
    return [self.userInfo objectForKey:key];
}



@end
