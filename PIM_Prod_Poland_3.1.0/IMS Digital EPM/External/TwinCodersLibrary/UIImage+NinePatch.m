//
//  UIImage+NinePatch.m
//  FnacSocios
//
//  Created by Guillermo Gutiérrez on 13/04/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "UIImage+NinePatch.h"
#import "UIImage-TUNinePatch.h"

@implementation UIImage (NinePatch)

static NSString* const NINEPATCH_EXTENSION = @".9.png";

#pragma mark - Private methods
- (UIEdgeInsets)genericEdgeInsets {
    UIEdgeInsets edgeInsets;
    
    // By default, use 1px rectangle in the center
    edgeInsets.top = roundf(self.size.height / 2) - 1;
    edgeInsets.bottom = edgeInsets.top;
    edgeInsets.left = roundf(self.size.width / 2) - 1;
    edgeInsets.right = edgeInsets.left;
    
    return edgeInsets;
}

- (UIImage*)subImageInRect:(CGRect)rect {
	UIImage *subImage = nil;
	CGImageRef cir = [self CGImage];
	if (cir) {
		rect.origin.x *= self.scale;
		rect.origin.y *= self.scale;
		rect.size.width *= self.scale;
		rect.size.height *= self.scale;
		CGImageRef subCGImage = CGImageCreateWithImageInRect(cir, rect);
		if (subCGImage) {
			subImage = [UIImage imageWithCGImage:subCGImage scale:self.scale orientation:self.imageOrientation];
			CGImageRelease(subCGImage);
		} else {
			TCLog(@"Couldn't create subImage in rect: '%@'.", NSStringFromCGRect(rect));
		}
	} else {
		TCLog(@"self.CGImage is somehow nil.");
	}
	return subImage;
}

- (UIImage*)contentNinePatchImage {
	return [self imageAsNinePatchImage];
}

- (UIEdgeInsets)ninePatchEdgeInsetsWithContentSize:(CGSize)contentSize {
    NSRange upperStrip = [self blackPixelRangeInUpperStrip];
    NSRange leftStrip = [self blackPixelRangeInLeftStrip];
    
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(leftStrip.location, 
                                               upperStrip.location,
                                               contentSize.height - (leftStrip.location + leftStrip.length),
                                               contentSize.width - (upperStrip.location + upperStrip.length));
    return edgeInsets;
}


+ (UIImage*)stretchableImage:(UIImage*)originalImage withEdgeInsets:(UIEdgeInsets)edgeInsets {
    UIImage* stretchableImage = nil;
    // Using iOS4 stretchable image for all operating systems, iOS5 was causing strange errors
    // iOS >=5
//    if ([originalImage respondsToSelector:@selector(resizableImageWithCapInsets:)]) {
//        stretchableImage = [originalImage resizableImageWithCapInsets:edgeInsets];
//    }
//    // iOS <5 
//    else {
        stretchableImage = [originalImage stretchableImageWithLeftCapWidth:edgeInsets.left topCapHeight:edgeInsets.top];
//    }
    return stretchableImage;
}

#pragma mark - Public methods
+ (UIImage*)stretchableImageNamed:(NSString*)imageName {
    if ([[imageName lowercaseString] hasSuffix:NINEPATCH_EXTENSION]) {
        // Nine-Patch image, use the dedicated method
        return [self ninePatchImageNamed:imageName];
    }
    else {
        UIImage* originalImage = [UIImage imageNamed:imageName];
        UIEdgeInsets edgeInsets = [originalImage genericEdgeInsets];
        UIImage* stretchableImage = [self stretchableImage:originalImage withEdgeInsets:edgeInsets];
        return stretchableImage;
    }
}

+ (UIImage*)stretchableImageNamed:(NSString*)imageName withEdgeInsets:(UIEdgeInsets)edgeInsets {
    UIImage* image = [UIImage imageNamed:imageName];
    return [self stretchableImage:image withEdgeInsets:edgeInsets];
}

+ (id)ninePatchImageNamed:(NSString*)imageName {
    UIImage* image = [UIImage imageNamed:imageName];
    return [image ninePatchImage];
}
            
- (id)ninePatchImage {
    UIImage* contentImage = [self contentNinePatchImage];
    UIEdgeInsets edgeInsets = [self ninePatchEdgeInsetsWithContentSize:contentImage.size];
    return [UIImage stretchableImage:contentImage withEdgeInsets:edgeInsets];
}

- (BOOL)isStretchable {
    BOOL stretchable = NO;
    
    // iOS >=5
    if ([self respondsToSelector:@selector(capInsets)]) {
        stretchable = ! UIEdgeInsetsEqualToEdgeInsets(self.capInsets, UIEdgeInsetsZero);
    }
    stretchable = stretchable || self.topCapHeight > 0 || self.leftCapWidth > 0;
    
    return stretchable;
}

@end
