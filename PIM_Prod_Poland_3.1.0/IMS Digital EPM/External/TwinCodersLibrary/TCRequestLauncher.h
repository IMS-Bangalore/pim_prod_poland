//
//  TCRequestLauncher.h
//  TwinCodersLibrary
//
//  Created by Alex Gutiérrez on 08/10/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCBaseRequest.h"

/** @brief Object that will handle the launch of the service requests **/
@interface TCRequestLauncher : NSObject <TCRequestEndDelegate>

@property (nonatomic, assign, getter=isAllowUnsafeCertificate) BOOL allowUnsafeCertificate;

/** @brief Launches the selected request */
-(void) launchRequest:(TCBaseRequest*)request;

/** @brief Cancel the selected request */
-(void) cancelRequest:(TCBaseRequest*)request;

@end
