//
//  ViewController.m
//  IMS Deactivated
//
//  Created by Guillermo Gutiérrez on 22/03/13.
//
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)downloadButtonTapped:(id)sender {
    NSURL* downloadURL = [NSURL URLWithString:@"https://rink.hockeyapp.net/apps/24e4742ad87179bff6219a69ef404928"];
    [[UIApplication sharedApplication] openURL:downloadURL];
}

@end
