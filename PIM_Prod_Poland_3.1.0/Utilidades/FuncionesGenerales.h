//
//  FuncionesGenerales.h
//  IMS Digital EPM
//
//  Created by Ricardo Berzal on 10/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FuncionesGenerales : NSObject {
    
}

+ (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2;
+ (int) AnioActual;

@end
